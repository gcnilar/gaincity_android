# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\madhu\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class app.gaincity.app_gain_city.view.MainFragment.MyJavaScriptInterface {
   public *;
}
-keep class okhttp3.** { *; }
-keep class okio.** { *; }
-keep class retrofit2.** { *; }
#21-2-2019
-keep class StaticMethods
#-keep class com.squareup.retrofit2.** { *; }
#-dontwarn com.squareup.retrofit2.**
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn retrofit2.**
#23-10-2018 added below
-dontwarn com.google.android.gms.internal.stable.*
-dontwarn com.google.android.gms.dynamite.DynamiteModule

#21-2-2019
#The information will only be useful if we can map the obfuscated names back to their original names,
#so we're saving the mapping to a file out.map.
 #The information can then be used by the ReTrace tool to restore the original stack trace.
-printmapping out.map
# Uncomment this to preserve the line number information for
# debugging stack traces. //12-2-2019 uncommented
-keepattributes SourceFile,LineNumberTable

#21-2-2019
-renamesourcefileattribute SourceFile
-keepattributes EnclosingMethod

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
