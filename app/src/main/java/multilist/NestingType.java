

package multilist;

/**
 * MultiLevelListView nest types.
 */
public enum NestingType {

    /**
     * SINGLE nest type. Only one group item is expanded at the same time.
     */
    SINGLE(0),
    /**
     * MULTIPLE nest type. Any group items are expandnded at the same time.
     */
    MULTIPLE(1);

    private int mValue;

    /**
     * Constructor.
     *
     * @param value nest type value.
     */
    NestingType(int value) {
        mValue = value;
    }

    /**
     * Gets nest type value.
     *
     * @return Nest type value.
     */
    public int getValue() {
        return mValue;
    }

    /**
     * Converts integer to nest type.
     *
     * @param value nest type as integer.
     * @return Nest type value.
     */
    public static NestingType fromValue(int value) {
        switch (value) {
            case 0:
                return SINGLE;
            case 1:
            default:
                return MULTIPLE;
        }
    }
}
