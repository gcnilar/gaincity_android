package app.gaincity.app_gain_city.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryResponse {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;
    @SerializedName("url_path")
    private String url_path;

    @SerializedName("group")//7-9-2018
    private String group;//7-9-2018

    @SerializedName("children")
    private ArrayList<CategoryResponse> subcatDetail;

    public CategoryResponse(String name, String id,String group, String url_path, ArrayList<CategoryResponse> subcatDetail) {
        this.id = id;
        this.name = name;
        this.subcatDetail = subcatDetail;
        this.url_path = url_path;
        this.group = group;//7-9-2018
    }

    public String getId() {
        return id;
    }

    public String getgroup() {
        return group;
    }//7-9-2018

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    /*public void setgroup(String group) {
        this.group = group;
    }*///7-9-2018
    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CategoryResponse> getSubcatDetail() {
        return subcatDetail;
    }


    public String geturl_path() {
        return url_path;
    }


}
