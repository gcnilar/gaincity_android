package app.gaincity.app_gain_city.service.model;

import com.google.gson.annotations.SerializedName;

public class CartResponse {
    @SerializedName("id")
    private String id;
    @SerializedName("email")
    private String email;
    @SerializedName("items_qty")
    private String items_qty;

    public CartResponse(String id, String email, String items_qty) {
        this.id = id;
        this.email = email;
        this.items_qty = items_qty;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return email;
    }

    public void setName(String email) {
        this.email = email;
    }

    public String getitems_qty() {
        return items_qty;
    }


}
