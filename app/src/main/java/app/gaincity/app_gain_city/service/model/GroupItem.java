package app.gaincity.app_gain_city.service.model;

public class GroupItem extends BaseItem {

    private int mLevel;

    public GroupItem(String name) {
        super(name);
        mLevel = 0;
    }
}
