package app.gaincity.app_gain_city.service.apiclient;

import java.util.ArrayList;
import app.gaincity.app_gain_city.service.model.CartResponse;
import app.gaincity.app_gain_city.service.model.CategoryResponse;
import app.gaincity.app_gain_city.service.model.MemberResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("menu/api/android")
    Call<ArrayList<CategoryResponse>> getArrCategories();

    @POST("integration/customer/token")
    Call<String> login(@Query(value="username", encoded = true) String username, @Query("password") String password);

    @GET("carts/mine")
    Call<CartResponse> getCartQty();//for checkout- only if there are cart items it will do checkout

    //9-11-2018 s

    @GET("customers/membershipinfo")
    Call<MemberResponse> membershipinfo();

    //9-11-2018 e
}