package app.gaincity.app_gain_city.service.model;

import com.google.gson.annotations.SerializedName;

public class MemberResponse {
    @SerializedName("info")
    private String info;
    @SerializedName("customerName")
    private String customerName;
    @SerializedName("customerNo")//24-12-2018 old @SerializedName("membershipNo")
    private String customerNo;
    @SerializedName("loyaltyPoints")
    private String loyaltyPoints;
    @SerializedName("pointsToExpire")
    private String pointsToExpire;
    @SerializedName("earliestExpireDate")
    private String earliestExpireDate;


    public MemberResponse(String info,String customerName, String customerNo, String loyaltyPoints
    ,String pointsToExpire,String earliestExpireDate) {
        this.info = info;
        this.customerName = customerName;
        this.customerNo = customerNo;
        this.loyaltyPoints = loyaltyPoints;
        this.pointsToExpire = pointsToExpire;
        this.earliestExpireDate = earliestExpireDate;
    }
//17-12-2018 s
    public String getpointsToExpire() {
        return pointsToExpire;
    }

    public void setpointsToExpire(String info) {
        this.pointsToExpire = pointsToExpire;
    }

    public String getearliestExpireDate() {
        return earliestExpireDate;
    }

    public void setearliestExpireDate(String info) {
        this.earliestExpireDate = earliestExpireDate;
    }

    //17-12-2018 e
    public String getinfo() {
        return info;
    }

    public void setinfo(String info) {
        this.info = info;
    }

    public String getcustomerName() {
        return customerName;
    }

    public void setcustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getmembershipNo() {
        return customerNo;
    }

    public void setmembershipNo(String membershipNo) {
        this.customerNo = membershipNo;
    }
    public String getloyaltyPoints() {
        return loyaltyPoints;
    }

    public void setloyaltyPoints(String loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }
}
