package app.gaincity.app_gain_city.service.model;

import java.util.ArrayList;

public class BaseItem {
    private String urlpath;
    private String mName;
    private String group;//7-9-2018
    private int catId;
    private ArrayList<CategoryResponse> subCats;

    public BaseItem(String name) {
        mName = name;
    }

    public ArrayList<CategoryResponse> getSubCats() {
        return subCats;
    }

    public void SetSubCats(ArrayList<CategoryResponse> _subCats) {
        this.subCats = _subCats;
    }

    public String getName() {
        return mName;
    }

    public String geturlpath() {
        return urlpath;
    }
    public String getgroup() {
        return group;
    }//7-9-2018
    public void setgroup(String group) {
        this.group = group;
    }//7-9-2018
    public void seturlpath(String urlpath) {
        this.urlpath = urlpath;
    }

    public void setCatId(int _catId) {
        this.catId = _catId;
    }
}
