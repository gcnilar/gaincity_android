package app.gaincity.app_gain_city.service.apiclient;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import app.gaincity.app_gain_city.Variables;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static String CUSTOMER_LOGIN_URL = Variables.URL_HOME + "/rest/V1/";
    public static String adminToken;
    private static Retrofit categoriesRetrofit = null;
    private static Retrofit customerloginRetrofit = null;
    private static Retrofit CustomerCartQtyClient = null;
    public static String custoken;

    public static Retrofit getcustomerLoginClient() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        customerloginRetrofit = new Retrofit.Builder()
                .baseUrl(CUSTOMER_LOGIN_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();

        return customerloginRetrofit;
    }


    public static Retrofit getCustomerCartQtyClient(String strcustoken) {
        custoken = "";
        custoken = strcustoken;
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Authorization", custoken);
                        return chain.proceed(ongoing.build());
                    }
                })
                .build();

        CustomerCartQtyClient = new Retrofit.Builder()
                .baseUrl(CUSTOMER_LOGIN_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();

        return CustomerCartQtyClient;
    }

    public static Retrofit getClient() {
        if (categoriesRetrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request.Builder ongoing = chain.request().newBuilder();
                            ongoing.addHeader("Accept", "application/json");
                            ongoing.addHeader("Authorization", "Bearer " + adminToken);
                            return chain.proceed(ongoing.build());
                        }
                    })
                    .build();

            categoriesRetrofit = new Retrofit.Builder()
                    .baseUrl(CUSTOMER_LOGIN_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return categoriesRetrofit;
    }
}