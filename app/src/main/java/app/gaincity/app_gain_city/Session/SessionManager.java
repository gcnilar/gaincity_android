package app.gaincity.app_gain_city.Session;

import android.content.Context;
import android.content.SharedPreferences;
//24-9-2018 old import android.os.Build;
import android.util.Base64;
//24-9-2018 old import android.util.Log;
//24-9-2018 old import android.webkit.CookieManager;

import javax.crypto.SecretKey;

import app.gaincity.app_gain_city.EncUtil;
import app.gaincity.app_gain_city.StaticMethods;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "GaincityPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String SESSION_COOKIE = "sessioncookie";
    public static final String LAST_URL = "lasturl";
    public static final String LAST_URL_EXCEPT_LOGIN = "lasturlexceptlogin";

    // User name (make variable public to access from outside)
    public static final String KEY_PASS = "password";
    // Email address (make variable public to access from outside)
    public static final String KEY_TOKEN = "token";
    public static final String KEY_CUST_EMAIL = "CustomerEmail";
    public static final String SAVED_PASSWORD = "savedPassword";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String email, String password, String token) {

        String strEncPw = null;
        try {
            SecretKey secret = EncUtil.generateKey();
            byte[] arrEncPw = EncUtil.encryptMsg(password, secret);
            strEncPw = Base64.encodeToString(arrEncPw, Base64.DEFAULT);//7-6-2018
            //24-9-2018 old Log.d("saveThis", "saveThis: " + strEncPw);
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(_context, e);
            //24-9-2018 old Log.d("secret4", "secret4: " + e.toString());
        }

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putBoolean(SAVED_PASSWORD, true);

        // Storing email in pref
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_CUST_EMAIL, email);
        editor.putString(KEY_PASS, strEncPw);

        // commit changes
        editor.commit();
    }

    public void setlastURL(String lastURL) {
        // Storing login value as TRUE
        editor.putString(LAST_URL, lastURL);
        editor.commit();
    }

    public void setlastURLExceptLogin(String lastURL) {
        // Storing login value as TRUE
        editor.putString(LAST_URL_EXCEPT_LOGIN, lastURL);
        editor.commit();
    }
    public String getlastURLExceptLogin() {
        return pref.getString(LAST_URL_EXCEPT_LOGIN, "");
    }

    public String getlastURL() {
        return pref.getString(LAST_URL, "");
    }

    public void createWebviewSession(String sessioncookie) {
        // Storing login value as TRUE
        editor.putString(SESSION_COOKIE, sessioncookie);
        editor.commit();
    }

    public void logoutUser() {

        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }


    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean isSavedPassword() {
        return pref.getBoolean(SAVED_PASSWORD, false);
    }

    public void setIsLoggedIn(boolean b) {//NOTE: we do not save password, just add login session
        editor.putBoolean(IS_LOGIN, b);
        editor.commit();
    }

    public String getEmail() {
        return pref.getString(KEY_CUST_EMAIL, "");
    }

    public String getPassword() {
        String decryptedPw = null;
        try {
            SecretKey secret = EncUtil.generateKey();
            byte[] arrDecPw = Base64.decode(pref.getString(KEY_PASS, ""), Base64.DEFAULT);
            decryptedPw = EncUtil.decryptMsg(arrDecPw, secret);
            //24-9-2018 old Log.d("saveThis3", "saveThis3: " + decryptedPw);
        } catch (Exception e) {
            e.printStackTrace();
            StaticMethods.sendCrashReport(_context, e);
            //24-9-2018 old Log.d("secret4", "secret4: " + e.toString());
        }

        return decryptedPw;
    }
}
