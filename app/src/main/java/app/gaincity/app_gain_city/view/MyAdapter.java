package app.gaincity.app_gain_city.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.gaincity.app_gain_city.R;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<StoreListitem> list;

    public MyAdapter(Context context, List<StoreListitem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).gettype();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType) {
            case 1: {
                Context scaledContext = ScaledContextWrapper.wrap(context);//29-10-2018
                //29-10-2018 OLD   v = LayoutInflater.from(context).inflate(R.layout.store_list_item, parent, false);
                v = LayoutInflater.from(scaledContext).inflate(R.layout.store_list_item, parent, false); //29-10-2018

                return new ViewHolder1(v);
            }
            case 2: {
                Context scaledContext = ScaledContextWrapper.wrap(context);//29-10-2018
                //29-10-2018 OLD  v = LayoutInflater.from(context).inflate(R.layout.main_title_store, parent, false);
                v = LayoutInflater.from(scaledContext).inflate(R.layout.main_title_store, parent, false); //29-10-2018

                return new ViewHolder2(v);
            }
            case 0: {
                Context scaledContext = ScaledContextWrapper.wrap(context);//29-10-2018
                //29-10-2018 OLD  v = LayoutInflater.from(context).inflate(R.layout.store_list_title, parent, false);
                v = LayoutInflater.from(scaledContext).inflate(R.layout.store_list_title, parent, false);//29-10-2018

                return new ViewHolder0(v);
            }
            case 3: {
                Context scaledContext = ScaledContextWrapper.wrap(context);//29-10-2018

                //29-10-2018 OLD  v = LayoutInflater.from(context).inflate(R.layout.about_store, parent, false);
                v = LayoutInflater.from(scaledContext).inflate(R.layout.about_store, parent, false);//29-10-2018
                return new ViewHolder0(v);
            }
        }
        return new ViewHolder1(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == 1) {
            ViewHolder1 viewHolder1 = (ViewHolder1) holder;
            StoreListitem item = list.get(position);
            viewHolder1.txtname.setText(item.getname());
            viewHolder1.txtaddress.setText(item.getaddress());
            String hours = item.getopening() + " - " + item.getclosing();
            viewHolder1.txthours.setText(hours);
            viewHolder1.txtphone.setText(item.gettelno());
            if (item.getcolor() == 1) {
                viewHolder1.parent.setBackgroundColor(context.getResources().getColor(R.color.textColorPrimary));
            } else if (item.getcolor() == 0) {
                viewHolder1.parent.setBackgroundColor(context.getResources().getColor(R.color.store_list));
            }


        } else if (viewType == 0) {
            ViewHolder0 viewHolder0 = (ViewHolder0) holder;
        } else if (viewType == 2) {

            ViewHolder2 viewHolder2 = (ViewHolder2) holder;
            StoreListitem item = list.get(position);
            viewHolder2.txtTitle.setText(item.getname());
        } else if (viewType == 3) {
            ViewHolder0 viewHolder0 = (ViewHolder0) holder;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {
        public TextView txtTitle;

        public ViewHolder2(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {
        public TextView txtname, txtaddress, txthours, txtphone;
        public LinearLayout parent;

        public ViewHolder1(View itemView) {
            super(itemView);
            parent = (LinearLayout) itemView.findViewById(R.id.parent);
            txtname = (TextView) itemView.findViewById(R.id.name);
            txtaddress = (TextView) itemView.findViewById(R.id.address);
            txthours = (TextView) itemView.findViewById(R.id.hours);
            txtphone = (TextView) itemView.findViewById(R.id.phone);
        }
    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {
        public ViewHolder0(View itemView) {
            super(itemView);

        }
    }

}