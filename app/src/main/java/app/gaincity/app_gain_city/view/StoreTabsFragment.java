package app.gaincity.app_gain_city.view;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;

public class StoreTabsFragment extends Fragment {
    Context ctx;
    static View v;
    static FragmentManager fm;
    public static final String TAG = "StoreTabsFragment";
    private Store_L_AsyncTask storeAsyncTask;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//29-10-2018 S
        Context scaledContext = ScaledContextWrapper.wrap(getActivity());
        LayoutInflater scaledInflater = LayoutInflater.from(scaledContext);
        v = scaledInflater.inflate(R.layout.store_tabs, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018
        //29-10-2018 E
       //29-10-2018 OLD  v = inflater.inflate(R.layout.store_tabs, container, false);
        ctx = (MainActivity) getActivity();
        Variables.storeData = new ArrayList<>();
        Variables.StoreMapData = new ArrayList<>();
        fm = getChildFragmentManager();

        if (Variables.storeData.size() == 0) {
            storeAsyncTask = new Store_L_AsyncTask(ctx, Variables.STATIC_URL + "location.json", getActivity());
            storeAsyncTask.execute();
        }
        return v;

    }


    // Add Fragments to Tabs
    private static void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(fm);
        adapter.addFragment(new StoreMapFragment(), "MAP VIEW");
        adapter.addFragment(new StoreL_Fragment(), "LIST VIEW");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        v = null;
        ctx = null;//22-10-2018

        if (storeAsyncTask != null) {
            storeAsyncTask.cancel(false);
        }
    }

    public static class Store_L_AsyncTask extends AsyncTask<Void, Void, JSONObject> {
        private WeakReference<Context> mContext;
        private boolean errorOccured = false;
        private String jsonURL = "";
        Activity mActivity;

        public Store_L_AsyncTask(Context c, String s, Activity a) {
            mActivity = a;
            mContext = new WeakReference<>(c);
            jsonURL = s;
        }

        @Override
        protected void onPreExecute() {
            Context context = mContext.get();
            if (!(((MainActivity) context).prgdilogStore.isShowing())) {
                ((MainActivity) context).prgdilogStore.show();
            }

        }


        @Override
        protected JSONObject doInBackground(Void... params) {
            Context context = mContext.get();
            if (context == null) return null;

            try {

                URLConnection urlConn = null;
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(jsonURL);
                    urlConn = url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuffer.append(line);
                    }

                    return new JSONObject(stringBuffer.toString());
                } catch (Exception ex) {
                    //24-9-2018 old Log.e("App", "yourDataTask", ex);
                    return null;
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(context, e);

                errorOccured = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            final Context context = mContext.get();
            if (context == null) return;
            if (!errorOccured) {
                if (response != null) {
                    //  JSONObject jsonObj = new JSONObject(jsonStr);
                    try {
                        // Getting JSON Array node
                        JSONArray main = response.getJSONArray("main");
                        Variables.storeData.add(new StoreListitem("About Store", "", "", "", "",
                                "", "", 3, 2));//22-10-2018

                        Variables.storeData.add(new StoreListitem("Main Showrooms", "", "", "", "",
                                "", "", 2, 2));//19-10-2018

                        Variables.storeData.add(new StoreListitem("title1", "", "", "", "",
                                "", "", 0, 2));

                        // looping through All Contacts
                        for (int i = 0; i < main.length(); i++) {
                            JSONObject c = main.getJSONObject(i);
                            String name = c.getString("name");
                            String address = c.getString("address");
                            String opening = c.getString("opening");
                            String closing = c.getString("closing");
                            String telno = c.getString("telno");
                            String latitude = c.getString("latitude");
                            String longitude = c.getString("longitude");

                            // Phone node is JSON Object

                            int cType = i % 2;
                            Variables.storeData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));
                            Variables.StoreMapData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));

                        }
                        Variables.storeData.add(new StoreListitem("Specialty Store", "", "", "", "",
                                "", "", 2, 2));//19-10-2018
                        Variables.storeData.add(new StoreListitem("title1", "", "", "", "",
                                "", "", 0, 2));
                        JSONArray specialty = response.getJSONArray("specialty");

                        // looping through All Contacts
                        for (int i = 0; i < specialty.length(); i++) {
                            JSONObject c = specialty.getJSONObject(i);
                            String name = c.getString("name");
                            String address = c.getString("address");
                            String opening = c.getString("opening");
                            String closing = c.getString("closing");
                            String telno = c.getString("telno");
                            String latitude = c.getString("latitude");
                            String longitude = c.getString("longitude");

                            // Phone node is JSON Object
                            int cType = i % 2;

                            Variables.storeData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));
                            Variables.StoreMapData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));

                        }

                    } catch (final JSONException e) {
                        /*Toast.makeText(context,
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();*/

                    }


                    //  Variables.adapter.notifyDataSetChanged();
                    Variables.viewPager = (ViewPager) v.findViewById(R.id.viewpager);
                    //Variables.viewPager=viewPager;
                    setupViewPager(Variables.viewPager);
                    // Set Tabs inside Toolbar
                    TabLayout tabs = (TabLayout) v.findViewById(R.id.result_tabs);
                    tabs.setupWithViewPager(Variables.viewPager);
                }
            }
            if (((MainActivity) context).prgdilogStore.isShowing()) {
                ((MainActivity) context).prgdilogStore.dismiss();
            }
        }
    }
}