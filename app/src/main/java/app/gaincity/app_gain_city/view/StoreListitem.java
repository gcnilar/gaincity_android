package app.gaincity.app_gain_city.view;

public class StoreListitem {
    private String name;
    private String address;
    private String opening;
    private String closing;
    private String telno;
    private String latitude;
    private String longitude;

    private int type;
    private int color;

    public StoreListitem(String name, String address, String opening, String closing, String telno,
                         String latitude, String longitude, int type, int color) {
        this.name = name;
        this.address = address;
        this.opening = opening;
        this.closing = closing;
        this.telno = telno;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.color = color;
    }

    public int getcolor() {
        return color;
    }

    public void setcolor(int color) {
        this.color = color;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }

    public String getopening() {
        return opening;
    }

    public void setopening(String opening) {
        this.opening = opening;
    }

    public String getclosing() {
        return closing;
    }

    public void setclosing(String closing) {
        this.closing = closing;
    }

    public String gettelno() {
        return telno;
    }

    public void settelno(String telno) {
        this.telno = telno;
    }


    public String getlatitude() {
        return latitude;
    }

    public void setlatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getlongitude() {
        return longitude;
    }

    public void setlongitude(String longitude) {
        this.longitude = longitude;
    }

    public int gettype() {
        return type;
    }

    public void settype(int type) {
        this.type = type;
    }

}
