package app.gaincity.app_gain_city.view;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;
import app.gaincity.app_gain_city.service.apiclient.ApiClient;
import app.gaincity.app_gain_city.service.apiclient.ApiInterface;
import app.gaincity.app_gain_city.service.model.MemberResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.BLACK;


public class FamilyCardFragment extends Fragment {
    public static final String TAG = "FamilyCardFragment";
    int imgWidth;
    View layout_v;
    Context ctx;
    Timer timer_famCard;
    //int testVar;//13-11-2018
    int displayWidth;
    boolean wasAppInBackground = false, calledRefreshBtn = false;
    TextView txtName, txtMembershipNo, txtloyaltyPoints,txtExpiry;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1

        startFamCardTimer();//13-11-2018

    }

    @Override
    public void onPause() {
        super.onPause();
        wasAppInBackground = true;

        //testVar=0;
        if (timer_famCard != null) {
            timer_famCard.cancel();
            timer_famCard.purge();//"purge()" removes all canceled tasks from the task queue
            timer_famCard = null;
        }
    }

    @Override
    public void onStop() {
super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       String screensize= getArguments().getString("screensize");
       if(screensize.equals("large")) {
           layout_v = inflater.inflate(R.layout.family_card_large, container, false);
       }
       else if(screensize.equals("small")){
           layout_v = inflater.inflate(R.layout.family_card_medium, container, false);
       }
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ctx = (MainActivity) getActivity();
        ((MainActivity) ctx).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        txtName = (TextView) layout_v.findViewById(R.id.txtName);
        txtMembershipNo = (TextView) layout_v.findViewById(R.id.txtMembershipNo);
        txtloyaltyPoints = (TextView) layout_v.findViewById(R.id.txtloyaltyPoints);
        txtExpiry= (TextView) layout_v.findViewById(R.id.txtExpiry);
        displayWidth = getArguments().getInt("displayWidth");

        Button btnRefreshFamCard = (Button) layout_v.findViewById(R.id.btnRefreshFamCard);
        btnRefreshFamCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //need to restart timer since it should happen after 10 mints
                if (timer_famCard != null) {
                    timer_famCard.cancel();
                    timer_famCard.purge();//"purge()" removes all canceled tasks from the task queue
                    timer_famCard = null;
                }
                calledRefreshBtn = true;
                //testVar=0;
                startFamCardTimer();
            }
        });

        drawFamCard(getArguments().getString("name"), getArguments().getString("membershipNo"),
                getArguments().getString("loyaltyPoints"), getArguments().getString("earliestExpireDate")
                , getArguments().getString("expirePoints"));//13-11-2018

        return layout_v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        layout_v = null;
        ctx = null;
        layout_v = null;
        //15-11-2018 s
        if (timer_famCard != null) {
            timer_famCard.cancel();
            timer_famCard.purge();//"purge()" removes all canceled tasks from the task queue
            timer_famCard = null;
        }
        //15-11-2018 e
        txtName = null;
        txtMembershipNo = null;
        txtloyaltyPoints = null;
        txtExpiry = null;


    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, imgWidth, imgWidth, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : (getResources().getColor(R.color.bgFamilyCard)); //13-11-2018 old  WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        //12-11-2018 OLD bitmap.setPixels(pixels, 0, imgWidth, 0, 0, w, h);
        bitmap.setPixels(pixels, 0, imgWidth, 0, 0, imgWidth, imgWidth);//12-11-2018
        return bitmap;
    }


    private Bitmap createBarcodeBitmap(String data, int width, int height) throws WriterException {
        MultiFormatWriter writer = new MultiFormatWriter();
        String finalData = Uri.encode(data);

        //   height is 1 as this is a 1D Barcode.
        BitMatrix bm = writer.encode(finalData, BarcodeFormat.CODE_128, width, 1);
        int bmWidth = bm.getWidth();

        Bitmap imageBitmap = Bitmap.createBitmap(bmWidth, height, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < bmWidth; i++) {
            // Paint columns of width 1
            int[] column = new int[height];
            Arrays.fill(column, bm.get(i, 0) ? Color.BLACK : (getResources().getColor(R.color.bgFamilyCard))); //13-11-2018 old Color.WHITE);
            imageBitmap.setPixels(column, 0, 1, i, 0, 1, height);
        }

        return imageBitmap;
    }

    public void startFamCardTimer() {
        int waitTime;
        if (wasAppInBackground || calledRefreshBtn) {
            waitTime = 0;
        } else {
            waitTime = 10;
        }
        wasAppInBackground = false;

        if (timer_famCard == null) {
            timer_famCard = new Timer();
            TimerTask_FamCard Every10MinTask = new TimerTask_FamCard() {//TO AVOID LEAKS
                @Override
                public void run() {
                    // testVar=testVar+1;//13-11-2018
                    memberInfo_inFrag();
                }
            };

            timer_famCard.schedule(Every10MinTask, TimeUnit.MINUTES.toMillis(waitTime), TimeUnit.MINUTES.toMillis(10));
        }
    }

    public void memberInfo_inFrag() {

        ((MainActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!(((MainActivity) ctx).prgdialogMenu.isShowing())) {
                    ((MainActivity) ctx).prgdialogMenu.show();
                }
            }
        });


        ApiInterface apiService = null;
        Call<MemberResponse> famCardCall = null;
//Log.d("Variables.cusToken","Variables.cusToken"+Variables.cusToken);

        apiService = ApiClient.getCustomerCartQtyClient(Variables.cusToken).create(ApiInterface.class);
        famCardCall = apiService.membershipinfo();
    if(famCardCall!=null){//4-1-2018
        famCardCall.enqueue(new Callback<MemberResponse>() {
            @Override
            public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {
                if (!(response.isSuccessful())) {

                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx, getResources().getString(R.string.error_occured), Toast.LENGTH_SHORT).show();

                            if (((MainActivity) ctx).prgdialogMenu.isShowing()) {
                                ((MainActivity) ctx).prgdialogMenu.dismiss();
                            }
                            ((MainActivity) ctx).closeFamilyCard_inFrag();
                            //17-12-2018 s
                            ((MainActivity) ctx).getTokenSilently(((MainActivity) ctx).username, ((MainActivity) ctx).password);
                            //17-12-2018 e
                        }
                    });

                    return;
                } else {
                    //Log.d("expiryL","expiryL"+response.body().getearliestExpireDate());//17-12-2018
                    final boolean isMemLoggedIn = response.body().getinfo().equals("Member Logged In");
                    final String strName = response.body().getcustomerName();
                    final String strMemNo = response.body().getmembershipNo();
                    final String strExp = response.body().getearliestExpireDate();
                    final String strExpPoints = response.body().getpointsToExpire();
                    final String strLoyaltyPoints = response.body().getloyaltyPoints();

                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isMemLoggedIn) {
                                drawFamCard(strName, strMemNo, strLoyaltyPoints, strExp, strExpPoints);//17-12-2018
                            } else {
                                //not logged in
                                Toast.makeText(ctx, getResources().getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
                                if (((MainActivity) ctx).prgdialogMenu.isShowing()) {
                                    ((MainActivity) ctx).prgdialogMenu.dismiss();
                                }
                                ((MainActivity) ctx).closeFamilyCard_inFrag();
                                //17-12-2018 s
                                ((MainActivity) ctx).getTokenSilently(((MainActivity) ctx).username, ((MainActivity) ctx).password);
                                //17-12-2018 e
                                /*17-12-2018 old ((MainActivity) ctx).callWebViewLogoutFromWebView();//13-11-2018
                                //((MainActivity)ctx).logoutAndClearCookies();//can not clear cookie as we call logout in webview while
                                // happenning clearing cookies// But No need native logout here, since we
                                //call webview logout, it will at last call native logout from webview*/
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<MemberResponse> call, Throwable t) {
                ((MainActivity) ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ctx, getResources().getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
                        if (((MainActivity) ctx).prgdialogMenu.isShowing()) {
                            ((MainActivity) ctx).prgdialogMenu.dismiss();
                        }
                        ((MainActivity) ctx).closeFamilyCard_inFrag();
                        //17-12-2018 s
                        ((MainActivity) ctx).getTokenSilently(((MainActivity) ctx).username, ((MainActivity) ctx).password);
                        //17-12-2018 e
                    }
                });

            }
        });
        //4-1-2018 s
    }
    else{
        Toast.makeText(ctx, getResources().getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        if (((MainActivity) ctx).prgdialogMenu.isShowing()) {
            ((MainActivity) ctx).prgdialogMenu.dismiss();
        }
        ((MainActivity) ctx).closeFamilyCard_inFrag();
        //17-12-2018 s
        ((MainActivity) ctx).getTokenSilently(((MainActivity) ctx).username, ((MainActivity) ctx).password);

    }
        //4-1-2018 e
    }

    public void drawFamCard(String name, String memNo, String loyaltyPoints, String strExp,String expirePoints) {//17-12-2018

        txtName.setText(name);
        txtMembershipNo.setText(memNo);
        txtloyaltyPoints.setText(loyaltyPoints);
        LinearLayout Expiryll  =(LinearLayout)layout_v.findViewById(R.id.Expiryll);
        if(!(expirePoints.equals("0"))) {//21-12-2018
            String strDetailsExp = expirePoints + " points will expire on " + strExp;//17-12-2018
            Expiryll.setVisibility(View.VISIBLE);//21-12-2018
            txtExpiry.setText(strDetailsExp);//17-12-2018
        }
        else if(expirePoints.equals("0")){//21-12-2018
            Expiryll.setVisibility(View.GONE);//21-12-2018
        }

        int barcodeWidth =(int)Math.round( displayWidth /2.5);//31-1-2019 old ok displayWidth / 4;
        int barcodeHeight =  displayWidth / 14;//31-1-2019 old ok displayWidth / 16;
        imgWidth =(int)Math.round( displayWidth /2.5);//31-1-2019 old ok displayWidth / 4;

        ImageView img_qr = (ImageView) layout_v.findViewById(R.id.generatedQR);
        ImageView img_Barcode = (ImageView) layout_v.findViewById(R.id.generatedBarcode);

        try {
            // generate a 150x150 QR code
            Bitmap bm = encodeAsBitmap(memNo);//9-11-2018 old ("barcode_content");

            if (bm != null) {
                img_qr.setImageBitmap(bm);
            }
        } catch (WriterException e) {
        }

        try {
            // generate a 150x150 QR code
            Bitmap bm = createBarcodeBitmap(memNo, barcodeWidth, barcodeHeight);//9-11-2018 old ("barcode_content");

            if (bm != null) {
                img_Barcode.setImageBitmap(bm);
            }
        } catch (WriterException e) {
        }

        if (((MainActivity) ctx).prgdialogMenu.isShowing()) {
            ((MainActivity) ctx).prgdialogMenu.dismiss();
        }
    }

    static class TimerTask_FamCard extends TimerTask {//NOTE: MADE STATIC CLASS TO AVOID MEMORY LEAKS

        @Override
        public void run() {

        }
    }
}