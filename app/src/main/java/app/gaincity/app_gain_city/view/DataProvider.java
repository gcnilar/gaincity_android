package app.gaincity.app_gain_city.view;

import java.util.ArrayList;
import java.util.List;

import app.gaincity.app_gain_city.Variables;
import app.gaincity.app_gain_city.service.model.BaseItem;
import app.gaincity.app_gain_city.service.model.CategoryResponse;
import app.gaincity.app_gain_city.service.model.GroupItem;
import app.gaincity.app_gain_city.service.model.Item;

public class DataProvider {
    public static List<BaseItem> getInitialItems() {
        return getRootItems(new GroupItem("root"));
    }

    public static List<BaseItem> getRootItems(BaseItem baseItem) {
        List<BaseItem> cc = getCatItems(Variables.categories);
        return cc;
    }

    public static List<BaseItem> getSubItems(BaseItem baseItem) {
        List<BaseItem> cc = getCatItems(baseItem.getSubCats());
        return cc;
    }

    public static List<BaseItem> getCatItems(ArrayList<CategoryResponse> baseItem) {
        List<BaseItem> result = new ArrayList<>();
        for (CategoryResponse cat : baseItem) {
            //Onclick it adds again
            BaseItem item;
            if (cat.getSubcatDetail() != null && cat.getSubcatDetail().size() != 0) {
                item = new GroupItem(cat.getName());
                ((GroupItem) item).SetSubCats(cat.getSubcatDetail());

                if (cat.getId().trim().length() == 0) {
                    item.setCatId(0);
                } else {
                    item.setCatId(Integer.parseInt(cat.getId()));
                }

                item.seturlpath(cat.geturl_path());
                item.setgroup(cat.getgroup());//7-9-2018
            } else {

                item = new Item(cat.getName());
                if (cat.getId().trim().length() == 0) {
                    item.setCatId(0);
                } else {
                    item.setCatId(Integer.parseInt(cat.getId()));
                }

                item.seturlpath(cat.geturl_path());
                item.setgroup(cat.getgroup());//7-9-2018
            }
            result.add(item);
        }

        return result;
    }

    public static boolean isExpandable(BaseItem baseItem) {
        return baseItem instanceof GroupItem;
    }

}
