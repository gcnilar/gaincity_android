package app.gaincity.app_gain_city.view;

import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;//16-10-2018 old android.app.Fragment;
//24-9-2018 old import android.app.ProgressDialog;
import android.content.Context;
//24-9-2018 old import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
//24-9-2018 old import android.util.Log;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//24-9-2018 old import android.webkit.CookieManager;
//24-9-2018 old import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;

public class LoginFragment extends Fragment {
    public static final String TAG = "LoginFragment";
    String email, password;
    View layout_v;
    Context ctx;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout_v = inflater.inflate(R.layout.activity_login, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018
        ctx= (MainActivity) getActivity();//11-9-2018
        //6-9-2018 s
        if(!(Variables.forgotPasswordEmail.equals(""))){
String forgotPasswordmsg="If there is an account associated with "+Variables.forgotPasswordEmail+" you will receive an email with a link to reset your password." ;
        //String forgotPasswordmsg2="If you don't see account recovery email in your inbox within 15 minutes, look for it in \"Junk Mail Box\". If you find it there, please mark it as \"Not Junk\".";
            TextView txtmessage = (TextView) layout_v.findViewById(R.id.txtmessage);

           // String forgotPwDisplaymsg = "<b>" + forgotPasswordmsg1 + "</b> <br>" + forgotPasswordmsg2;
            txtmessage.setText(Html.fromHtml(forgotPasswordmsg ));

            Variables.forgotPasswordEmail="";//11-9-2018
        }
        else{

            RelativeLayout green_border = (RelativeLayout) layout_v.findViewById(R.id.green_border);
            green_border.setVisibility(View.GONE);
        }
        //6-9-2018 e

        if((Variables.numcartItemsBeforeClickCheckout!=0)){
            Variables.numcartItemsBeforeClickCheckout=0;
            LinearLayout btncontinueAsGuest = (LinearLayout) layout_v.findViewById(R.id.guestLayout);
            btncontinueAsGuest.setVisibility(View.VISIBLE);
        }


//If user has already logged in and was redirected to native login form from serverside
        //that means need to logout from native side too
        //But if user comes from cart page to native login form page after clicking proceed checkout
        // , user is already loggedout and no need
        //dismissing any progressdialogs and some progressdialogs may be required too?
        //yes , After showing native login(LoginFragment) it redirects
        //            //webview to cart page behind the scene, so we should stay until it happened in the
        //background. so we still need progress dialog when user is in login form
        //while doing checkout
//But except in checkout, are there any other possible places which redirects to native login can
        //happen with progress dialogs when user is not logged in? seems not.
        //Unless if there is an internal server error and WHILE doing something with progressdialog, incorrectly showed native login for not
        //logged in user, then it can happen- WILL THIS HAPPEN?

        //12-9-2018 S
        if((((MainActivity) ctx).prgCancelled.isShowing())){
            ((MainActivity) ctx).prgCancelled.dismiss();
        }
        //12-9-2018 E
        //13-9-2018 s
        if(((MainActivity) ctx).prgdialogHome.isShowing()){

            ((MainActivity) ctx).prgdialogHome.dismiss();
        }//13-9-2018 e
        if (((MainActivity) ctx).prgdilogBtn.isShowing()) {

            ((MainActivity) ctx).prgdilogBtn.dismiss();
        }
        //TODO:check later S
        if (((MainActivity) ctx).prgdilogStore.isShowing()) {
            ((MainActivity) ctx).prgdilogStore.dismiss();
        }
        if(((MainActivity) ctx).prgdialogMenu.isShowing()){
            ((MainActivity) ctx).prgdialogMenu.dismiss();
        }
        //TODO:check later e
        if (((MainActivity) ctx).prgdialog.isShowing()) {
            ((MainActivity) ctx).prgdialog.dismiss();
        }
        if (((MainActivity) ctx).prgdialogsearch.isShowing()) {
            ((MainActivity) ctx).prgdialogsearch.dismiss();
        }
        if (((MainActivity) ctx).prgdialogLogout.isShowing()) {
            ((MainActivity) ctx).prgdialogLogout.dismiss();

//27-08-2018 s
            ((MainActivity) ctx).clearCookies();

//27-08-2018 e

        }
        if (((MainActivity) ctx).prgdialog_noti.isShowing()) {
            ((MainActivity) ctx).prgdialog_noti.dismiss();
        }
        if ((((MainActivity) ctx).prgdialogChkout != null) && (((MainActivity) ctx).prgdialogChkout.isShowing())) {
            //NOTE: This prgdialogChkout is not initializing in oncreate
            //of MainActivity , so I had to check whether it is null or not first
            ((MainActivity) ctx).prgdialogChkout.dismiss();
        }

        if (((MainActivity) ctx).prgdialogAdding.isShowing()) {
            ((MainActivity) ctx).prgdialogAdding.dismiss();
        }
        if (((MainActivity) ctx).prgDialogLoading.isShowing()) {
            ((MainActivity) ctx).prgDialogLoading.dismiss();
        }
        if (((MainActivity) ctx).prgDialogLoadingPurchase.isShowing()) {
            ((MainActivity) ctx).prgDialogLoadingPurchase.dismiss();
        }

        if (((MainActivity) ctx).session.isLoggedIn()) {



//Note:we receive login fragment when we use webview since webview user is not logged in. So we do not
            //need to clear cookies here to ensure user is logged out. Also here we may need cookies after sign in with native login
            ((MainActivity) ctx).logout();//30-08-2018 old  ((MainActivity) ctx).session.logoutUser();
//clearing variables safely in logout()
            //30-08-2018 old  ((MainActivity) ctx).updateDrawerLayout();
        }

        Button button = (Button) layout_v.findViewById(R.id.email_sign_in_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("email_sign_in_button", "onclick");
                if (StaticMethods.isNetworkAvailable((MainActivity)ctx)) {
                    email = ((EditText) layout_v.findViewById(R.id.email)).getText().toString();
                    password = ((EditText) layout_v.findViewById(R.id.password)).getText().toString();
                    if ((email.trim().length() != 0) && (password.trim().length() != 0)) {

                        if (StaticMethods.isValidEmail(email)) {
                            try {
                                //login for Add to wishlist
                                //login for checkout
                                //login for other cases
                                // set a variable in Loginfragment
                                //to identify which place is calling login
                                //then call relevant login method according to that variable

                                //24-9-2018 old Log.d("loginMethod", "loginMethod" + Variables.loginMethod);
                                if (Variables.loginMethod == 0) {
                                  //13-9-2018 old need  ((MainActivity) ctx).prgdialogChkout = new ProgressDialog(((MainActivity) ctx));

                                    if (!((((MainActivity) ctx).prgdialogChkout).isShowing())) {
                                       //13-9-2018 old need ((MainActivity) ctx).prgdialogChkout.setCancelable(false);
                                        ((MainActivity) ctx).prgdialogChkout.show();
                                    }

                                    Variables.loginMethod = -1;

                                    //18-12-2018 s

                                    ((MainActivity) ctx).username= email;
                                    ((MainActivity) ctx).password=password;
                                    //18-12-2018 e
                                    ((MainActivity) ctx).customerloginForCheckout(email, password);
                                } else if (Variables.loginMethod == 1) {
//27-08-2018 s
                                   // ((MainActivity) ctx).prgdialog_wishlist = new ProgressDialog(((MainActivity) ctx));
                                    if (!((((MainActivity) ctx).prgdialog_wishlist).isShowing())) {
                                        ((MainActivity) ctx).prgdialog_wishlist.setCancelable(false);
                                        ((MainActivity) ctx).prgdialog_wishlist.show();
                                    }
//27-08-2018 e
                                    Variables.loginMethod = -1;
                                    //18-12-2018 s

                                    ((MainActivity) ctx).username= email;
                                    ((MainActivity) ctx).password=password;
                                    //18-12-2018 e
                                    ((MainActivity) ctx).customerloginWishList(email, password);
                                } else {
                                    Variables.loginMethod = -1;

                                    //18-12-2018 s

                                    ((MainActivity) ctx).username= email;
                                    ((MainActivity) ctx).password=password;
                                    //18-12-2018 e

                                   //30-08-2018 old ((MainActivity) ctx).customerlogin(email, password);
                                    ((MainActivity) ctx).customerloginWithoutSavePasssword(email, password);


                                }


                            } catch (Exception e) {
                                e.printStackTrace();

                                StaticMethods.sendCrashReport(((MainActivity) ctx), e);
                            }

                        } else {
                            ((EditText) layout_v.findViewById(R.id.email)).setError(getString(R.string.invalidEmail));

                        }

                    } else {
                        if (email.trim().length() == 0) {
                            ((EditText) layout_v.findViewById(R.id.email)).setError(getString(R.string.emptyEmail));
                        }
                        if (password.trim().length() == 0) {
                            ((EditText) layout_v.findViewById(R.id.password)).setError(getString(R.string.emptyPassword));
                        }
                    }
                } else {
                    StaticMethods.noConnection((MainActivity)ctx);
                }
            }
        });

        return layout_v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        layout_v=null;//11-9-2018
        ctx=null;//11-9-2018
    }
}