package app.gaincity.app_gain_city.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Variables;

public class StoreMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter {
    View v;
    Context ctx;
    RelativeLayout mapDetail;
    TextView tvDname, tvDaddress, tvDtelno, tvDhours;
    public static final String TAG = "StoreMapFragment";
    LinearLayout llroute;
    private Context mContext;
    private SupportMapFragment supportMapFragment;


    @Override
    public void onMapReady(final GoogleMap map) {

        if (map != null) {
            Variables.gmap = map;
            map.setInfoWindowAdapter(this);
            float zoomLevel = 10.0f; //This goes up to 21
            if (Variables.StoreMapData.size() != 0) {
                LatLng ltlg = new LatLng(Double.parseDouble(Variables.StoreMapData.get(9).getlatitude()),
                        Double.parseDouble(Variables.StoreMapData.get(9).getlongitude()));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(ltlg, zoomLevel));


                int rc = ActivityCompat.checkSelfPermission((StoreMapActivity) ctx, Manifest.permission.ACCESS_FINE_LOCATION);
                int rc2 = ActivityCompat.checkSelfPermission((StoreMapActivity) ctx, Manifest.permission.ACCESS_COARSE_LOCATION);
                if ((rc == PackageManager.PERMISSION_GRANTED) && (rc2 == PackageManager.PERMISSION_GRANTED)) {
                    //24-9-2018 old  Log.d("PERMISSION_GRANTED", "PERMISSION_GRANTED");
                    map.setMyLocationEnabled(true);
                    map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                        @Override
                        public void onMyLocationChange(Location arg0) {
                            // TODO Auto-generated method stub
                            Variables.currentLocation = new LatLng(arg0.getLatitude(), arg0.getLongitude());
                        }
                    });


                } else {
                    final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                    ActivityCompat.requestPermissions((StoreMapActivity) ctx, permissions, StoreMapActivity.HANDLE_LOCATION);
                }

                for (StoreListitem item : Variables.StoreMapData) {

                    Marker mker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(item.getlatitude()),
                                    Double.parseDouble(item.getlongitude())))
                            .snippet(item.getname() + "::" + item.getaddress() + "::" + item.gettelno() + "::" +
                                    item.getopening() + " - " + item.getclosing() + "::" + item.getlatitude() + "::" +
                                    item.getlongitude())
                    );

                    Variables.markerList.add(mker);
                }
            }
        }

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        mapDetail.setVisibility(View.VISIBLE);
        String snippet = marker.getSnippet();
        String[] arrstr = snippet.split("::");
        View v = getLayoutInflater().inflate(R.layout.marker_popup, null);
        TextView tvname = ((TextView) v.findViewById(R.id.name));
        tvname.setText(arrstr[0]);
        tvDname.setText(arrstr[0]);
        tvDaddress.setText(arrstr[1]);
        tvDtelno.setText("Tel:" + arrstr[2]);
        Variables.selectedMarkerPhone = arrstr[2];
        tvDhours.setText(arrstr[3]);
        Variables.selectedMarker = new LatLng(Double.parseDouble(arrstr[4]), Double.parseDouble(arrstr[5]));

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        v = null;
        ctx = null;
        mapDetail = null;
        tvDname = null;
        tvDaddress = null;
        tvDtelno = null;
        tvDhours = null;
        llroute = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.store_map, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018
        Variables.markerList = new ArrayList<>();//22-10-2018
        //v2 s
        // Getting reference to the SupportMapFragment
        ctx = (StoreMapActivity) getActivity();
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        tvDname = ((TextView) v.findViewById(R.id.name));
        tvDaddress = ((TextView) v.findViewById(R.id.address));
        tvDtelno = ((TextView) v.findViewById(R.id.telno));
        tvDhours = ((TextView) v.findViewById(R.id.hours));
        mapDetail = ((RelativeLayout) v.findViewById(R.id.mapDetail));
        llroute = ((LinearLayout) v.findViewById(R.id.llroute));

        llroute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Variables.currentLocation!=null) {
                    String uri = "http://maps.google.com/maps?f=d&hl=es&saddr=" + Variables.currentLocation.latitude + "," + Variables.currentLocation.longitude + "&daddr=" + Variables.selectedMarker.latitude +
                            "," + Variables.selectedMarker.longitude;
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
                else {
                    Variables.requestDirection=true;
                    final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                    ActivityCompat.requestPermissions((StoreMapActivity) ctx, permissions, StoreMapActivity.HANDLE_LOCATION);
                }


            }
        });

        TextView txtcall = ((TextView) v.findViewById(R.id.txtcall));
        txtcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((StoreMapActivity) ctx).callGaincity(Variables.selectedMarkerPhone);
            }
        });

        return v;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}