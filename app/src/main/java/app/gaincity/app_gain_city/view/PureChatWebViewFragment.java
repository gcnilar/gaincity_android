package app.gaincity.app_gain_city.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Variables;

public  class PureChatWebViewFragment extends DialogFragment {
    static PureChatWebViewFragment newInstance() {
        return new PureChatWebViewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.alert_dialog_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        WebView wv = v.findViewById(R.id.webView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new MyWebViewClient());
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setBuiltInZoomControls(true);

        // enable for pure chat needs to access storage
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setDatabaseEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            wv.getSettings().setDatabasePath("/data/data/" + wv.getContext().getPackageName() + "/databases/");
        }
        wv.requestFocus(View.FOCUS_DOWN);

        String url =  "https://app.purechat.com/w/bt6eh"; //https://app.purechat.com/w/btyxbz
        wv.loadUrl(url);


        return v;
    }

    private class MyWebViewClient extends WebViewClient {

        ProgressDialog pd = new ProgressDialog(getActivity());

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            // TODO show you progress image
            super.onPageStarted(view, url, favicon);
            pd.setTitle("Loading");
            pd.setMessage("Wait while loading...");
            pd.setCancelable(false); // disable dismiss by tapping outside of the dialog
            pd.show();
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            // TODO hide your progress image
            super.onPageFinished(view, url);
            pd.dismiss();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    }
}