package app.gaincity.app_gain_city.view;

import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;//16-10-2018 old  android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Variables;


public class ErrorFragment extends Fragment {
    View v;
    public static final String TAG = "ErrorFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout=0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        v=null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.error_layout, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String strError = bundle.getString("error", "");
            ImageView internetimg = (ImageView) v.findViewById(R.id.no_iternet_img);
            if (strError.equals(getResources().getString(R.string.no_internet))) {

                internetimg.setVisibility(View.VISIBLE);
            } else {
                internetimg.setVisibility(View.INVISIBLE);
            }

            TextView tverr = (TextView) v.findViewById(R.id.errText);
            tverr.setText(strError);
        }
    }
}