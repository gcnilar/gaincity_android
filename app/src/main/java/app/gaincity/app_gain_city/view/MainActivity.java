package app.gaincity.app_gain_city.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;//16-10-2018 old android.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;//16-10-2018 old  android.app.FragmentTransaction;
//24-9-2018 old import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
//24-9-2018 old import android.graphics.Color;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
//24-9-2018 old import android.support.annotation.RequiresPermission;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextWatcher;
//24-9-2018 old import android.util.Log;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;

//import android.util.Log;
//import android.util.Log;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.Credentials;
import com.google.android.gms.auth.api.credentials.CredentialsClient;
import com.google.android.gms.auth.api.credentials.CredentialsOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
//24-9-2018 old import java.io.File;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
//24-9-2018 old import java.util.concurrent.atomic.AtomicInteger;

import app.gaincity.app_gain_city.Barcode.BarcodeCaptureFragment;
import app.gaincity.app_gain_city.Barcode.BarcodeGraphicTracker;
import app.gaincity.app_gain_city.BuildConfig;
import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Session.SessionManager;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;
import app.gaincity.app_gain_city.service.apiclient.ApiClient;
import app.gaincity.app_gain_city.service.apiclient.ApiInterface;
import app.gaincity.app_gain_city.service.model.CartResponse;
import app.gaincity.app_gain_city.service.model.CategoryResponse;
import app.gaincity.app_gain_city.service.model.MemberResponse;
import multilist.ItemInformation;
import multilist.MultiListAdapter;
import multilist.MultiLevelListView;
import multilist.OnItemClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import app.gaincity.app_gain_city.service.model.BaseItem;

import static android.os.Build.VERSION.SDK_INT;


public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BarcodeGraphicTracker.BarcodeUpdateListener {
    CredentialsClient mCredentialsClient;
    Menu mMenu;
    EditText searchTxt;//3-9-2018
    SplitToolbar toolbar;
    EditText email2;
    EditText edittxtpassword2;
    public TextView textCartItemCount = null;////7-9-2018 old, txtusername;
    //23-08-2018 old public ImageView loadingBadge=null;
    DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout relRefresh;
    CustomProgressDialog prgCancelled, prgdialog, prgdialogsearch, prgdialogLogout, prgdialog_noti, prgdialog_wishlist, dialogNativeLogout;
    CustomProgressDialog prgdialogAdding, prgDialogLoadingPurchase, prgDialogLnitialLoad;//12-7-2018 from MainFragment
    CustomProgressDialog prgDialogLoading, prgdialogHome, prgdialogMenu, prgdilogStore, prgdilogBtn;//,prgdialogCommon
    RelativeLayout menuRelLayout;
    LinearLayout menuLoginLayout;
    RelativeLayout menuLogoutLayout;
    String password;
    String username;
    public Credential credential;
    GoogleApiClient mCredentialsApiClient;
    SessionManager session;
    boolean mIsResolving;
    boolean isInsideSave;
    private static final int RC_CREDENTIALS_READ = 23;
    private static final int RC_CREDENTIALS_SAVE = 22;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 21;
    CustomProgressDialog prgdialogChkout;
    multilist.MultiLevelListView expListView;
    final int RC_SAVE = 1;
    CheckBox chksavePassword;
    boolean calledGroup3 = false;
    private static final int HANDLE_CALL_PHONE_PERM = 4;//RC_HANDLE_CAMERA_PERM = 2
    public static final int HANDLE_LOCATION = 5;
    RelativeLayout parentlvExp;//12-9-2018

    private Button mEmailSignInBtn, mCreateNewAccount; // created by karthikpks on 17 may 2020

    //3-9-2018 s
    //create a counter to count the number of instances of this activity
    // public static AtomicInteger activitiesLaunched = new AtomicInteger(0);
    //3-9-2018 e

    public void clearCookies() {//To ensure user is not logged in in webview
        if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //  Log.d(C.TAG, "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            // Log.d(C.TAG, "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(this);
            // cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            // cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //29-10-2018 s
        try {

            float fontScale = getResources().getConfiguration().fontScale;
            //Log.d("fontScale","fontScale"+fontScale+(fontScale!=1.0));
            if (fontScale != 1.0) {

                Resources resources = getResources();
                Configuration configuration = resources.getConfiguration();

                DisplayMetrics metrics = getResources().getDisplayMetrics();
                WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
                wm.getDefaultDisplay().getMetrics(metrics);
                metrics.scaledDensity = (float) 1.0 * metrics.density;//1.0 =fontScale
                configuration.densityDpi = (int) (metrics.densityDpi * 1.0);//1.0=fontScale
                configuration.fontScale = (float) 1.0;
                getApplicationContext().createConfigurationContext(configuration);//v1 TODO:NEED?
                createConfigurationContext(configuration);


            }
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(this, e);
        }
        //29-10-2018 e


        //3-9-2018 s
        //if launching will create more than one
        //instance of this activity, bail out
        // if (activitiesLaunched.incrementAndGet() > 1) { finish(); }

        // if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
        //  Log.d("broughtFront", "broughtFront");
        //may be here we can use old state's variables, sessions, only new instance of webview
        //and progress dialogs required, but when we use new webview instance our
        //login session may not be available there

        // Activity was brought to front and not created,
        // Thus finishing this will get us to the last viewed activity
        //finish();
        // return;

        //   }
//else{
        //3-9-2018 e

        resetVariables_onCreateOnly();//30-08-2018
        resetVariables_Logout_onCreate();//30-08-2018

        //21-9-2018 old prgdialogCommon = new CustomProgressDialog(this, R.drawable.prg_spinner);//19-9-2018
        prgdialog = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need new ProgressDialog(this);
        prgdialogsearch = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need  new ProgressDialog(this);
        prgdialogLogout = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need  new ProgressDialog(this);
        prgdialog_noti = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need new ProgressDialog(this);
        prgDialogLoading = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need new ProgressDialog(this);
        prgdialogAdding = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need new ProgressDialog(this);
        prgDialogLoadingPurchase = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need  new ProgressDialog(this);
        prgDialogLnitialLoad = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need new ProgressDialog(this);
        prgdialog_wishlist = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need  new ProgressDialog(this);
        dialogNativeLogout = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need  new ProgressDialog(this);//28-08-2018
        prgCancelled = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 old need  new ProgressDialog(this);//12-09-2018
        prgdialogHome = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018
        prgdialogMenu = new CustomProgressDialog(this, R.drawable.prg_spinner);//25-9-2018
        prgdialogChkout = new CustomProgressDialog(this, R.drawable.prg_spinner);//13-9-2018 TODO:NEED TEST WAS NOT INITIALIZING HERE PREVIOUSLY
        prgdilogStore = new CustomProgressDialog(this, R.drawable.prg_spinner);//18-10-2018
        prgdilogBtn = new CustomProgressDialog(this, R.drawable.prg_spinner);//22-10-2018


        Variables.oncreateCalled = true;

        //1-11-2018 s
        if (StaticMethods.isNetworkAvailable(this)) {
            //1-11-2018 e
            VersionCodeAsyncTask versionCodeTask = new VersionCodeAsyncTask(MainActivity.this, this, Variables.URL_HOME + "mobileapi/android/version");
            versionCodeTask.execute();
        }//1-11-2018
        CredentialsOptions options = new CredentialsOptions.Builder()
                .forceEnableSaveDialog()
                .build();

        mCredentialsClient = Credentials.getClient(this, options);
        session = new SessionManager(getApplicationContext());
        //24-9-2018 old Log.d("onCreateMM27", "onCreateMM27" + session.isSavedPassword());
        //24-9-2018 old Log.d("loadNotificationURLCC", "loadNotificationURLCC");
        Thread.UncaughtExceptionHandler handleAppCrash =
                new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread thread, Throwable ex) {
                        //24-9-2018 old Log.e("error", ex.getCause().toString());
                        //send email here
                        ex.printStackTrace();
                        StaticMethods.sendCrashReport(MainActivity.this, ex);
                    }
                };
        Thread.setDefaultUncaughtExceptionHandler(handleAppCrash);
        setContentView(R.layout.activity_main);
        chksavePassword = (CheckBox) findViewById(R.id.chksavePassword);
        addListenerOnChkSavePassword();
        GoogleSignInOptions gso =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()

                        .build();
        mCredentialsApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
//                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mCredentialsApiClient.connect();
        loadNavMenu();


        MainActivity.PrepareMenuAsyncTask task = new MainActivity.PrepareMenuAsyncTask(MainActivity.this);
        task.execute();


        deleteCache(this);//26-9-2018 old TODO:NEED to remove session data?// 24-9-2018

        //27-08-2018 s
        clearCookies();

//27-08-2018 e
        if (StaticMethods.isNetworkAvailable(this)) {

            if (session.isSavedPassword()) {
                /*13-9-2018 old need  prgdialog.setMessage(getResources().getString(R.string.loadingDialog));
                prgdialog.setCancelable(false);*/
                if (!(prgdialog.isShowing())) {//17-9-2018
                    prgdialog.show();
                }
                Variables.isInsideSessionLogin = true;
                Variables.loginPost = true;

                //18-12-2018 s

                username= session.getEmail();
                password=session.getPassword();
                //18-12-2018 e

                customerloginAuto(session.getEmail(), session.getPassword());//9-11-2018


                //30-08-2018 OLD assigned but never used  Variables.doingloginAction = true;
                /*9-11-2018 old s
                try {//10-9-2018
                    Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" +URLEncoder.encode(session.getEmail(), "UTF-8")  + "&login[password]=" + session.getPassword() + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018

                }//10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                   catch (Exception e){
                    e.printStackTrace();

                    StaticMethods.sendCrashReport(MainActivity.this, e);
                }
                //10-9-2018 e
                //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);

                loadFragmentUrl(Variables.fragmentUrl);
                updateDrawerLayout();
                9-11-2018 old need e*/
            } else {

                logout();//clearCookies() called above in oncreate, resetting all variables done in begining of oncreate //30-08-2018
                /*session.logoutUser();
                updateDrawerLayout();*/
                /*30-08-2018 old Variables.calledUpgrade = false;
                Variables.fragmentUrl = "";
                Variables.numcartItems = 0;
                //30-08-2018 old  Variables.taskLoadingCount = 0;
                Variables.cusToken = "";
                Variables.loginPost = false;
                Variables.useSmartLock = false;
                Variables.isInsideSessionLogin = false;
                Variables.isActiveinEmail = false;
                Variables.isActiveinPassword = false;
                Variables.isTokenSuccess = false;*/
                //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = false;

                setupBadge();
                if (!(prgdialogHome.isShowing())) {//17-9-2018
                    prgdialogHome.show();//13-9-2018
                }
                session.setlastURL(Variables.getBaseUrl());
                loadFragmentUrl(Variables.getBaseUrl());
                //10-7-2018 old need TODO:ADD SmartLock  requestCredential();
            }
        } else {
           /* Intent intent = getIntent();//23-08-2018
            if ((intent.hasExtra("loadNotificationURL"))) {//23-08-2018
                Variables.notificationURL=
            }*/

            /*if (session.isLoggedIn()) {
                session.logoutUser();
            }

            updateDrawerLayout();*/

            /*if() {
                StaticMethods.noConnection(this);
            }*/
        }


        //}//3-9-2018
    }

    public void loadNavMenu() {
        toolbar = (SplitToolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //7-9-2018 old txtusername = (TextView) findViewById(R.id.username);
        email2 = (EditText) findViewById(R.id.email);
        email2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Variables.isActiveinEmail = true;
                Variables.isActiveinPassword = false;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edittxtpassword2 = (EditText) findViewById(R.id.password);
        edittxtpassword2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Variables.isActiveinEmail = false;
                Variables.isActiveinPassword = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        relRefresh = (RelativeLayout) getLayoutInflater().inflate(R.layout.layout_refresh, null, false);
        //1-11-2018 s
        /*LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         relRefresh = (RelativeLayout) inflater.from(this).inflate(R.layout.layout_refresh,
                null);
        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.FILL_PARENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
        relRefresh.setLayoutParams(layoutParams);*/
        //1-11-2018 e

        menuRelLayout = (RelativeLayout) findViewById(R.id.menu_layout);
        menuLoginLayout = (LinearLayout) findViewById(R.id.menu_login_layout);
        menuLogoutLayout = (RelativeLayout) findViewById(R.id.menu_logout_layout);

        expListView = (multilist.MultiLevelListView) findViewById(R.id.lvExp);
        parentlvExp = (RelativeLayout) findViewById(R.id.parentlvExp);//12-9-2018

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        toggle.setHomeAsUpIndicator(R.drawable.icon_menu); //set your own

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });


        toggle.syncState();

        navigationView.setItemIconTintList(null);//5-5 stop changing color of item icons

        //BEFORE CALL updateDrawerLayout logout user if no network
        if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
            if (!session.isLoggedIn()) {
//27-08-2018 s
       /* 30-08-2018 old        clearCookies();

//27-08-2018 e
                session.logoutUser();*/

                logoutAndClearCookies();//30-08-2018
            }
        }

        mEmailSignInBtn = (Button) findViewById(R.id.email_sign_in_button);
        mCreateNewAccount = (Button) findViewById(R.id.create_new_account);
        mEmailSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLogin(view);
            }
        });
        mCreateNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createaccount(view);
            }
        });

        updateDrawerLayout();
    }


    public void requestCredential() {
        CredentialRequest mCredentialRequest = new CredentialRequest.Builder()
                .setPasswordLoginSupported(true)

                .build();


        Auth.CredentialsApi.request(mCredentialsApiClient, mCredentialRequest).setResultCallback(
                new ResultCallback<CredentialRequestResult>() {
                    @Override
                    public void onResult(@NonNull CredentialRequestResult credentialRequestResult) {
                        if (credentialRequestResult.getStatus().isSuccess()) {

                            //24-9-2018 old Log.d("Credentialmdd", "Credentialmdd" + credentialRequestResult.getCredential());


                        } else {
                            //24-9-2018 old Log.d("Credentialmdd1", "Credentialmdd1" + credentialRequestResult.getStatus());

                            if (credentialRequestResult.getStatus().getStatusCode() == CommonStatusCodes.RESOLUTION_REQUIRED) {
                                resolveResult(credentialRequestResult.getStatus(), RC_CREDENTIALS_READ);

                            }

                        }
                    }
                });
    }

    @SuppressLint("RestrictedApi")//27-08-2018 added-Need test
    private void saveCredential() {
        if (credential != null) {


            mCredentialsClient.save(credential).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        //24-9-2018 old Log.d("SAVE: OK", "SAVE: OK");
                        Toast.makeText(getApplicationContext(), "Credentials saved", Toast.LENGTH_SHORT).show();
                        credential = null;
                        return;
                    }

                    Exception e = task.getException();
                    if (e instanceof ResolvableApiException) {
                        // Try to resolve the save request. This will prompt the user if
                        // the credential is new.
                        ResolvableApiException rae = (ResolvableApiException) e;
                        try {


                            rae.startResolutionForResult(MainActivity.this, RC_SAVE);
                        } catch (IntentSender.SendIntentException e2) {
                            e.printStackTrace();

                            StaticMethods.sendCrashReport(MainActivity.this, e);
                            // Could not resolve the request
                            credential = null;
                            //24-9-2018 old Log.e("Failedsend resolution.", "Failedsend resolution." + e2);
                            // Toast.makeText(getApplicationContext(), "Save failed", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        // Request has no resolution
                        credential = null;
                        //24-9-2018 old Log.e("Failedsend resolution.", "Failedsend resolution." + e);
                        // Toast.makeText(getApplicationContext(), "Save failed", Toast.LENGTH_SHORT).show();
                    }
                }

            });


        }
    }

    private void deleteCredential() {//used during testing
        credential = new Credential.Builder("mm@gaincity.com")
                .setPassword("Password1")
                .build();
        if (credential != null) {
            Auth.CredentialsApi.delete(mCredentialsApiClient, credential)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                        }
                    });

            credential = null;
        }
    }


    @Override
    public void onBackPressed() {


        //24-9-2018 old Log.d("goback", "goback");
        if (drawer.isDrawerOpen(GravityCompat.START)) {//if drawer is open, closes it
            drawer.closeDrawer(GravityCompat.START);
            return;
        }


        final FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
        MainFragment fragment = (MainFragment) fm.findFragmentByTag(MainFragment.TAG);


        //24-08-2018 s
        LoginFragment loginFragment = (LoginFragment) fm.findFragmentByTag(LoginFragment.TAG);

//12-11-2018 s
        FamilyCardFragment familyCardFragment = (FamilyCardFragment) fm.findFragmentByTag(FamilyCardFragment.TAG);
        if ((familyCardFragment != null) && (familyCardFragment.isVisible())) {
            setHeaderPortrait();
            fm.beginTransaction().remove(familyCardFragment).commit();

            String lastURLExceptLogin = session.getlastURLExceptLogin();
            loadFragmentUrl(lastURLExceptLogin);

        }


        //12-11-2018 e
        else if ((loginFragment != null) && (loginFragment.isVisible())) {
            FragmentTransaction trans = fm.beginTransaction();
            trans.remove(fragment);//FOR login, we need to restart MainFragment,so we remove old MainFragment here????
            trans.commit();

            fm.beginTransaction().remove(loginFragment).commit();
            String lastURLExceptLogin = session.getlastURLExceptLogin();
            loadFragmentUrl(lastURLExceptLogin);

        } else {
            //24-08-2018 e


            BarcodeCaptureFragment barcodeCaptureFragment = (BarcodeCaptureFragment) fm.findFragmentByTag(BarcodeCaptureFragment.TAG);


            if ((barcodeCaptureFragment != null) && (barcodeCaptureFragment.isVisible())) {

                ////7-9-2018 old StaticMethods.showMsgCloseApp(this);
//return;//7-9-2018
            }


            ErrorFragment errorFragment = (ErrorFragment) fm.findFragmentByTag(ErrorFragment.TAG);


            if ((errorFragment != null) && (errorFragment.isVisible())) {

                StaticMethods.showMsgCloseApp(this);

            } else if ((fragment != null) && ((fragment.isVisible()) && ((fragment.webView.getUrl() != null) && ((fragment.webView.getUrl().equals(Variables.appendToHomeUrl(""))))))) {//10-7-2018 replaced above line  //if((Variables.strWebviewUrl.equals(Variables.URL_HOME))||(Variables.strWebviewUrl.equals(Variables.URL_HOME2))){//28-5-2018 old not ok  if(Variables.isInsideHome){


                //displays dialog to ask user if they want to close the application
                StaticMethods.showMsgCloseApp(this);


            } else if ((fragment != null) && fragment.isVisible()) {
//24-08-2018 s
            /*if((fragment.webView.getUrl().equals(Variables.appendToHomeUrl("checkout/")))){
                Log.d("lastURLExceptBB","lastURLExceptBB");
                loadFragmentUrl(session.getlastURLExceptLogin());
            }
            else {*/
                //24-08-2018 e
                boolean goback = ((MainFragment) fragment).webView.canGoBack();

                if (goback) {

                    // Variables.backPressedWebview=true;//24-08-2018
                    //12-9-2018 old need?? fragment.goBack();
                    ((MainFragment) fragment).webView.goBack();//12-9-2018
                } else {
                    StaticMethods.showMsgCloseApp(this);

                }
                //}

            } else if (fm.getBackStackEntryCount() >= 1) { //returns to the previous fragment
                fm.popBackStack();
                //24-9-2018 old Log.d("popBackStack", "popBackStack");

            } else {
                //displays dialog to ask user if they want to close the application
                StaticMethods.showMsgCloseApp(this);

            }
        }

    }

    @Override
    public void onDestroy() {
        //24-9-2018 old Log.d("Lifecycle", "onDestroy");
        //3-9-2018 s
        //remove this activity from the counter
        //  activitiesLaunched.getAndDecrement();//TODO:CHECK BUT THIS WILL NOT BE CALLED EVERY TIME
        //AS ondestroy not getting called sometimes
        //3-9-2018 e
        super.onDestroy();
//27-08-2018 s
        clearCookies();

//27-08-2018 e
        //this gets called when finish() called but when remove app from background,this doesnt get
        // called-  session.logoutUser() - purpose of having this here??? ;//23-08-2018
//Note:can not use logoutAndClearCookies(), since updating UI not possible here, so I keep this old code
        //12-9-2018 s
        //to avoid leaks

        //13-11-2018 s
        prgdialogHome = null;
        prgdialogMenu = null;
        //13-11-2018 e

        mCredentialsClient = null;
        mMenu = null;
        searchTxt = null;
        toolbar = null;
        email2 = null;
        edittxtpassword2 = null;
        textCartItemCount = null;
        drawer = null;
        navigationView = null;
        relRefresh = null;
        prgdialog = null;
        prgdialogsearch = null;
        prgCancelled = null;
        prgdialogLogout = null;
        prgdialog_noti = null;
        prgdialog_wishlist = null;
        dialogNativeLogout = null;
        prgdialogAdding = null;
        prgDialogLoading = null;
        prgDialogLoadingPurchase = null;
        prgDialogLnitialLoad = null;
        menuRelLayout = null;
        menuLoginLayout = null;
        menuLogoutLayout = null;
        credential = null;
        mCredentialsApiClient = null;
        session = null;
        prgdialogChkout = null;
        prgdilogStore = null;//31-10-2018
        prgdilogBtn = null;//31-10-2018
        expListView = null;
        chksavePassword = null;
        parentlvExp = null;
        //12-9-2018 e
    }

    @Override

    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.main, menu);


        textCartItemCount = (TextView) menu.findItem(R.id.action_cart).getActionView().findViewById(R.id.cart_badge);
        //23-08-2018 old loadingBadge = (ImageView) menu.findItem(R.id.action_cart).getActionView().findViewById(R.id.cartLoadingImg);
        searchTxt = (EditText) menu.findItem(R.id.searchBar).getActionView().findViewById(R.id.searchTxt);//3-9-2018

        mMenu = menu;


        setupBadge();

        //3-9-2018 s

        searchTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    doSearch();
                    return true;
                }
                return false;
            }
        });
        //3-9-2018 e


        return true;
    }

    public void setupBadge() {


        try {
            if (textCartItemCount != null) {//23-08-2018 old if ((textCartItemCount != null)&& (loadingBadge != null)){
                //23-08-2018 oldloadingBadge.setVisibility(View.GONE);


                if (Variables.numcartItems == 0) {


                    //hides badge when zero items
                    if (textCartItemCount.getVisibility() != View.GONE) {
                        textCartItemCount.setVisibility(View.GONE);
                        textCartItemCount.setSelected(true); //For ellipsize="marquee" output will be  auto sliding from right to left


                    }
                } else {
                    //sets text in badge and shows badge
                    if (Variables.numcartItems > 99) {
                        Variables.numcartItems = 99;
                        textCartItemCount.setText(String.valueOf(Variables.numcartItems) + "+");
                    } else {
                        textCartItemCount.setText(String.valueOf(Variables.numcartItems));
                    }


                    if (textCartItemCount.getVisibility() != View.VISIBLE) {
                        textCartItemCount.setVisibility(View.VISIBLE);

                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onStart() {
        //24-9-2018 old Log.d("onStart", "onStart");
        //19-4-2018 old  mCredentialsApiClient.connect();//TODO:CHECK WHETHER REQUIRED 8-6-2018
        Intent intent = getIntent();//23-08-2018
        // Log.d("onStart","onStart"+intent.getExtras().get("image"));


        super.onStart();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

//24-9-2018 old Log.d("onNewIntent","onNewIntent"+intent.hasExtra("loadNotificationURL"));
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //method by push notification without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //method by push notification  without getting action to set to -1

        //Note:when launch by app icon caused black screen error since getStringExtra code of this log- Log.d("loadNotificationURL33", "loadNotificationURL33" +intent.getStringExtra("loadNotificationURL")+ intent.getExtras().get("image"));


        if (intent.hasExtra("loadNotificationURL")) {
            final String notificationURL = intent.getStringExtra("loadNotificationURL");
            intent.removeExtra("loadNotificationURL");
            if (!prgdialog_noti.isShowing()) {
//  13-9-2018 old need               prgdialog_noti.setMessage(getResources().getString(R.string.loadingDialog));
//                prgdialog_noti.setCancelable(false);
                prgdialog_noti.show();
            }
            // Toast.makeText(this, "opened app's msg", Toast.LENGTH_SHORT).show();


            if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                // StaticMethods.noConnection(this);
                final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Error")
                        .setMessage(getResources().getString(R.string.noInternetDialog))
                        //.setNeutralButton("OK", null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        })
                        .setCancelable(false)
                        .create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                        ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {
                            Variables.fragmentUrl = notificationURL;
                            loadFragmentUrl(notificationURL);
                            alertDialog.dismiss();
                        }


                        //}


                    }
                });

            } else {
                Variables.fragmentUrl = notificationURL;
                loadFragmentUrl(notificationURL);
            }


        } else if (intent.hasExtra("image")) {
            final String notificationURL = intent.getExtras().get("image").toString();
            intent.removeExtra("image");
            if (!prgdialog_noti.isShowing()) {
                /*13-9-2018 old need prgdialog_noti.setMessage(getResources().getString(R.string.loadingDialog));
                prgdialog_noti.setCancelable(false);*/
                prgdialog_noti.show();
            }
            // Toast.makeText(this, "opened app's msg", Toast.LENGTH_SHORT).show();


            if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                // StaticMethods.noConnection(this);
                final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Error")
                        .setMessage(getResources().getString(R.string.noInternetDialog))
                        //.setNeutralButton("OK", null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        })
                        .setCancelable(false)
                        .create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                        ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {
                            Variables.fragmentUrl = notificationURL;
                            loadFragmentUrl(notificationURL);
                            alertDialog.dismiss();
                        }


                        //}


                    }
                });

            } else {
                Variables.fragmentUrl = notificationURL;
                loadFragmentUrl(notificationURL);
            }

        }

    }


    @Override
    protected void onResume() {


        super.onResume();

        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //method by push notification without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //method by push notification  without getting action to set to -1
        //24-9-2018 old Log.d("loadNotificationURL9991", "loadNotificationURL9991");
        Intent intent = getIntent();

        //21-9-2018 ASUS noti err- when app closed- got
        // nullhttps://www.gaincity.com/catalog/category/1299/weekly-best-deals
        //here.
        //FCMIntentService , onMessageReceived not called when app closed, so it hasn't loadNotificationURL
        //noti icon used here is "noti_icon_plain"
        //in manifest - android:resource="@drawable/noti_icon_plain"
        //it should be smaller-but test it with other devices
        //24-9-2018 old Log.d("loadNotificationURL999", "loadNotificationURL999" +intent.getStringExtra("loadNotificationURL")+ intent.getStringExtra("image"));


        if ((intent != null) && (intent.hasExtra("image"))) {

            //When app is in background this one is called and activity get recreated
            final String notificationURL = intent.getStringExtra("image");
            intent.removeExtra("image");

            if (!prgdialog_noti.isShowing()) {
                /*13-9-2018 old need prgdialog_noti.setMessage(getResources().getString(R.string.loadingDialog));
                prgdialog_noti.setCancelable(false);*/
                prgdialog_noti.show();
            }
            //24-9-2018 old Log.d("loadNotificationURLPP", "loadNotificationURLPP"+session.isSavedPassword());
            if (session.isSavedPassword()) {

                if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                    // StaticMethods.noConnection(this);
                    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Error")
                            .setMessage(getResources().getString(R.string.noInternetDialog))
                            //.setNeutralButton("OK", null)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            })
                            .setCancelable(false)
                            .create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                            ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                            if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {
                                Variables.notificationURL = notificationURL;
                                loadFragmentUrl(notificationURL);
                                alertDialog.dismiss();
                            }


                            //}


                        }
                    });

                } else {
                    Variables.notificationURL = notificationURL;
                }


            } else {
//27-08-2018 s
                /*30-08-2018 old clearCookies();

//27-08-2018 e
session.logoutUser();//need logout here otherwise show login data

                updateDrawerLayout();*/

                //need logout here otherwise show login data
                logoutAndClearCookies();//30-08-2018


                Variables.fragmentUrl = notificationURL;

                if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                    // StaticMethods.noConnection(this);
                    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Error")
                            .setMessage(getResources().getString(R.string.noInternetDialog))
                            //.setNeutralButton("OK", null)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            })
                            .setCancelable(false)
                            .create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                            ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                            if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {
                                loadFragmentUrl(notificationURL);
                                alertDialog.dismiss();
                            }


                            //}


                        }
                    });

                } else {
                    loadFragmentUrl(notificationURL);
                }

            }

            // Toast.makeText(this, "closedApp's msg", Toast.LENGTH_SHORT).show();
        } else if ((intent != null) && (intent.hasExtra("loadNotificationURL"))) {
            final String notificationURL = intent.getStringExtra("loadNotificationURL");
            intent.removeExtra("loadNotificationURL");
            //Toast.makeText(this, "opened22 app's msg", Toast.LENGTH_SHORT).show();

            if (!prgdialog_noti.isShowing()) {
                /*13-9-2018 old need prgdialog_noti.setMessage(getResources().getString(R.string.loadingDialog));
                prgdialog_noti.setCancelable(false);*/
                prgdialog_noti.show();
            }

            Variables.fragmentUrl = notificationURL;
            // loadFragmentUrl(notificationURL);
            if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                // StaticMethods.noConnection(this);
                final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Error")
                        .setMessage(getResources().getString(R.string.noInternetDialog))
                        //.setNeutralButton("OK", null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        })
                        .setCancelable(false)
                        .create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                        ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {
                            loadFragmentUrl(notificationURL);
                            alertDialog.dismiss();
                        }


                        //}


                    }
                });

            } else {
                loadFragmentUrl(notificationURL);
            }


        } else {

            if (Variables.oncreateCalled == true) {
                if ((Variables.loginPost == false) && (session.isSavedPassword())) {

                    if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                        // StaticMethods.noConnection(this);
                        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Error")
                                .setMessage(getResources().getString(R.string.tryLoginDialog))
                                //.setNeutralButton("OK", null)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                    }
                                })
                                .setCancelable(false)
                                .create();
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.show();

                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                                ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {


                                    /*13-9-2018 old need prgdialog.setMessage(getResources().getString(R.string.loadingDialog));
                                    prgdialog.setCancelable(false);*/
                                    prgdialog.show();
                                    Variables.isInsideSessionLogin = true;
                                    Variables.loginPost = true;
                                    //30-08-2018 OLD assigned but never used  Variables.doingloginAction = true;

                                    //18-12-2018 s

                                    username= session.getEmail();
                                    password=session.getPassword();
                                    //18-12-2018 e


                                    customerloginAuto(session.getEmail(), session.getPassword());//9-11-2018

                                    /*9-11-2018 old
                                    try {//10-9-2018
                                        Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" +URLEncoder.encode(session.getEmail(), "UTF-8")   + "&login[password]=" + session.getPassword() + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018

                                    } //10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                   catch (Exception e){
                                        e.printStackTrace();

                                        StaticMethods.sendCrashReport(MainActivity.this, e);
                                    }
                                    //10-9-2018 e

                                    //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);

                                    loadFragmentUrl(Variables.fragmentUrl);
                                    updateDrawerLayout();*/


                                    alertDialog.dismiss();
                                }


                                //}


                            }
                        });

                    }


                } else if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Error")
                            .setMessage(getResources().getString(R.string.checkInternetDialog))
                            //.setNeutralButton("OK", null)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            })

                            .setCancelable(false)
                            .create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //if (StaticMethods.isNetworkAvailable(MainActivity.this)) {
                            ConnectivityManager connectivityManager = (ConnectivityManager) (MainActivity.this).getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                            if ((activeNetworkInfo != null) && (activeNetworkInfo.isConnected())) {


                                loadFragmentUrl(Variables.URL_HOME);

                                alertDialog.dismiss();
                            }


                            //}


                        }
                    });
                }
            }
        }
        Variables.oncreateCalled = false;


    }


    @Override
    protected void onPause() {
        //24-9-2018 old Log.d("Lifecycle", "onPause");
        // stop cookie syncing for pre-lollipop devices


        super.onPause();
    }

    @Override
    public void onStop() {
        //24-9-2018 old Log.d("Lifecycle", "onStop");
        mCredentialsApiClient.disconnect();//TODO:CHECK WHETHER REQUIRED 8-6-2018
        super.onStop();
    }

    private void resolveResult(Status status, int requestCode) {
        if (mIsResolving) {
            //24-9-2018 old Log.w("resolveResult", "resolveResult: already resolving.");
            return;
        }
        //24-9-2018 old Log.d("resolveResult", "resolveResult Resolving: " + status);
        if (status.hasResolution()) {
            //24-9-2018 old Log.d("resolveResult", "resolveResult: RESOLVING");
            try {
                status.startResolutionForResult(this, requestCode);
                mIsResolving = true;
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(MainActivity.this, e);
                //24-9-2018 old Log.e("resolveResult", "resolveResult: Failed to send resolution.", e);
            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(MainActivity.this, e);

            }
        } else {
            //24-9-2018 old Log.d("resolveResult", "resolveResult goToContent");
            ;
            ;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //24-9-2018 old Log.d("onActivityResult", "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);


        if (requestCode == RC_SAVE) {
            if (resultCode == RESULT_OK) {

                //24-9-2018 old Log.d("SAVE: OK", "SAVE: OK");
                Toast.makeText(this, "Credentials saved", Toast.LENGTH_SHORT).show();
            } else {

                //24-9-2018 old Log.e("SAVE: Canceled by user", "SAVE: Canceled by user");
            }
            credential = null;
        } else if (requestCode == RC_CREDENTIALS_READ) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    //login when credential is retrieved successfully
                    credential = data.getParcelableExtra(Credential.EXTRA_KEY);


                    //10-5-2018 ???? need to remove deleteCache otherwise activity is null for next login  deleteCache(getApplicationContext());
                    // Sign the user in with information from the Credential.
                    //customerlogin(credential.getId(), credential.getPassword());
                    onCredentialRetrieved(credential);

                }
            } else {
                //24-9-2018 old Log.e("onActivityResult", "onActivityResult: NOT OK");
                //  setSignInEnabled(true);
            }
            mIsResolving = false;//30-10
        } else if (requestCode == RC_CREDENTIALS_SAVE) {
            //24-9-2018 old Log.d("onActivityResult22", "onActivityResult22: " + resultCode);
            if (resultCode == RESULT_OK) {
                //saved credential successfully
                //24-9-2018 old Log.d("onActivityResult22", "onActivityResult: OK");// credential.getId()


            } else {
                //24-9-2018 old Log.e("onActivityResult22", "onActivityResult Failed");
            }
            //24-9-2018 old Log.d("resolveResult", "resolveResult goToContent");
            mIsResolving = false;
            isInsideSave = false;
        }

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {


        super.onConfigurationChanged(newConfig);


    }


    public static class VersionCodeAsyncTask extends AsyncTask<Void, Void, JSONObject> {
        private WeakReference<Context> mContext;
        private boolean errorOccured = false;
        private String jsonURL = "";
        Activity mActivity;

        public VersionCodeAsyncTask(Activity a, Context c, String s) {
            mActivity = a;
            mContext = new WeakReference<>(c);
            jsonURL = s;
        }

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected JSONObject doInBackground(Void... params) {
            Context context = mContext.get();
            if (context == null) return null;
            try {

                URLConnection urlConn = null;
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(jsonURL);
                    urlConn = url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                    StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuffer.append(line);
                    }

                    return new JSONObject(stringBuffer.toString());
                } catch (Exception ex) {
                    //24-9-2018 old Log.e("App", "yourDataTask", ex);
                    return null;
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(context, e);

                errorOccured = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            final Context context = mContext.get();
            if (context == null) return;
            if (!errorOccured) {
                if (response != null) {
                    try {


                        int versionCode = BuildConfig.VERSION_CODE;

                        int minSupportedVersion = Integer.parseInt(response.getString("minAndroidVersionCode"));
//24-9-2018 old Log.d("minSupportedVersion","minSupportedVersion"+minSupportedVersion);
                        //7-9-2018 s
                        if (minSupportedVersion == -1) {
                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                            alertDialog.setMessage("Gaincity App is under construction");//26-12-2018
                            //26-12-2018 old alertDialog.setTitle("Gaincity App is under construction");

                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close App",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            dialog.dismiss();
                                            mActivity.finish();
                                        }
                                    });
                            alertDialog.show();
                        }
                        //7-9-2018 e

                        else if (versionCode < minSupportedVersion) {//to force update change
                            //minSupportedVersion to latest versionCode

                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                            alertDialog.setMessage("Update our app now to enjoy the latest features.");
                            //  alertDialog.setTitle("Update our app now to enjoy the latest features.");

                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Update",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            dialog.dismiss();
                                            Intent updateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=app.gaincity.app_gain_city"));//15-10-2018 old ("market://details?id=app.gaincity.app_gaincity"));
// Verify it resolves
                                            PackageManager packageManager = context.getPackageManager();
                                            List<ResolveInfo> activities = packageManager.queryIntentActivities(updateIntent, 0);
                                            boolean isIntentSafe = activities.size() > 0;

// Start an activity if it's safe
                                            if (isIntentSafe) {
                                               //need? dialog.dismiss();//16-1-2019
                                                context.startActivity(updateIntent);

                                                mActivity.finish();
                                                System.exit(0);
                                            }

                                        }
                                    });
                            alertDialog.show();

                            // TextView tv = (TextView) alertDialog.findViewById(R.id.title);//23-10-2018
                            // tv.setSingleLine(false);//23-10-2018

                            Intent updateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=app.gaincity.app_gain_city"));//15-10-2018 old ("market://details?id=app.gaincity.app_gain_city" ));
                        }

                    } catch (JSONException ex) {
                        //24-9-2018 old Log.e("App", "Failure", ex);
                    }
                }
            }
        }
    }


    public static class RegisterAsyncTask extends AsyncTask<Void, Void, Void> {
        private WeakReference<Context> mContext;
        private boolean errorOccured = false;


        public RegisterAsyncTask(Context c, String s) {
            mContext = new WeakReference<>(c);

        }

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(Void... params) {
            Context context = mContext.get();
            if (context == null) return null;
            try {

                FirebaseMessaging.getInstance().subscribeToTopic("Test");//TODO:NEED SUBSCRIBE?
                //24-9-2018 old Log.d("FCM", "subscribed to Test");
            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(context, e);
                //24-9-2018 old Log.d("FCM", "Error registering on server");
                errorOccured = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Context context = mContext.get();
            if (context == null) return;
            if (!errorOccured) {

                //24-9-2018 old Log.d("FCM", "registered on database");
            }
        }
    }


    public void goHome(View v) {
        //loads homepage
        //when gaincity logo is clicked
        loadFragmentUrl(Variables.appendToHomeUrl(""));
    }

    public void showCart(View v) {
        //loads cart webpage
        // when cart icon is clicked
        loadFragmentUrl(Variables.appendToHomeUrl("checkout/cart/"));


    }

    public void showMemCard(View v) {

        if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
            StaticMethods.noConnection(this);
        } else {

            if (!(session.isLoggedIn())) {

                drawer.openDrawer(GravityCompat.START);
            } else {
                //23-10-2018 s

                if (!(prgdilogBtn.isShowing())) {
                    prgdilogBtn.show();
                }
//23-10-2018 e

                loadFragmentUrl(Variables.appendToHomeUrl("customer/account/"));
            }
        }


    }

    public void scanBarcode(View v) {

        if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
            StaticMethods.noConnection(this);
        } else {


            //creates barcodecapturefragment to scan QRcode
            // when scan icon is clicked
            FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();


            BarcodeCaptureFragment fragment = (BarcodeCaptureFragment) fm.findFragmentByTag(BarcodeCaptureFragment.TAG);


            if (fragment == null) {

                fragment = new BarcodeCaptureFragment();
            } else if (!fragment.isVisible()) {
                fm.popBackStack(BarcodeCaptureFragment.TAG, 0);
            } else {
                //already scanning
            }

            if (!fragment.isAdded()) {
                ft.replace(R.id.category_cont, fragment, BarcodeCaptureFragment.TAG);

                ft.addToBackStack(BarcodeCaptureFragment.TAG);
                ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                ft.commit();
            }

        }
    }

    public void openChat(View v) {
//        DialogFragment newFragment = PureChatWebViewFragment.newInstance();
//        newFragment.show(getSupportFragmentManager(), "dialog");

        Intent myIntent = new Intent(this, PureChatWebViewActivity.class);
        startActivity(myIntent);
    }

    @Override
    public void onBarcodeDetected(Barcode barcode) {

        final Barcode m_barcode = barcode;
        if (isValidBarcode(barcode.displayValue)) {
            //loads webpage from scanned QRcode

            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Variables.qrcodeRedirect = true;
                    Variables.fragmentUrl = m_barcode.displayValue;
                    //30-08-2018 old never assigned to true   Variables.needNewFragment = false;
                    loadFragmentUrl(Variables.fragmentUrl);

                }
            });


        } else {
            //shows dialog to notify user of invalid QR code
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Gain City App")
                    .setPositiveButton(R.string.ok, null)
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {

                        }
                    });
            if (barcode.displayValue.equals("")) {
                //no QR code captured
                builder.setMessage(getString(R.string.barcode_failure));
            } else {
                //not url and not gaincity product
                builder.setMessage(getString(R.string.barcode_error));
            }

            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    builder.show();
                }
            });
        }

    }

    private boolean isValidBarcode(String barcode) {

        return Patterns.WEB_URL.matcher(barcode).matches() && barcode.contains("gaincity");///
    }


    public void closeDrawer() {
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    private static class PrepareMenuAsyncTask extends AsyncTask<Void, Void, Void> {

        private WeakReference<MainActivity> mActivity;

        public PrepareMenuAsyncTask(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        protected void onPreExecute() {
            //Log.d("MenuList3", "MenuList3");
            MainActivity activity = mActivity.get();
            if (activity != null) {
                if (activity.relRefresh.getParent() == null) {
                    //24-9-2018 old Log.d("relRefreshnul","relRefreshnul");
                    //12-9-2018 old need activity.navigationView.addView(activity.relRefresh);
                    activity.parentlvExp.removeView(activity.expListView);    //12-9-2018
                    //12-9-2018 s
                    RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);//1-11-2018 old wrapcontent
                    //1-11-2018 commented params3.addRule(RelativeLayout.CENTER_IN_PARENT);//7-9-2018
                    activity.relRefresh.setLayoutParams(params3);
                    //12-9-2018 e
                    activity.parentlvExp.addView(activity.relRefresh);    //12-9-2018

                }
                activity.relRefresh.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                activity.relRefresh.findViewById(R.id.refresh_btn).setVisibility(View.GONE);
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
            //Log.d("MenuList", "MenuList");
            try {
                MainActivity activity = mActivity.get();
                if (activity != null) {
                    activity.prepareListData();
                }
            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(mActivity.get(), e);

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Log.d("PrepareListData11","PrepareListData11");
            MainActivity activity = mActivity.get();
            if (activity != null) {


            }
        }
    }

    private void prepareListDataFailed() {
        relRefresh.findViewById(R.id.progressBar).setVisibility(View.GONE);

        //1-11-2018 s
        /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);//7-9-2018
        relRefresh.findViewById(R.id.refresh_btn).setLayoutParams(params);*/
        //1-11-2018 e
        relRefresh.findViewById(R.id.refresh_btn).setVisibility(View.VISIBLE);
    }

    public void btnRefresh(View v) {

        //Log.d("MenuList4", "MenuList4");
        if (!(StaticMethods.isNetworkAvailable(this))) {
            StaticMethods.noConnection(this);
        } else {
            //Log.d("MenuList2", "MenuList2");
            MainActivity.PrepareMenuAsyncTask task = new MainActivity.PrepareMenuAsyncTask(MainActivity.this);
            task.execute();
        }
    }


    public void prepareListData() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CategoryResponse>> catcall = apiService.getArrCategories();
if(catcall!=null){//4-1-2018
        catcall.enqueue(new Callback<ArrayList<CategoryResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<CategoryResponse>> call, Response<ArrayList<CategoryResponse>> response) {
                if (!(response.isSuccessful())) {
                    prepareListDataFailed();
                    //Log.d("PrepareListData", "PrepareListData failed" + response.message());//26-9-2018
                    return;
                }
                // Log.d("PrepareListData ok","PrepareListData ok");
                ArrayList<CategoryResponse> arr = response.body();

                Variables.categories = arr;

                MainActivity.ListAdapter listAdapter = new MainActivity.ListAdapter();

                expListView.setAdapter(listAdapter);
                expListView.setOnItemClickListener(mOnItemClickListener);

                listAdapter.setDataItems(DataProvider.getInitialItems());


                updateDrawerLayout();
                if (relRefresh.getParent() != null) {

                    parentlvExp.removeView(relRefresh);  //12-9-2018 old navigationView.removeView(relRefresh);

                }

            }

            @Override
            public void onFailure(Call<ArrayList<CategoryResponse>> call, Throwable t) {
                prepareListDataFailed();
                //24-9-2018 old Log.d("PrepareListData", "getCategories failed" + t.getMessage());
            }
        });
    //4-1-2018 s
    }
    else{
    prepareListDataFailed();
}
//4-1-2018 e
    }


    public void btnLogin(View v) {//when login button is clicked
        //hides keyboard
        Log.d("btnLogin", "onclick");
        /*InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);*/

        hideKeyboard(); // -- added by karthikpks on 17 may 2020

        if (StaticMethods.isNetworkAvailable(this)) {


            Variables.useSmartLock = false;
            Variables.cusToken = "";
            Variables.numcartItems = 0;

            setupBadge();
            credential = null;

            updateDrawerLayout();


            //checks if the edittexts are empty
            if (email2.getText() != null && edittxtpassword2.getText() != null && email2.getText().toString().trim().length() != 0 && edittxtpassword2.getText().toString().trim().length() != 0) {


                String password, username;
                password = edittxtpassword2.getText().toString();
                username = email2.getText().toString();

                //28-08-2018 S
                edittxtpassword2.setText("");
                email2.setText("");
                //28-08-2018 E

                if (StaticMethods.isValidEmail(username)) {
                    //18-12-2018 s

                    this.username= username;
                    this.password=password;
                    //18-12-2018 e
                    customerlogin(username, password);

                } else {
                    email2.setError(getString(R.string.invalidEmail));
                }


            } else {

                if (email2.getText().toString().trim().length() == 0) {
                    email2.setError(getString(R.string.emptyEmail));
                }

                if (edittxtpassword2.getText().toString().trim().length() == 0) {
                    edittxtpassword2.setError(getString(R.string.emptyPassword));
                }


            }
        } else {
            if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
                StaticMethods.noConnection(this);
            }
        }

    }


    public void customerloginWishList(String strusername, String strpassword) {
        password = strpassword;
        username = strusername;

        //24-9-2018 old Log.d("customerlogin", "customerlogin : " + password + username);

        ApiInterface apiService_get_token = null;
        Call<String> call = null;


        apiService_get_token =
                ApiClient.getcustomerLoginClient().create(ApiInterface.class);


        //10-9-2018 old  call = apiService_get_token.login(username, password);//TODO:ADD AUTO LOGIN (credential.getId(),credential.getPassword())
        //10-9-2018 s
        try {
            call = apiService_get_token.login(URLEncoder.encode(username, "UTF-8"), password);
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
        //10-9-2018 e
if(call!=null){
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();///
                    //Log.d("LoginTT", "LoginTT" + response.body());

                    //creates credential

                    if (Variables.useSmartLock == false) {// saving smart lock when first time login
                        credential = new Credential.Builder(username)
                                .setPassword(password)

                                .build();
                        //11-9-2018 OLD TODO:ADD FOR SMART LOCK  saveCredential();
                    }


                    Variables.cusToken = "Bearer " + response.body();

                    /*if (Variables.savePassword) {
                        session.createLoginSession(username, password, Variables.cusToken);

                        Variables.savePassword = false;
                        chksavePassword.setChecked(false);
                    } else {*/

                    session.setIsLoggedIn(true);//NOTE: we do not save password, just do login
                    ///}


                    Variables.loginPost = true;
                    //30-08-2018 OLD assigned but never used  Variables.doingloginAction = true;
                    try {//10-9-2018
                        Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" + URLEncoder.encode(username, "UTF-8") + "&login[password]=" + password + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018
                    }//10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                    catch (Exception e) {
                        e.printStackTrace();

                        StaticMethods.sendCrashReport(MainActivity.this, e);
                    }
                    //10-9-2018 e
                    session.setlastURL(Variables.URL_HOME + "wishlist/");//29-08-2018 To redirect to wishlist page after login
                    //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);


                    loadFragmentUrl(Variables.fragmentUrl);
                    updateDrawerLayout();
                } else {
                    credential = null;
                    Toast.makeText(getApplicationContext(), getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();


                    //24-9-2018 old Log.d("Login", "Login Failed: You did not sign in correctly or your account is temporarily disabled." + response.errorBody().toString());

                    if (prgdialog_wishlist.isShowing()) {
                        prgdialog_wishlist.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                //24-9-2018 old Log.e("Login", "onResponse fail" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                credential = null;

                if (prgdialog_wishlist.isShowing()) {
                    prgdialog_wishlist.dismiss();
                }

            }
        });
        //4-1-2018 s
    }
    else{
    Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
    credential = null;

    if (prgdialog_wishlist.isShowing()) {
        prgdialog_wishlist.dismiss();
    }
}
        //4-1-2018 e
    }

    //30-08-2018 s
    //Note: after logout with an error I clear cookies
//for normal logout I dont clear cookie

    public void logout() {//WithoutClearCookies

        resetVariables_Logout_onCreate();
        session.logoutUser();
        credential = null;
        updateDrawerLayout();
    }

    public void logoutAndClearCookies() {

        /*resetVariables_Logout_onCreate();
        session.logoutUser();
       // setupBadge();
        credential = null;
        updateDrawerLayout();*/

        logout();
        clearCookies();//After logout finish clear cookies
    }

    public void resetVariables_Logout_onCreate() {//called when logout or oncreate
       //18-12-2018 s
        username=null;//use for silent signin when token expired for api
        password=null;//use for silent signin when token expired for api
        //18-12-2018 e

        Variables.cusToken = "";
        Variables.loginPost = false;
        Variables.useSmartLock = false;
        Variables.isActiveinEmail = false;
        Variables.isActiveinPassword = false;
        Variables.isTokenSuccess = false;
        Variables.savePassword = false;
        Variables.loadCheckout = false;
        //No relation to logout or oncreate, related to the page we are loading- Variables.insideCheckout=false;//11-9-2018
    }

    public void resetVariables_onCreateOnly() {//called when oncreate only
        Variables.numcartItemsBeforeClickCheckout = 0;
        Variables.numcartItems = 0;
        Variables.isInsideSessionLogin = false;
        Variables.categories = null;
        Variables.showGroup1 = true;
        Variables.clickedMenuItem = false;
        Variables.qrcodeRedirect = false;
        Variables.stoppedUsingJs = false;
        Variables.loginMethod = -1;
        // Variables.notificationURL = "";
        Variables.forgotPasswordEmail = "";//11-9-2018
        Variables.GaincityPhoneNum = "";//11-9-2018
        //21-9-2018 old Variables.clickedGroup=false;//19-9-2018
    }

    //Note: customerlogin Without SavePasssword option
    public void customerloginWithoutSavePasssword(String strusername, String strpassword) {
        password = strpassword;
        username = strusername;

        //24-9-2018 old Log.d("customerlogin", "customerlogin : " + password + username);

        ApiInterface apiService_get_token = null;
        Call<String> call = null;


        apiService_get_token =
                ApiClient.getcustomerLoginClient().create(ApiInterface.class);


        //10-9-2018 old  call = apiService_get_token.login(username, password);//TODO:ADD AUTO LOGIN (credential.getId(),credential.getPassword())
        //10-9-2018 s
        try {
            call = apiService_get_token.login(URLEncoder.encode(username, "UTF-8"), password);
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
        //10-9-2018 e
        if(call!=null){//4-1-2018
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();///
                    //24-9-2018 old Log.d("Login", "Login Successful : " + response.body());

                    //creates credential

                    if (Variables.useSmartLock == false) {// saving smart lock when first time login
                        credential = new Credential.Builder(username)
                                .setPassword(password)

                                .build();
                        //11-9-2018 OLD TODO:ADD FOR SMART LOCK  saveCredential();//13-4-2018 e
                    }


                    Variables.cusToken = "Bearer " + response.body();

                   /* if (Variables.savePassword) {
                        session.createLoginSession(username, password, Variables.cusToken);

                        Variables.savePassword = false;
                        chksavePassword.setChecked(false);
                    } else {*/

                    session.setIsLoggedIn(true);//NOTE: we do not save password, just do login
                    // }


                    Variables.loginPost = true;
                    //30-08-2018 OLD assigned but never used   Variables.doingloginAction = true;
                    try {
                        Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" + URLEncoder.encode(username, "UTF-8") + "&login[password]=" + password + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018
                    }
//10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                    catch (Exception e) {
                        e.printStackTrace();

                        StaticMethods.sendCrashReport(MainActivity.this, e);
                    }
                    //10-9-2018 e
                    //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);


                    loadFragmentUrl(Variables.fragmentUrl);
                    updateDrawerLayout();
                } else {
                    credential = null;
                    Toast.makeText(getApplicationContext(), getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();

                    //24-9-2018 old Log.d("Login", "Login Failed: You did not sign in correctly or your account is temporarily disabled." + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                //24-9-2018 old Log.e("Login", "onResponse fail" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                credential = null;

            }
        });
        //4-1-2018 s
    }
    else{
            Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
            credential = null;
        }
        //4-1-2018 e
    }

    //30-08-2018 e

    //9-11-2018 s
    public void customerloginAuto(String strusername, String strpassword) {// from menu with save password option

        if (!(prgDialogLoading.isShowing())) {
            /*13-9-2018 old need prgDialogLoading.setMessage(getResources().getString(R.string.loadingDialog));
            prgDialogLoading.setCancelable(false);*/
            prgDialogLoading.show();

        }

        password = strpassword;
        username = strusername;

        //24-9-2018 old Log.d("customerlogin", "customerlogin : " + password + username);

        ApiInterface apiService_get_token = null;
        Call<String> call = null;


        apiService_get_token =
                ApiClient.getcustomerLoginClient().create(ApiInterface.class);


        //10-9-2018 old call = apiService_get_token.login(username, password);//TODO:ADD AUTO LOGIN (credential.getId(),credential.getPassword())
//10-9-2018 s
        try {
            call = apiService_get_token.login(URLEncoder.encode(username, "UTF-8"), password);
        } catch (Exception e) {
            prgDialogLoading.dismiss();//13-9-2018
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
        //10-9-2018 e

        if(call!=null){ //4-1-2018
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();///
                    //24-9-2018 old Log.d("Login", "Login Successful : " + response.body());
                    //Log.d("LoginTT", "LoginTT" + response.body());//GIVES a token only
                    //creates credential

                    if (Variables.useSmartLock == false) {// saving smart lock when first time login
                        credential = new Credential.Builder(username)
                                .setPassword(password)

                                .build();
                        //11-9-2018 OLD TODO:ADD FOR SMART LOCK  saveCredential();//13-4-2018 e
                    }


                    Variables.cusToken = "Bearer " + response.body();

                    /*9-11-2018 old
                    if (Variables.savePassword) {
                        session.createLoginSession(username, password, Variables.cusToken);

                        Variables.savePassword = false;
                        chksavePassword.setChecked(false);
                    } else {

                        session.setIsLoggedIn(true);//NOTE: we do not save password, just do login
                    }


                    Variables.loginPost = true;*/
                    //30-08-2018 OLD assigned but never used   Variables.doingloginAction = true;
                    try {//10-9-2018
                        Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" + URLEncoder.encode(username, "UTF-8") + "&login[password]=" + password + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018
                    }
//10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                    catch (Exception e) {
                        e.printStackTrace();

                        StaticMethods.sendCrashReport(MainActivity.this, e);
                    }
                    //10-9-2018 e
                    //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);


                    loadFragmentUrl(Variables.fragmentUrl);
                    updateDrawerLayout();
                } else {
                    credential = null;
                    //10-9-2018 old need TODO:ADD Toast.makeText(getApplicationContext(),  getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();

                    Toast.makeText(getApplicationContext(), getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();

                    prgDialogLoading.dismiss();//13-9-2018
                    //24-9-2018 old Log.d("Login", "Login Failed: You did not sign in correctly or your account is temporarily disabled." + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                //24-9-2018 old Log.e("Login", "onResponse fail" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                credential = null;
                prgDialogLoading.dismiss();//13-9-2018
            }
        });
            //4-1-2018 s
    }
        else{
            Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
            credential = null;
            prgDialogLoading.dismiss();
        }
        //4-1-2018 e
    }
//9-11-2018 e

    //17-12-2018 s

    public void getTokenSilently(String strusername, String strpassword) {

        if (!(prgDialogLoading.isShowing())) {
            prgDialogLoading.show();
        }

        //20-12-2018 password = strpassword;//commented to avoid error- when user logout from cookies but token is valid
        //20-12-2018 username = strusername;//commented to avoid error- when user logout from cookies but token is valid

        ApiInterface apiService_get_token = null;
        Call<String> call = null;

        apiService_get_token =
                ApiClient.getcustomerLoginClient().create(ApiInterface.class);


        //10-9-2018 old call = apiService_get_token.login(username, password);//TODO:ADD AUTO LOGIN (credential.getId(),credential.getPassword())

        try {
            if(((strusername!=null)&&(!(strusername.equals(""))))&&((strpassword!=null)&&(!(strpassword.equals(""))))) {//24-12-2018
                call = apiService_get_token.login(URLEncoder.encode(strusername, "UTF-8"), strpassword);
//4-1-2018 s
if(call!=null){
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            Variables.cusToken = "Bearer " + response.body();
                            prgDialogLoading.dismiss();//17-12-2018
                            Toast.makeText(getApplicationContext(), getString(R.string.tryAgain), Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
                            callWebViewLogoutFromWebView();//logout because it is already logged in //17-12-2018
                            prgDialogLoading.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
                        callWebViewLogoutFromWebView();//logout because it is already logged in //17-12-2018
                        prgDialogLoading.dismiss();
                    }
                });
            }
            else{
    Toast.makeText(getApplicationContext(), getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
    callWebViewLogoutFromWebView();//logout because it is already logged in //17-12-2018
    prgDialogLoading.dismiss();
}
            //4-1-2018 e

            }
            else{
                //4-1-2018 old prgDialogLoading.dismiss();
                //4-1-2018 s
                    Toast.makeText(getApplicationContext(), getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
                    callWebViewLogoutFromWebView();//logout because it is already logged in //17-12-2018
                    prgDialogLoading.dismiss();
                //4-1-2018 e
            }
        } catch (Exception e) {
            prgDialogLoading.dismiss();
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }


   }


    public void customerlogin(String strusername, String strpassword) {// from menu with save password option

        if (!(prgDialogLoading.isShowing())) {
            /*13-9-2018 old need prgDialogLoading.setMessage(getResources().getString(R.string.loadingDialog));
            prgDialogLoading.setCancelable(false);*/
            prgDialogLoading.show();

        }

        password = strpassword;
        username = strusername;

        //24-9-2018 old Log.d("customerlogin", "customerlogin : " + password + username);

        ApiInterface apiService_get_token = null;
        Call<String> call = null;


        apiService_get_token =
                ApiClient.getcustomerLoginClient().create(ApiInterface.class);


        //10-9-2018 old call = apiService_get_token.login(username, password);//TODO:ADD AUTO LOGIN (credential.getId(),credential.getPassword())
//10-9-2018 s
        try {
            call = apiService_get_token.login(username, password);
        } catch (Exception e) {
            prgDialogLoading.dismiss();//13-9-2018
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
        //10-9-2018 e
if(call!=null){//4-1-2018
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();///
                    //24-9-2018 old Log.d("Login", "Login Successful : " + response.body());
                    //Log.d("LoginTT", "LoginTT" + response.body());//GIVES a token only
                    //creates credential

                    if (Variables.useSmartLock == false) {// saving smart lock when first time login
                        credential = new Credential.Builder(username)
                                .setPassword(password)

                                .build();
                        //11-9-2018 OLD TODO:ADD FOR SMART LOCK  saveCredential();//13-4-2018 e
                    }


                    Variables.cusToken = "Bearer " + response.body();
                    //Log.d("Variables.cusToken","Variables.cusToken"+Variables.cusToken);
                    if (Variables.savePassword) {
                        session.createLoginSession(username, password, Variables.cusToken);

                        Variables.savePassword = false;
                        chksavePassword.setChecked(false);
                    } else {

                        session.setIsLoggedIn(true);//NOTE: we do not save password, just do login
                    }


                    Variables.loginPost = true;
                    //30-08-2018 OLD assigned but never used   Variables.doingloginAction = true;
                    try {//10-9-2018
//                        Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" + URLEncoder.encode(username, "UTF-8") + "&login[password]=" + password + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//22-01-2019
                        Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" + username + "&login[password]=" + URLEncoder.encode(password, "UTF-8")  + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018
                    }
//10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                    catch (Exception e) {
                        e.printStackTrace();

                        StaticMethods.sendCrashReport(MainActivity.this, e);
                    }
                    //10-9-2018 e
                    //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);


                    loadFragmentUrl(Variables.fragmentUrl);
                    updateDrawerLayout();
                } else {
                    credential = null;
                    //10-9-2018 old need TODO:ADD Toast.makeText(getApplicationContext(),  getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();

                    Toast.makeText(getApplicationContext(), getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();

                    prgDialogLoading.dismiss();//13-9-2018
                    //24-9-2018 old Log.d("Login", "Login Failed: You did not sign in correctly or your account is temporarily disabled." + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                //24-9-2018 old Log.e("Login", "onResponse fail" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                credential = null;
                prgDialogLoading.dismiss();//13-9-2018
            }
        });
        //4-1-2018 s
    }
    else{
    Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
    credential = null;
    prgDialogLoading.dismiss();
}
    //4-1-2018 e
    }

    public void noticeGuest(View v) {

        try {
            GuestNoticeFragment fragment = new GuestNoticeFragment();
            FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
            android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.category_cont, fragment, GuestNoticeFragment.TAG);
            ft.addToBackStack(GuestNoticeFragment.TAG);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        } catch (Exception e) {

            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }

    }

    public void gobacktoLogin(View v) {


        FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();

        if (fm.getBackStackEntryCount() >= 1) { //returns to the previous fragment
            fm.popBackStack();
            //24-9-2018 old Log.d("popBackStack", "popBackStack");

        }
        /*GuestNoticeFragment guestFragment = (GuestNoticeFragment) fm.findFragmentByTag(GuestNoticeFragment.TAG);
        fm.beginTransaction().remove(guestFragment).commit();*/


    }

    public void continueAsGuest(View v) {
        Variables.numcartItemsBeforeClickCheckout = 0;
        if (StaticMethods.isNetworkAvailable(this)) {
            // 13-9-2018 old need prgdialogChkout = new ProgressDialog(this);
            if (!(prgdialogChkout).isShowing()) {

                /*13-9-2018 old need prgdialogChkout.setMessage(getResources().getString(R.string.loadingDialog));//
                prgdialogChkout.setCancelable(false);*/
                prgdialogChkout.show();
            }

            loadFragmentUrl(Variables.appendToHomeUrl("checkout/"));
        } else {
            StaticMethods.noConnection(this);
        }
    }


    public void createaccount(View v) {
        //loads create account webpage
        //when register button is clicked
//hide keyboard
        if (StaticMethods.isNetworkAvailable(this)) {
            //13-9-2018 old need prgdialogChkout = new ProgressDialog(this);
            if (!(prgdialogChkout).isShowing()) {

               /*//13-9-2018 old need  prgdialogChkout.setMessage(getResources().getString(R.string.loadingDialog));
                prgdialogChkout.setCancelable(false);*/
                prgdialogChkout.show();
            }


            /*InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);*/

            hideKeyboard(); // -- added by karthikpks on 17 may 2020

            loadFragmentUrl(Variables.appendToHomeUrl("customer/account/create/"));
        } else {
            StaticMethods.noConnection(this);
        }
    }

    void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Maxim Dmitriev
         */
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void forgotPassword(View v) {


        //loads forgot password webpage
        //when register button is clicked
        if (StaticMethods.isNetworkAvailable(this)) {
            ////13-9-2018 old need  prgdialogChkout = new ProgressDialog(this);
            if (!(prgdialogChkout).isShowing()) {

                /*//13-9-2018 old need prgdialogChkout.setMessage(getResources().getString(R.string.loadingDialog));
                prgdialogChkout.setCancelable(false);*/
                prgdialogChkout.show();
            }

            loadFragmentUrl(Variables.appendToHomeUrl("customer/account/forgotpassword/"));
        } else {
            StaticMethods.noConnection(this);
        }
    }


    public void familyCard(View v) {
        try {
            //9-11-2018 s
            if (!(prgdialogMenu.isShowing())) {
                prgdialogMenu.show();
            }

            memberInfo();
            //9-11-2018 e

            /*9-11-2018 old
            FamilyCardFragment fragment = new FamilyCardFragment();
            FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.category_cont, fragment, FamilyCardFragment.TAG);
            ft.addToBackStack(FamilyCardFragment.TAG);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();*/

            closeDrawer();//9-11-2018
        } catch (Exception e) {

            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
    }


    public void viewAccount(View v) {
        if (!(prgdilogBtn.isShowing())) {
            prgdilogBtn.show();
        }
        //loads user account webpage
        //when view account button is clicked
//        Variables.loginPost = false;
        loadFragmentUrl(Variables.appendToHomeUrl("customer/account"));
//        String viewAccount =  Variables.URL_HOME + "customer/account/loginPost?login[username]=" + username + "&login[password]=" + password + "&send=&app=1";//Variables.URL_HOME;//fragment.getLastUrl();//11-4-2018
//        loadFragmentUrl(viewAccount);

    }

    public void btnLogout(View v) {


        //TODO:NEED session.setlastURL(Variables.URL_HOME);//8-6-2018

        if (StaticMethods.isNetworkAvailable(this)) {
            if (!(prgdialogLogout.isShowing())) {

                /*13-9-2018 old need  prgdialogLogout.setMessage(getResources().getString(R.string.signoutDialog));//("You are signing out...");
                prgdialogLogout.setCancelable(false);*/
                prgdialogLogout.show();

            }


            loadFragmentUrl(Variables.appendToHomeUrl("customer/account/logout/"));

            logout();//for normal logout I dont clear cookie //30-08-2018

           /*30-08-2018 old session.logoutUser();

            Variables.useSmartLock = false;
            Variables.cusToken = "";
            Variables.numcartItems = 0;
            //Variables.calledUpgrade = false;

            Variables.fragmentUrl = "";

            //30-08-2018 old   Variables.taskLoadingCount = 0;

            Variables.loginPost = false;

            Variables.isInsideSessionLogin = false;
            Variables.isActiveinEmail = false;
            Variables.isActiveinPassword = false;
            Variables.isTokenSuccess = false;*/


            //30-08-2018 OLD assigned but never used  Variables.lastURLcalled = false;


            /*30-08-2018 OLD setupBadge();
            credential = null;

            updateDrawerLayout();*/


        } else {
            StaticMethods.noConnection(this);
        }

    }

    //28-08-2018 s

    public void callWebViewLogoutFromWebView() {
//when user not logged in in native side but logged in webview

        //TODO:NEED session.setlastURL(Variables.URL_HOME);//8-6-2018

        if (StaticMethods.isNetworkAvailable(this)) {
            if (!(prgdialogLogout.isShowing())) {
                /*13-9-2018 old need prgdialogLogout.setMessage(getResources().getString(R.string.signoutDialog));
                prgdialogLogout.setCancelable(false);*/
                prgdialogLogout.show();//after dismiss prgdialogLogout we delete cookies

            }


            loadFragmentUrl(Variables.appendToHomeUrl("customer/account/logout/"));


            /*30-08-2018 old
            Variables.useSmartLock = false;
            Variables.cusToken = "";
            Variables.numcartItems = 0;*/
            //Variables.calledUpgrade = false;

            Variables.fragmentUrl = "";

            //30-08-2018 old  Variables.taskLoadingCount = 0;

           /*30-08-2018 old
            Variables.loginPost = false;

            Variables.isInsideSessionLogin = false;
            Variables.isActiveinEmail = false;
            Variables.isActiveinPassword = false;
            Variables.isTokenSuccess = false;*/


            //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = false;

            resetVariables_Logout_onCreate();//30-08-2018
            //setupBadge();
            credential = null;

            // updateDrawerLayout();


        } else {
            StaticMethods.noConnection(this);
        }

    }

    public void callNativeLogoutFromWebView() {



            /*30-08-2018 old session.logoutUser();

            Variables.useSmartLock = false;
            Variables.cusToken = "";
            Variables.numcartItems = 0;*/
        // Variables.calledUpgrade = false;

        Variables.fragmentUrl = "";

        //30-08-2018 old  Variables.taskLoadingCount = 0;

            /*30-08-2018 old Variables.loginPost = false;

            Variables.isInsideSessionLogin = false;
            Variables.isActiveinEmail = false;
            Variables.isActiveinPassword = false;
            Variables.isTokenSuccess = false;*/


        //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = false;


            /*setupBadge();
            credential = null;

            updateDrawerLayout();*/

        logoutAndClearCookies();//30-08-2018 old  clearCookies();//After logout finish clear cookies

        if (StaticMethods.isNetworkAvailable(this)) {
            //TODO:NEED session.setlastURL(Variables.URL_HOME);//8-6-2018
            if (!(dialogNativeLogout.isShowing())) {
                /*13-9-2018 old need dialogNativeLogout.setMessage(getResources().getString(R.string.signoutDialog));
                dialogNativeLogout.setCancelable(false);*/
                dialogNativeLogout.show();//after dismiss prgdialogLogout we delete cookies

            }
            loadFragmentUrl(Variables.URL_HOME);//This is required since sometimes it goes to login page
        }
    }

    //28-08-2018 e
    public void updateDrawerLayout() {
        //if logged out, displays login layout
        //else displays logout layout


        //24-9-2018 old Log.d("expListView22", "expListView22" + expListView);
        if (!session.isLoggedIn()) {
            if (menuLogoutLayout.getParent() != null) menuRelLayout.removeView(menuLogoutLayout);
            if (menuLoginLayout.getParent() != null) menuRelLayout.removeView(menuLoginLayout);
            if (expListView.getParent() != null) parentlvExp.removeView(expListView);//12-9-2018
            menuRelLayout.removeView(parentlvExp);  //12-9-2018 old need menuRelLayout.removeView(expListView);
            //24-9-2018 old Log.d("expListView222", "expListView222" + expListView);

            menuRelLayout.addView(menuLoginLayout);


            RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //7-9-2018 old need  params3.addRule(RelativeLayout.BELOW, R.id.menu_login_layout);
            params3.addRule(RelativeLayout.ABOVE, R.id.menu_login_layout);//7-9-2018
            params3.addRule(RelativeLayout.ALIGN_PARENT_TOP, R.id.menu_login_layout);//7-9-2018
            parentlvExp.setLayoutParams(params3); //12-9-2018 old need  expListView.setLayoutParams(params3);
            parentlvExp.addView(expListView);  //12-9-2018
            menuRelLayout.addView(parentlvExp); //12-9-2018 old need  menuRelLayout.addView(expListView);

            if (Variables.isActiveinEmail == true) {
                email2.requestFocus();
            } else if (Variables.isActiveinPassword == true) {
                edittxtpassword2.requestFocus();
            }


        } else {
            if (menuLogoutLayout.getParent() != null) menuRelLayout.removeView(menuLogoutLayout);
            if (menuLoginLayout.getParent() != null) menuRelLayout.removeView(menuLoginLayout);
            if (expListView.getParent() != null) parentlvExp.removeView(expListView);//12-9-2018

            menuRelLayout.removeView(parentlvExp); //12-9-2018 old need menuRelLayout.removeView(expListView);
            //24-9-2018 old Log.d("expListView2222", "expListView2222" + expListView);
            menuRelLayout.addView(menuLogoutLayout);


            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //7-9-2018 old need  params.addRule(RelativeLayout.BELOW, R.id.menu_logout_layout);
            params.addRule(RelativeLayout.ABOVE, R.id.menu_logout_layout);//7-9-2018
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, R.id.menu_logout_layout);//7-9-2018
            parentlvExp.setLayoutParams(params);//12-9-2018 old need  expListView.setLayoutParams(params);
            parentlvExp.addView(expListView);  //12-9-2018
            menuRelLayout.addView(parentlvExp); //12-9-2018 old need  menuRelLayout.addView(expListView);

            if (Variables.isActiveinEmail == true) {
                email2.requestFocus();
            } else if (Variables.isActiveinPassword == true) {
                edittxtpassword2.requestFocus();
            }

            //7-9-2018 old  txtusername.setText(session.getEmail());


        }

    }


    public void loadFragmentUrl(String s) {
        Log.d("mOnItemClickListener lo", s);
        Variables.fragmentUrl = s;

        FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
        MainFragment fragment = (MainFragment) fm.findFragmentByTag(MainFragment.TAG);
        FragmentTransaction ft = fm.beginTransaction();

        if (fragment == null) {   //30-08-2018 old never assigned to true  if ((fragment == null) || (Variables.needNewFragment)) {

            //creates new fragment
            //30-08-2018 old never assigned to true     Variables.needNewFragment = false;
            fragment = new MainFragment();

        } else {
//24-9-2018 old Log.d("testMainFragment1","testMainFragment1");
            //reuses fragment

            //25-2-2019 s
           /* */

           //Added same code for testing live app 25-2-2019 s
            /*fragment = new MainFragment();
            ft.replace(R.id.category_cont, fragment, MainFragment.TAG);

            //11-9-2018 old need - Removed to not allow activity  ft.addToBackStack(MainFragment.TAG);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();*/
           // boolean fragmentPopped = fm.popBackStackImmediate(MainFragment.TAG, 0);
            //Log.d("fragmentPopped","fragmentPopped"+fragmentPopped);

            if (!fragment.isAdded()) {
                ft.replace(R.id.category_cont, fragment, MainFragment.TAG);

                ft.addToBackStack(MainFragment.TAG);
                ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
//                ft.commit();
            }

            fragment.loadFragmentUrl();


            //Added same code for testing live app 25-2-2019 e
            //25-2-2019 e


           //25-2-2019  old needed before-(ded same code for testing live app 25-2-2019 ") fragment.loadFragmentUrl();


        }
        if (!fragment.isAdded()) {
            //24-9-2018 old Log.d("testMainFragment2","testMainFragment2");
            ft.replace(R.id.category_cont, fragment, MainFragment.TAG);

            //11-9-2018 old need - Removed to not allow activity  ft.addToBackStack(MainFragment.TAG);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        }
        if (!Variables.clickedMenuItem) {
            //Note:if "Forgot your password?" button clicked, we need to close menu drawer
            //at the time we click that button Variables.clickedMenuItem is false
            //so at that time this block will be called.
            //if we do not use this Variables.clickedMenuItem,
            //then each time we come to loadFragmentUrl, we need to close drawer.
            //Then again if we do like that, it creates a problem since we do not need to close drawer when
            //we load web pages from menu item click. At that time we load page
            //in background and do not close drawer
            closeDrawer();
        } else {
            //Note:use to not to close drawer when menu item clicked.
            //we close drawer when leave item clicked inside onItemClicked method
            Variables.clickedMenuItem = false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//7-9-2018 s
        //19-10-2018 s
        if (requestCode == HANDLE_LOCATION) {//5
            requestPermissionsResult(requestCode, permissions, grantResults);

        }
        //19-10-2018 e
        if (requestCode == HANDLE_CALL_PHONE_PERM) {//4
            requestPermissionsResult(requestCode, permissions, grantResults);

        } else if (requestCode == 2) {//RC_HANDLE_CAMERA_PERM = 2
            //7-9-2018 e

            //pass results of request camera permission to barcodecapturefragment
            FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
            BarcodeCaptureFragment fragment = ((BarcodeCaptureFragment) fm.findFragmentByTag(BarcodeCaptureFragment.TAG));
            if (fragment != null)
                fragment.requestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                //24-9-2018 old Log.i("checkPlayServices", "This device is not supported.");
            }
            return false;
        }
        return true;
    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();//???? -need to delete cache otherwise when next time open it may have previous session data, even user didn't save password
            deleteDir(dir);
        } catch (Exception e) {

            e.printStackTrace();

            StaticMethods.sendCrashReport(context, e);
        }
    }

    public static boolean deleteDir(File dir) {//TODO:CHECK WHETHER REQUIRED 8-6-2018
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    private void onCredentialRetrieved(Credential credential) {
        String accountType = credential.getAccountType();


        Variables.useSmartLock = true;

        // Sign the user in with information from the Credential.
        customerlogin(credential.getId(), credential.getPassword());


    }

    public void searchGCweb(View v) {

        if (!(StaticMethods.isNetworkAvailable(this))) {//1-11-2018 - !( added
            StaticMethods.noConnection(this);
        } else {
            try {

                if (Variables.showGroup1 == true) {
                    mMenu.setGroupVisible(R.id.group_1, false);
                    Variables.showGroup1 = false;
                    TextView searchTxt = (TextView) mMenu.findItem(R.id.searchBar).getActionView().findViewById(R.id.searchTxt);

                    mMenu.setGroupVisible(R.id.group_2, true);
                    searchTxt.requestFocus();
                } else if (Variables.showGroup1 == false) {

                    ((TextView) mMenu.findItem(R.id.searchBar).getActionView().findViewById(R.id.searchTxt)).setText("");//5-6-2018


                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    mMenu.setGroupVisible(R.id.group_1, true);
                    Variables.showGroup1 = true;

                    mMenu.setGroupVisible(R.id.group_2, false);
                }


            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(MainActivity.this, e);


            }
        }
    }

    public void doSearch(View v) {


        TextView searchTxt = (TextView) mMenu.findItem(R.id.searchBar).getActionView().findViewById(R.id.searchTxt);

        String strsearchTxt = searchTxt.getText().toString();
        if ((strsearchTxt != null) && (!strsearchTxt.isEmpty())) {

            Variables.showGroup1 = true;
            //30-08-2018 old never assigned to true  Variables.needNewFragment = false;
//31-10-2018 s
            if (!((prgdialogsearch.isShowing()))) {
                prgdialogsearch.show();
            }
            //31-10-2018 e

            loadFragmentUrl(Variables.appendToHomeUrl("catalogsearch/result/?q=" + strsearchTxt)); //10-7-2018 old need  loadFragmentUrl(Variables.URL_HOME_WITH_INDEX + "catalogsearch/result/?q=" + strsearchTxt);
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

        } else {
            Toast.makeText(this, "Please add search text!", Toast.LENGTH_SHORT).show();

        }
    }

    //3-9-2018 s
    public void doSearch() {


        TextView searchTxt = (TextView) mMenu.findItem(R.id.searchBar).getActionView().findViewById(R.id.searchTxt);

        String strsearchTxt = searchTxt.getText().toString();
        if ((strsearchTxt != null) && (!strsearchTxt.isEmpty())) {

            Variables.showGroup1 = true;
            //30-08-2018 old never assigned to true  Variables.needNewFragment = false;

//31-10-2018 s
            if (!((prgdialogsearch.isShowing()))) {
                prgdialogsearch.show();
            }
            //31-10-2018 e
            loadFragmentUrl(Variables.appendToHomeUrl("catalogsearch/result/?q=" + strsearchTxt)); //10-7-2018 old need  loadFragmentUrl(Variables.URL_HOME_WITH_INDEX + "catalogsearch/result/?q=" + strsearchTxt);
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

        } else {
            Toast.makeText(this, "Please add search text!", Toast.LENGTH_SHORT).show();

        }
    }

    private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClicked(MultiLevelListView parent, View view, Object item, ItemInformation itemInfo) {

            Log.d("mOnItemClickListener", ((BaseItem) item).getName());
            Log.d("mOnItemClickListener", ((BaseItem) item).geturlpath());

            if ((((BaseItem) item).getName().equals("Store Location"))) {
                Log.d("mOnItemClickListener if", ((BaseItem) item).geturlpath());
                Variables.clickedMenuItem = true;

//                StoreTabsFragment fragment = new StoreTabsFragment();
//                FragmentManager fm = getSupportFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//
//                if (!fragment.isAdded()) {
//                    ft.replace(R.id.category_cont, fragment, StoreTabsFragment.TAG);
//
//                    ft.addToBackStack(StoreTabsFragment.TAG);
//                    ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
//                    ft.commit();
//                }

//                ft.replace(R.id.category_cont, fragment, StoreTabsFragment.TAG);
//                ft.addToBackStack(StoreTabsFragment.TAG);
//                ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
//                ft.commit();

                Intent intent = new Intent(getApplicationContext(), StoreMapActivity.class);
                startActivity(intent);
                closeDrawer();


            }
            else if (!(((BaseItem) item).geturlpath().equals("#"))) {
                Log.d("mOnItemClickListener ee", ((BaseItem) item).geturlpath());
                Variables.clickedMenuItem = true;

                if (!(prgdialogMenu.isShowing())) {
                    prgdialogMenu.show();
                }
                Variables.loadedMenuItem = Variables.appendToHomeUrl(((BaseItem) item).geturlpath());
                //21-9-2018 old  Variables.clickedGroup= false;//19-9-2018
                loadFragmentUrl(Variables.appendToHomeUrl(((BaseItem) item).geturlpath()));
                closeDrawer();

            }

        }

        @Override
        public void onGroupItemClicked(MultiLevelListView parent, View view, Object item, ItemInformation itemInfo) {
            Log.d("onGroupItemClicked", ((BaseItem) item).getName());
            Log.d("onGroupItemClicked", ((BaseItem) item).geturlpath());

            if (!(((BaseItem) item).geturlpath().equals("#"))) {
                Variables.clickedMenuItem = true;
                //21-9-2018 old Variables.clickedGroup= true;//19-9-2018 used when showing progress dialog every time
                loadFragmentUrl(Variables.appendToHomeUrl(((BaseItem) item).geturlpath()));
            }
        }
    };

    private class ListAdapter extends MultiListAdapter {

        private class ViewHolder {
            TextView nameView;
            TextView infoView;
            ImageView arrowView;
            LevelView levelBeamView;
            LinearLayout boderbelow, boderabove;
            RelativeLayout parent2;

        }

        @Override
        public List<?> getSubObjects(Object object) {
            return DataProvider.getSubItems((BaseItem) object);
        }

        @Override
        public boolean isExpandable(Object object) {
            return DataProvider.isExpandable((BaseItem) object);
        }

        @Override
        public View getViewForObject(Object object, View convertView, ItemInformation itemInfo) {
            MainActivity.ListAdapter.ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new MainActivity.ListAdapter.ViewHolder();
                Context scaledContext = ScaledContextWrapper.wrap(MainActivity.this);//29-10-2018

                //29-10-2018 OLD NEED  convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.data_item, null);
                convertView = LayoutInflater.from(scaledContext).inflate(R.layout.data_item, null);

                viewHolder.nameView = (TextView) convertView.findViewById(R.id.dataItemName);
                viewHolder.arrowView = (ImageView) convertView.findViewById(R.id.dataItemArrow);
                viewHolder.levelBeamView = (LevelView) convertView.findViewById(R.id.dataItemLevelBeam);
                viewHolder.boderbelow = (LinearLayout) convertView.findViewById(R.id.boderbelow);
                viewHolder.boderabove = (LinearLayout) convertView.findViewById(R.id.boderabove);
                viewHolder.parent2 = (RelativeLayout) convertView.findViewById(R.id.parent2);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (MainActivity.ListAdapter.ViewHolder) convertView.getTag();
            }

            viewHolder.nameView.setText(((BaseItem) object).getName());

            if (itemInfo.isExpandable()) {
                viewHolder.arrowView.setVisibility(View.VISIBLE);
                viewHolder.arrowView.setImageResource(itemInfo.isExpanded() ?
                        R.drawable.up : R.drawable.down);
            } else {
                viewHolder.arrowView.setVisibility(View.GONE);
            }

//7-9-2018 s

            if ((((BaseItem) object).getName()).equals("separator1")) {
                viewHolder.parent2.setVisibility(View.GONE);
                viewHolder.boderabove.setVisibility(View.GONE);
            } else if ((((BaseItem) object).getName()).equals("separator2")) {
                viewHolder.parent2.setVisibility(View.GONE);
                viewHolder.boderabove.setVisibility(View.VISIBLE);
            } else if ((((BaseItem) object).getgroup()).equals("1")) {
                viewHolder.parent2.setVisibility(View.VISIBLE);
                viewHolder.nameView.setTextColor(getResources().getColor(R.color.colorShopCattxt));
                viewHolder.boderbelow.setBackgroundColor(getResources().getColor(R.color.menuGroup1border));
                viewHolder.boderabove.setVisibility(View.GONE);
            } else if ((((BaseItem) object).getgroup()).equals("2")) {
                viewHolder.nameView.setTextColor(getResources().getColor(R.color.menuGroup2));
                viewHolder.boderbelow.setBackgroundColor(getResources().getColor(R.color.menuGroup1border));
                viewHolder.parent2.setVisibility(View.VISIBLE);

                // Log.d("gr2mm","gr2mm"+viewHolder.nameView.getText().toString()+((BaseItem) object).getName()+((BaseItem) object).getgroup());
                viewHolder.boderabove.setVisibility(View.GONE);
            } else if ((((BaseItem) object).getgroup()).equals("3")) {
                viewHolder.parent2.setVisibility(View.VISIBLE);
                //24-9-2018 old Log.d("gr3mm","gr3mm");
                /*if(calledGroup3==false){
                  //  viewHolder.boderabove.setVisibility(View.VISIBLE);
                    Log.d("gr3mm11","gr3mm11");
                calledGroup3=true;}
                else{
                 //   viewHolder.boderabove.setVisibility(View.GONE);
                }*/
                viewHolder.boderabove.setVisibility(View.GONE);
                viewHolder.nameView.setTextColor(getResources().getColor(R.color.menuGroup2));
                viewHolder.boderbelow.setBackgroundColor(getResources().getColor(R.color.menuGroup1border));
            }
//7-9-2018 e
            viewHolder.levelBeamView.setLevel(itemInfo.getLevel());

            return convertView;
        }
    }


    public void showLogin() {
        try {
            LoginFragment fragment = new LoginFragment();
            FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.category_cont, fragment, LoginFragment.TAG);
            ft.addToBackStack(LoginFragment.TAG);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.commit();
        } catch (Exception e) {

            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
    }


    public void customerloginForCheckout(String strusername, String strpassword) {


        password = strpassword;
        username = strusername;


        //24-9-2018 old Log.d("customerlogin", "customerlogin : " + password + username);

        ApiInterface apiService_get_token = null;
        Call<String> call = null;


        apiService_get_token =
                ApiClient.getcustomerLoginClient().create(ApiInterface.class);


        //10-9-2018 old  call = apiService_get_token.login(username, password);//TODO:ADD AUTO LOGIN (credential.getId(),credential.getPassword())
        //10-9-2018 s
        try {
            call = apiService_get_token.login(URLEncoder.encode(username, "UTF-8"), password);
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(MainActivity.this, e);
        }
        //10-9-2018 e

if(call!=null){//4-1-2018
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();///
                    //24-9-2018 old Log.d("Login", "Login Successful : " + response.body());


                    Variables.cusToken = "Bearer " + response.body();

                    //28-08-2018 old session.createLoginSession(username, password, Variables.cusToken);
//28-08-2018 s
                    /*if (Variables.savePassword) {
                        session.createLoginSession(username, password, Variables.cusToken);

                        Variables.savePassword = false;
                        chksavePassword.setChecked(false);
                    } else {*/

                    session.setIsLoggedIn(true);//NOTE: we do not save password, just do login
                    //  }

                    //28-08-2018 e

                    ApiInterface apiService = null;
                    Call<CartResponse> cusCartQtyCall = null;

                    //24-9-2018 old Log.d("customerlogin", "customerlogin : " + Variables.cusToken);
                    apiService = ApiClient.getCustomerCartQtyClient(Variables.cusToken).create(ApiInterface.class);
                    cusCartQtyCall = apiService.getCartQty();
if(cusCartQtyCall!=null){//4-1-2018
                    cusCartQtyCall.enqueue(new Callback<CartResponse>() {
                        @Override
                        public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                            if (!response.isSuccessful()) {
//29-08-2018 S
                                if ((prgdialogChkout).isShowing()) {
                                    //24-9-2018 old Log.d("prgdialogChkoutdd", "prgdialogChkoutdd");
                                    prgdialogChkout.dismiss();
                                }
                                //29-08-2018 E

                                //24-9-2018 old Log.d("getCartQty", "getCartQty response failed" + response.message());
                                return;
                            } else {
                                //24-9-2018 old Log.d("getCartQty22mm", "getCartQty22mm" + response.body().getitems_qty());
                                //7-11-2018 Commented as not tested- if(Integer.parseInt(response.body().getitems_qty())!=0) {//7-11-2018- added line only
                                Variables.isTokenSuccess = false;//need to be false for checkout login
                                Variables.loginPost = false;//need to be false for checkout login


                                //30-08-2018 OLD assigned but never used  Variables.doingloginAction = true;
                                try {//10-9-2018
                                    Variables.fragmentUrl = Variables.URL_HOME + "customer/account/loginPost?login[username]=" + URLEncoder.encode(username, "UTF-8") + "&login[password]=" + password + "&send=&app=1";
                                }
//10-9-2018 URLEncoder.encode(username, "UTF-8")  added
                                catch (Exception e) {
                                    e.printStackTrace();

                                    StaticMethods.sendCrashReport(MainActivity.this, e);
                                }
                                //10-9-2018 e

                                //24-9-2018 old Log.d("fragmentUrl117", "fragmentUrl117" + Variables.fragmentUrl);

//28-08-2018 s
                                Variables.loadCheckout = true;

                                loadFragmentUrl(Variables.fragmentUrl);
                                updateDrawerLayout();
                                //28-08-2018 e

                                //TODO: INSTEAD BELOW, USE COMMON METHOD FOR  loadFragmentUrl
                                /*28-08-2018 old need?
                                FragmentManager fm = getFragmentManager();
                                MainFragment fragment = (MainFragment) fm.findFragmentByTag(MainFragment.TAG);
                                FragmentTransaction ft = fm.beginTransaction();


                                Variables.loadUrlInPoppedFragment = true;
                                boolean fragmentPopped = fm.popBackStackImmediate(MainFragment.TAG, 0);
                                Log.d("fragmentPopped", "fragmentPopped" + fragmentPopped);


                                updateDrawerLayout();*/
                                //}//7-11-2018
                            }
                        }

                        @Override
                        public void onFailure(Call<CartResponse> call, Throwable t) {

                            //24-9-2018 old Log.d("getCartQty", "getCartQty failed" + t.getMessage());
                            //29-08-2018 S
                            if ((prgdialogChkout).isShowing()) {
                                //24-9-2018 old Log.d("prgdialogChkoutdd", "prgdialogChkoutdd");
                                prgdialogChkout.dismiss();
                            }
                            //29-08-2018 E
                        }
                    });
//4-1-2018 s
                }
                else{
    if ((prgdialogChkout).isShowing()) {
        //24-9-2018 old Log.d("prgdialogChkoutdd", "prgdialogChkoutdd");
        prgdialogChkout.dismiss();
    }
}
                //4-1-2018 e

                } else {
                    credential = null;


                    if ((prgdialogChkout).isShowing()) {
                        //24-9-2018 old Log.d("prgdialogChkoutdd", "prgdialogChkoutdd");
                        prgdialogChkout.dismiss();
                    }


                    Toast.makeText(getApplicationContext(), getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();///


                    //24-9-2018 old Log.d("Login", "Login Failed: You did not sign in correctly or your account is temporarily disabled." + response.errorBody().toString());

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                //24-9-2018 old Log.e("Login", "onResponse fail" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                credential = null;

                if ((prgdialogChkout).isShowing()) {
                    //24-9-2018 old Log.d("prgdialogChkoutdd", "prgdialogChkoutdd");
                    prgdialogChkout.dismiss();
                }

            }
        });
        //4-1-2018 s
    }
    else{
    Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
    credential = null;

    if ((prgdialogChkout).isShowing()) {
        //24-9-2018 old Log.d("prgdialogChkoutdd", "prgdialogChkoutdd");
        prgdialogChkout.dismiss();
    }
}
    //4-1-2018 e
    }


    //to clear focus when outside click of any edittext - including for all fragments in the activity
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        try{//14-1-2019

        //  Log.d("dispatchTouchEvent","dispatchTouchEvent");
        if (event != null) {//31-12-2018
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                View v = getCurrentFocus();
                if (v instanceof EditText) {
                    Rect outRect = new Rect();
                    v.getGlobalVisibleRect(outRect);
                    if(outRect!=null){//7-2-1019
                    if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                        //7-11-2018 old need?? Removed to fix NPE for 8.1 crash in playstore  v.clearFocus();

                    /*7-11-2018 old need
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    */
                        //7-11-2018 s

                        //If no view is focused, a NPE will be thrown-

                        if (v.hasFocus()) {//added to fix NPE for 8.1 crash in playstore
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            if(imm!=null) {//7-2-2019
                               if((v.getWindowToken())!=null) {//25-2-2019
                                   imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                   //27-2-2019 s
                               }
                               else{
                                  // return false;//TODO:Need testing, can cause freezing without an exception
                               }
                                //27-2-2019 e


                            }
                        }
                        //7-11-2018 e

                    }
                }//7-2-1019
                }
            }
        }//31-12-2018
//31-1-2019 s
            else{
            return false;
        }
            //31-1-2019 e
        //14-1-2019 s
    }catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(this, e);
        }
        //14-1-2019 e

            return super.dispatchTouchEvent(event);


    }


    public void addListenerOnChkSavePassword() {


        chksavePassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (((CheckBox) v).isChecked()) {
                    Variables.savePassword = true;
                } else {
                    Variables.savePassword = false;
                }

            }
        });

    }

    public void showStoreL_ns() {
//        StoreTabsFragment fragment = new StoreTabsFragment();
//        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
//        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
//        ft.replace(R.id.category_cont, fragment, StoreTabsFragment.TAG);
//        ft.addToBackStack(StoreTabsFragment.TAG);
//        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
//        ft.commit();
        Intent intent = new Intent(getApplicationContext(), StoreMapActivity.class);
        startActivity(intent);
        closeDrawer();
    }


    public void callGaincity(String phoneNumber) {
        //24-9-2018 old Log.d("phoneNumber22", "phoneNumber22"+phoneNumber);
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            Variables.GaincityPhoneNum = "";
            //24-9-2018 old Log.d("PERMISSION_GRANTED", "PERMISSION_GRANTED");
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(intent);
        } else {
            Variables.GaincityPhoneNum = phoneNumber;
            final String[] permissions = new String[]{Manifest.permission.CALL_PHONE};
            //requestPermissionsResult(4,permissions);
            //if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
            //    Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this, permissions, HANDLE_CALL_PHONE_PERM);
            // return;
           /* }
            else {
               AlertDialog.Builder builder = new AlertDialog.Builder(this);
               builder.setTitle("Gain City App")
                       .setMessage(R.string.no_call_permission)
                       .setPositiveButton(R.string.ok, null)
                       .show();
           }*/
        }


    }


    public void requestPermissionsResult(int requestCode,
                                         @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {


        if ((requestCode == HANDLE_LOCATION) &&
                (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            try {
                Variables.gmap.setMyLocationEnabled(true);
            } catch (SecurityException e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(MainActivity.this, e);
            }
            Variables.gmap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                @Override
                public void onMyLocationChange(Location arg0) {

                    Variables.currentLocation = new LatLng(arg0.getLatitude(), arg0.getLongitude());

                }
            });


            if ((Variables.requestDirection) && ((Variables.currentLocation) != null)) {
                Variables.requestDirection = false;
                String uri = "http://maps.google.com/maps?f=d&hl=es&saddr=" + Variables.currentLocation.latitude + "," + Variables.currentLocation.longitude + "&daddr=" + Variables.selectedMarker.latitude +
                        "," + Variables.selectedMarker.longitude;
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            } else if ((Variables.requestDirection) && ((Variables.currentLocation) == null)) {
                Variables.requestDirection = false;
                Toast.makeText(this, "Please try again.", Toast.LENGTH_SHORT).show();
            }

            return;
        } else if (requestCode == HANDLE_LOCATION) {
            if (Variables.requestDirection) {
                Variables.requestDirection = false;
            }

        }
        if (requestCode != HANDLE_CALL_PHONE_PERM) {
            //24-9-2018 old Log.d("CALL_PHONE", "CALL_PHONE Got unexpected permission result: " + requestCode);
            Variables.GaincityPhoneNum = "";
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //24-9-2018 old Log.d("CALL_PHONE", "CALL_PHONE : permission granted ");

            callGaincity(Variables.GaincityPhoneNum);
            return;
        }

        Variables.GaincityPhoneNum = "";
        //24-9-2018 old Log.e("CALL_PHONE", "CALL_PHONE:Permission not granted: results len = " + grantResults.length +
        //24-9-2018 old       " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Gain City App")
                .setMessage(R.string.no_call_permission)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    //7-9-2018 e
    //11-9-2018 s
/*@Override
public void finish() {//11-9-2018 TODO: freezes app when force close and start
    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        super.finishAndRemoveTask();
    }
    else {
        super.finish();
    }
}*/
    //11-9-2018 e
    //12-9-2018 s
    /*public void clearWebViewHistory(){
        final FragmentManager fm = getFragmentManager();
        MainFragment fragment = (MainFragment) fm.findFragmentByTag(MainFragment.TAG);
        if (fragment != null){
            fragment.webView.clearHistory();
        }
    }*/
    //12-9-2018 e
    //13-9-2018 s
    public class CustomProgressDialog extends Dialog {

        private ImageView iv;

        public CustomProgressDialog(Context context, int spinnerImg) {
            super(context, R.style.TransparentProgressDialog);
            WindowManager.LayoutParams wmp = getWindow().getAttributes();
            wmp.gravity = Gravity.CENTER_HORIZONTAL;
            getWindow().setAttributes(wmp);
            setCancelable(false);
            setOnCancelListener(null);
            setTitle(null);

            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iv = new ImageView(context);
            iv.setImageResource(spinnerImg);
            layout.addView(iv, params);
            addContentView(layout, params);
        }

        @Override
        public void show() {
            super.show();
            RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
            anim.setInterpolator(new LinearInterpolator());

            anim.setDuration(3000);
            anim.setRepeatCount(Animation.INFINITE);
            iv.setAnimation(anim);
            iv.startAnimation(anim);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Context context = ScaledContextWrapper.wrap(newBase);
        super.attachBaseContext(context);
    }

    public void memberInfo() {
        ApiInterface apiService = null;
        Call<MemberResponse> famCardCall = null;


       // Log.d("cusToken", "cusToken" + Variables.cusToken);
        apiService = ApiClient.getCustomerCartQtyClient(Variables.cusToken).create(ApiInterface.class);//use same API client used for cart quantity
        famCardCall = apiService.membershipinfo();
if(famCardCall!=null){//4-1-2018
        famCardCall.enqueue(new Callback<MemberResponse>() {
            @Override
            public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {


                if (!response.isSuccessful()) {

                    // Toast.makeText(MainActivity.this, "Error Occured", Toast.LENGTH_SHORT).show();
                    if (prgdialogMenu.isShowing()) {
                        prgdialogMenu.dismiss();
                    }

                    getTokenSilently(username, password);

                    return;
                } else {


                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int displayWidth = displayMetrics.widthPixels;


                    //14-11-2018 s
                   /*int px_600dp= StaticMethods.dpToPx(600,MainActivity.this);
                    if(displayWidth>=px_600dp){
                        displayWidth=StaticMethods.dpToPx(400,MainActivity.this);;
                    }*/

                    if (response.body().getinfo().equals("Member Logged In")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("name", response.body().getcustomerName());
                        bundle.putString("membershipNo", response.body().getmembershipNo());
                        bundle.putString("loyaltyPoints", response.body().getloyaltyPoints());
                        bundle.putString("earliestExpireDate", response.body().getearliestExpireDate());//17-12-2018
                        bundle.putString("expirePoints", response.body().getpointsToExpire());

                        //24-12-2018 s


                        int px_600dp = StaticMethods.dpToPx(600, MainActivity.this);

                        if (displayWidth > px_600dp) {
                            bundle.putString("screensize", "large"); //large screen
                        } else {
                            bundle.putString("screensize", "small");//medium and small screen
                        }
                        /*else {
                            bundle.putString("screensize", "small"); //small screen
                        }*/
                        //24-12-2018 e


                        bundle.putInt("displayWidth", displayWidth);

                        FamilyCardFragment fragment = new FamilyCardFragment();
                        fragment.setArguments(bundle);

                        FragmentManager fm = getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.category_cont, fragment, FamilyCardFragment.TAG);
                        ft.addToBackStack(FamilyCardFragment.TAG);
                        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                        ft.commit();

                    }

                    //26-12-2018 s
                    else if (response.body().getinfo().equals("Null customer No")) {
                        //Your request could not be completed. For assistance, please contact our customer service officer @ +65 6222 1212 or email us at gciwish@gaincity.com.

                        if (prgdialogMenu.isShowing()) {
                            prgdialogMenu.dismiss();
                        }

                        SpannableString spans = new SpannableString(MainActivity.this.getText(R.string.null_customer_No));
                        ClickableSpan clickSpanCall = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                callGaincity("+6562221212");
                            }

                            public void updateDrawState(TextPaint ds) {
                                //override link-centric text appearance
                                ds.setColor(getResources().getColor(R.color.colorPrimary));
                            }
                        };
                        int indexCall = (MainActivity.this.getText(R.string.null_customer_No).toString()).indexOf("+65 6222 1212");
                        spans.setSpan(clickSpanCall, indexCall, "+65 6222 1212".length() + indexCall, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        ClickableSpan clickSpanEmail = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                /*Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"gciwish@gaincity.com"});
                                i.putExtra(Intent.EXTRA_SUBJECT, "Contact Us - Family Card");
                                i.putExtra(Intent.EXTRA_TEXT   , "");*/
                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                        "mailto", "gciwish@gaincity.com", null));
                                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact Us - Family Card");
                                emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                                try {
                                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                                } catch (android.content.ActivityNotFoundException ex) {
                                    Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            public void updateDrawState(TextPaint ds) {
                                //override link-centric text appearance
                                ds.setColor(getResources().getColor(R.color.colorPrimary));
                            }
                        };
                        int indexEmail = (MainActivity.this.getText(R.string.null_customer_No).toString()).indexOf("gciwish@gaincity.com");
                        spans.setSpan(clickSpanEmail, indexEmail, "gciwish@gaincity.com".length() + indexEmail, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                        alertDialog.setMessage(spans);
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.WHITE);

                        ((TextView) alertDialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
                    }
//11-1-2019 s
                    else if (response.body().getinfo().equals("api not available")) {
                        if (prgdialogMenu.isShowing()) {
                            prgdialogMenu.dismiss();
                        }
                        Toast.makeText(MainActivity.this, "Service is not available. Please try again Later.", Toast.LENGTH_SHORT).show();
                    }
                    //11-1-2019 e
//26-12-2018 e


                    else {
                        if (prgdialogMenu.isShowing()) {
                            prgdialogMenu.dismiss();
                        }

                        getTokenSilently(username, password);

                    }

                }


            }

            @Override
            public void onFailure(Call<MemberResponse> call, Throwable t) {
                //Toast.makeText(MainActivity.this, "Error Occured", Toast.LENGTH_SHORT).show();
                if (prgdialogMenu.isShowing()) {
                    prgdialogMenu.dismiss();
                }
                getTokenSilently(username, password);
            }
        });
        //4-1-2018 s
    }
    else{
    if (prgdialogMenu.isShowing()) {
        prgdialogMenu.dismiss();
    }
    getTokenSilently(username, password);
}
        //4-1-2018 e
    }

    public void closeFamilyCard_inFrag() {

        final FragmentManager fm = getSupportFragmentManager();
        FamilyCardFragment familyCardFragment = (FamilyCardFragment) fm.findFragmentByTag(FamilyCardFragment.TAG);
        if ((familyCardFragment != null) && (familyCardFragment.isVisible())) {
            setHeaderPortrait();
            fm.beginTransaction().remove(familyCardFragment).commit();

            String lastURLExceptLogin = session.getlastURLExceptLogin();
            loadFragmentUrl(lastURLExceptLogin);

        }
    }

    public void closeFamilyCard(View v) {
        final FragmentManager fm = getSupportFragmentManager();
        FamilyCardFragment familyCardFragment = (FamilyCardFragment) fm.findFragmentByTag(FamilyCardFragment.TAG);
        if ((familyCardFragment != null) && (familyCardFragment.isVisible())) {
            setHeaderPortrait();
            fm.beginTransaction().remove(familyCardFragment).commit();

            String lastURLExceptLogin = session.getlastURLExceptLogin();
            loadFragmentUrl(lastURLExceptLogin);

        }
    }

    public void setHeaderPortrait() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().show();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
public void goToFAQ(View v){
    final FragmentManager fm = getSupportFragmentManager();
    FamilyCardFragment familyCardFragment = (FamilyCardFragment) fm.findFragmentByTag(FamilyCardFragment.TAG);
    if ((familyCardFragment != null) && (familyCardFragment.isVisible())) {
        setHeaderPortrait();
        fm.beginTransaction().remove(familyCardFragment).commit();
        if (!(prgdialogMenu.isShowing())) {
            prgdialogMenu.show();
        }
        loadFragmentUrl(Variables.URL_HOME +"customer-service/faq#familycardmembership");

    }

}
}

