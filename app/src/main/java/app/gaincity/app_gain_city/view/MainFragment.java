package app.gaincity.app_gain_city.view;


//24-9-2018 old import android.Manifest;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;//16-10-2018 old android.app.Fragment;
import android.support.v4.app.FragmentManager;//16-10-2018 old android.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;//16-10-2018 old android.app.FragmentTransaction;
import android.content.Context;
//24-9-2018 old import android.content.Intent;
import android.graphics.Bitmap;
//24-9-2018 old import android.net.Uri;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
//24-9-2018 old import android.support.annotation.RequiresPermission;
import android.os.Message;
import android.support.v4.view.GravityCompat;
//24-9-2018 old import android.util.Log;
//import android.util.Log;
//import android.view.Gravity;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
//24-9-2018 old import android.webkit.RenderProcessGoneDetail;

import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

//24-9-2018 old import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
//24-9-2018 old import java.util.concurrent.TimeUnit;
//24-9-2018 old import java.util.regex.Pattern;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;


public class MainFragment extends Fragment {
    //LinearLayout layoutwebView1;
    //24-9-2018 old  String dir;
    public final static String TAG = "MainFragmentTag";
    public WebView webView;
    int waitTime = 1;
    Context ctx;
    Timer timer_Cookie_check;
    String urlonPageStarted;
    //29-08-2018 old boolean calledShowcheckoutLogin = false;
    boolean needShowLoginForCheckout = false;
    private View v;
    private boolean connected = false;
    private FloatingActionButton fab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (v != null) return v;
        //24-9-2018 old Log.d("onCreateView", "onCreateView");

        v = inflater.inflate(R.layout.fragment_main, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018
        //29-10-2018 S
        ctx = (MainActivity) getActivity();
        /*Context scaledContext = ScaledContextWrapper.wrap(ctx);
        LayoutInflater scaledInflater = LayoutInflater.from(scaledContext);
        v = scaledInflater.inflate(R.layout.fragment_main, container, false);*/
        //29-10-2018 E

        fab = v.findViewById(R.id.fab);
        webView = (WebView) v.findViewById(R.id.webView1);
        // layoutwebView1 = (LinearLayout) v.findViewById(R.id.layoutwebView1);//25-9-2018


        WebSettings ws = webView.getSettings();
        ws.setUserAgentString("gaincity.android.app");

//25-9-2018 s

        //25-9-2018 s copied from above
        //26-10-2018 uncommented  webView.getSettings().setPluginState(WebSettings.PluginState.ON);//25-9-2018 need? for loading webpages inside app
        // webView.getSettings().setSupportMultipleWindows(true);//25-9-2018
        ws.setDomStorageEnabled(true);
        ws.setAllowFileAccess(true);//24-9-2018
        //24-9-2018 old dir = ((MainActivity) ctx).getFilesDir().getAbsolutePath();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true); //25-9-2018
        //17-9-2018 s
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);//v12
        //17-9-2018 e
        //21-9-2018 s
        webView.getSettings().setUseWideViewPort(true);//render more like a mobile browser
        webView.getSettings().setLoadWithOverviewMode(true);//render more like a mobile browser
        webView.getSettings().setSupportZoom(true);//enable pinch-zooming
        webView.getSettings().setBuiltInZoomControls(true);//enable pinch-zooming
        webView.getSettings().setDisplayZoomControls(false);//enable pinch-zooming
        //21-9-2018 e


        webView.clearCache(false);//24-9-2018 old true //27-08-2018 if false, only the RAM cache is cleared
        webView.requestFocusFromTouch();

        //25-9-2018 e
        //26-9-2018 e
/*
25-9-2018 copied below
       //24-9-2018 old dir = ((MainActivity) ctx).getFilesDir().getAbsolutePath();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);//25-9-2018
        //17-9-2018 s
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);//v12
        //17-9-2018 e
        //21-9-2018 s
        webView.getSettings().setUseWideViewPort(true);//render more like a mobile browser
        webView.getSettings().setLoadWithOverviewMode(true);//render more like a mobile browser
        webView.getSettings().setSupportZoom(true);//enable pinch-zooming
        webView.getSettings().setBuiltInZoomControls(true);//enable pinch-zooming
        webView.getSettings().setDisplayZoomControls(false);//enable pinch-zooming
        //21-9-2018 e


        webView.clearCache(false);//24-9-2018 old true //27-08-2018 if false, only the RAM cache is cleared
*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

        } else {
            CookieSyncManager.createInstance(webView.getContext());//still gives login form sometimes
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //TODO:Need For debug WebView.setWebContentsDebuggingEnabled(true);//TODO:REMOVE
        }


        CookieManager.getInstance().setAcceptCookie(true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        } else {

        }


        MyJavaScriptInterface jsInterface = new MyJavaScriptInterface();
        webView.addJavascriptInterface(jsInterface, "MyJavaScriptInterface");


        webView.setWebChromeClient(
                new WebChromeClient() {

                    /*                    //25-9-2018 s

                                        // Add new webview in same window
                                        @Override
                                        public boolean onCreateWindow(WebView view, boolean dialog,
                                                                      boolean userGesture, Message resultMsg) {
                                            WebView childView = new WebView(ctx);
                                            childView.getSettings().setJavaScriptEnabled(true);
                                            childView.setWebChromeClient(this);
                                            childView.setWebViewClient(new WebViewClient());
                                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                                            childView.setLayoutParams(params);
                                            view.addView(childView);
                                            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                                            transport.setWebView(childView);
                                            resultMsg.sendToTarget();

                                            childView.setWebViewClient(new WebViewClient() {
                                                @Override
                                                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                                    view.loadUrl(url);
                                                    return true;
                                                }
                                            });
                                            return true;
                                        }

                                        // remove new added webview whenever onCloseWindow gets called for new webview.
                                        @Override
                                        public void onCloseWindow(WebView window) {
                                            layoutwebView1.removeViewAt(layoutwebView1.getChildCount() - 1);
                                        }

                                        // For Android 4.1+
                                        @SuppressWarnings("unused")
                                        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                                            *//*mUploadMessage = uploadMsg;

                        Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                        i.addCategory(Intent.CATEGORY_OPENABLE);
                        i.setType(acceptType);

                        startActivityForResult(Intent.createChooser(i, "SELECT"), 100);*//*
                    }

                    // For Android 5.0+
                    @SuppressLint("NewApi")
                    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                       *//* if (mUploadMessageArr != null) {
                            mUploadMessageArr.onReceiveValue(null);
                            mUploadMessageArr = null;
                        }

                        mUploadMessageArr = filePathCallback;

                        Intent intent = fileChooserParams.createIntent();

                        try {
                            startActivityForResult(intent, 101);
                        } catch (ActivityNotFoundException e) {
                            mUploadMessageArr = null;

                            Toast.makeText(activity,"Some error occurred.", Toast.LENGTH_LONG).show();

                            return false;
                        }*//*

                        return true;
                    }
//25-9-218 e*/

                    @Override
                    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                       /*//24-9-2018 old  Log.d("consoleMessage", "consoleMessage" + consoleMessage.message() + " -- From line "
                                + consoleMessage.lineNumber() + " of "
                                + consoleMessage.sourceId());*/
                        return super.onConsoleMessage(consoleMessage);
                    }


                    public void onProgressChanged(WebView view, int progress) {
                        // Activities and WebViews measure progress with different scales.
                        // The progress meter will automatically disappear when we reach 100%
                        //ctx.setProgress(progress * 1000);


                        if ((progress == 100) && (((MainActivity) ctx).prgdialogAdding.isShowing())) {
                            ((MainActivity) ctx).prgdialogAdding.dismiss();


                            // Toast.makeText((MainActivity)ctx, "progress" + progress, Toast.LENGTH_SHORT).show();
                        }

//13-9-2018 s
                        //onPageCommitVisible not called in Android 5.0 so used this
                        //can not trust when it is going to call onProgressChanged
                        /*if((webView.getProgress()>12)&&(((MainActivity) ctx).prgdialogHome.isShowing())){

                            ((MainActivity) ctx).prgdialogHome.dismiss();
                        }*///13-9-2018 e
                    }
                });


        webView.setWebViewClient(new HelloWebViewClient());
/*//25-9-2018 s copied from above
       // webView.getSettings().setPluginState(WebSettings.PluginState.ON);//25-9-2018 need? for loading webpages inside app
       // webView.getSettings().setSupportMultipleWindows(true);//25-9-2018
        ws.setDomStorageEnabled(true);
        ws.setAllowFileAccess(true);//24-9-2018
        //24-9-2018 old dir = ((MainActivity) ctx).getFilesDir().getAbsolutePath();
        webView.getSettings().setJavaScriptEnabled(true);
     //   webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);//25-9-2018
        //17-9-2018 s
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);//v12
        //17-9-2018 e
        //21-9-2018 s
        webView.getSettings().setUseWideViewPort(true);//render more like a mobile browser
        webView.getSettings().setLoadWithOverviewMode(true);//render more like a mobile browser
        webView.getSettings().setSupportZoom(true);//enable pinch-zooming
        webView.getSettings().setBuiltInZoomControls(true);//enable pinch-zooming
        webView.getSettings().setDisplayZoomControls(false);//enable pinch-zooming
        //21-9-2018 e


        webView.clearCache(false);//24-9-2018 old true //27-08-2018 if false, only the RAM cache is cleared

        //25-9-2018 e*/

        loadFragmentUrl();

        //startCookieTimer();//2-11-2018  TODO:REMOVE


        return v;
    }

    public void loadFragmentUrl() {

        connected = StaticMethods.isNetworkAvailable(getActivity());//25-2-2019 old (MainActivity) ctx
        if (connected) {

            //Log.d("loadFragmentUrl", "loadFragmentUrl" + Variables.fragmentUrl+webView);//26-9-2018
            Log.d("manageCookies", "manageCookies " + CookieManager.getInstance().getCookie(Variables.fragmentUrl));
            Log.d("manageCookies", "cookiesDomain " + Variables.fragmentUrl);
            Log.d("mOnItemClickListener we", "webView " + webView.getSettings().getUserAgentString());
            webView.setWebViewClient(new HelloWebViewClient());
            webView.loadUrl(Variables.fragmentUrl);
            Log.d("mOnItemClickListener we", "webView " + webView.getSettings().getUserAgentString());
            //24-9-2018 old Log.d("UserAgentString", "UserAgentString " + webView.getSettings().getUserAgentString());


        } else {

            if (Variables.fragmentUrl.contains(Variables.URL_HOME + "customer/account/loginPost?login")) {
                if (((MainActivity) ctx).session.isLoggedIn()) {
//No need to clear cookies here since we do it when logout and when oncreate of app, loginpost may require
                    //previous cookies eg.for wishlist
                    ((MainActivity) ctx).session.logoutUser();// I do not use logout(),
                    // since I dont want to reset variables here, they are already
                    //reseted when we call this loginPost url
                }
            }

            if ((((MainActivity) ctx).prgDialogLoading != null) && ((((MainActivity) ctx).prgDialogLoading).isShowing())) {
                ((MainActivity) ctx).prgDialogLoading.dismiss();
            }

            if ((((MainActivity) ctx).prgdialogAdding != null) && ((((MainActivity) ctx).prgdialogAdding).isShowing())) {
                ((MainActivity) ctx).prgdialogAdding.dismiss();
            }

            if (((((MainActivity) ctx).prgdialog) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                (((MainActivity) ctx).prgdialog).dismiss();
            }

            if (((((MainActivity) ctx).prgdialogsearch) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                (((MainActivity) ctx).prgdialogsearch).dismiss();
            }

            if (((((MainActivity) ctx).prgdialogLogout) != null) && ((((MainActivity) ctx).prgdialogLogout).isShowing())) {
                (((MainActivity) ctx).prgdialogLogout).dismiss();
                //27-08-2018 s
                ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
            }

            if (((((MainActivity) ctx).prgdialogChkout) != null) && ((((MainActivity) ctx).prgdialogChkout).isShowing())) {
                (((MainActivity) ctx).prgdialogChkout).dismiss();
            }

            StaticMethods.noConnection((MainActivity) ctx);
        }
    }

    //2-11-2018 s TODO:REMOVE
   /*  public void startCookieTimer() {//TODO:remove


        if (timer_Cookie_check == null) {


            timer_Cookie_check = new Timer();


            TimerTask_CookieCheck Every5SecondsTask = new TimerTask_CookieCheck() {//TO AVOID LEAKS
                @Override
                public void run() {

                    //TODO:NEED? Variables.loggedInCookie=false;
                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("loggedInCookie", "loggedInCookie" + webView.getUrl());


                            if (webView.getUrl() != null) {
                                manageCookies(webView.getUrl());
                            }

                        }
                    });


                }
            };


            timer_Cookie_check.schedule(Every5SecondsTask, TimeUnit.SECONDS.toMillis(waitTime), TimeUnit.SECONDS.toMillis(5));   // 1000*10*60 every 10 minut

        }

    }*/
    //2-11-2018 e
    /*30-08-2018 old  */

    public boolean goBack() {
        if (isAdded() && webView != null && webView.canGoBack()) {
            //returns to the previous webpage
            webView.goBack();
            return true;
        }
        return false;
    }

//2-11-2018 s TODO:REMOVE 2-11-2018
    /*private void manageCookies(String url) {
        Log.d("manageCookies", "manageCookies" + CookieManager.getInstance().getCookie(url));//4-4-18

        String[] rawcookies;
        String raw = CookieManager.getInstance().getCookie(url);

        if (raw != null) {
            if (raw.contains("PHPSESSID")) {

                rawcookies = Pattern.compile(";").split(raw);
                for (String cc : rawcookies) {
                    if (cc.contains("PHPSESSID")) {

                        String[] rawcookies_2 = Pattern.compile("=").split(cc);

                        String newPHPSESSID = rawcookies_2[1];

                        Log.d("manageCookiesMM", "manageCookiesMM" + "===" + newPHPSESSID + "====" +newPHPSESSID.equals(Variables.PHPSESSID));
                        if (!(newPHPSESSID.equals(Variables.PHPSESSID))) {

                            Variables.PHPSESSID = newPHPSESSID;
                        }
                        *//*if (Variables.doingloginAction = true) {
                            if (!(newPHPSESSID.equals(Variables.PHPSESSID))) {
                                Variables.doingloginAction = false;
                                Variables.PHPSESSID = newPHPSESSID;
                            }


                        } else if ((!(newPHPSESSID.equals(Variables.PHPSESSID))) && (!(Variables.PHPSESSID.equals("")))) {
                            Log.d("manageCookiesMM2", "manageCookiesMM2");
                            //PHPSESSID will not change until user logout?
                        Variables.fragmentUrl=Variables.appendToHomeUrl("customer/account/logout/");
                        loadFragmentUrl();
                            Variables.useSmartLock = false;
                            Variables.cusToken = "";
                            Variables.numcartItems = 0;
                            Variables.calledUpgrade = false;

                            Variables.fragmentUrl = "";

                            //30-08-2018 old  Variables.taskLoadingCount = 0;

                            Variables.loginPost = false;

                            Variables.isInsideSessionLogin = false;
                            Variables.isActiveinEmail = false;
                            Variables.isActiveinPassword = false;
                            Variables.isTokenSuccess = false;


                            //30-08-2018 OLD assigned but never used  Variables.lastURLcalled = false;
                            if (((MainActivity) ctx).session.isLoggedIn()) {
                                ((MainActivity) ctx).session.logoutUser();


                                ((MainActivity) ctx).setupBadge();
                                ((MainActivity) ctx).credential = null;

                                ((MainActivity) ctx).updateDrawerLayout();
                            }
                        }

                        if (!(newPHPSESSID.equals(Variables.PHPSESSID))) {
                            Variables.PHPSESSID = newPHPSESSID;
                        }


                        break;*//*


                    }


                }
            }
        }

    }*/
//2-11-2018 e

    public final void enableCookies(WebView webView) {
        // enable javascript (has security implications!)
        webView.getSettings().setJavaScriptEnabled(true);


    }

    @Override
    public void onStop() {

        super.onStop();


    }

    @Override
    public void onStart() {
        super.onStart();

       /*28-08-2018 old need??
        if (Variables.loadUrlInPoppedFragment) {
            Variables.loadUrlInPoppedFragment = false;

            webView.loadUrl(Variables.fragmentUrl);
        }*/

    }

    @Override
    public void onResume() {
        super.onResume();

        if (!(((AppCompatActivity) getActivity()).getSupportActionBar().isShowing())) {//20-12-2018
            //Log.d("calledShowBars","calledShowBars");
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//20-12-2018
            ((AppCompatActivity) getActivity()).getSupportActionBar().show();//20-12-2018
        }

        //24-9-2018 old Log.d("onResume", "onResume");
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //fragment  without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment  without getting action to set to -1

    }

    @Override
    public void onPause() {
        super.onPause();


        if (timer_Cookie_check != null) {
            timer_Cookie_check.cancel();
            timer_Cookie_check = null;

        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //24-9-2018 old Log.d("Lifecycle", "onPause");
        //11-9-2018 s
        //set references to null to avoid leaks
        v = null;
        webView.destroy();//webview should destroy here otherwise leak fragment
        webView = null;
        timer_Cookie_check = null;
        ctx = null;

//11-9-2018 e
    }

    //11-9-2018 s
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //set references to null to avoid leaks
     /*   v=null;
// webView=null;
timer_Cookie_check=null;
        ctx=null;*/
    }

    public void showcheckoutLogin2() {
        Variables.loginMethod = 0;
        if (!(((MainActivity) ctx).session.isLoggedIn())) {


            ((MainActivity) ctx).showLogin();
            ((MainActivity) ctx).prgDialogLoading.dismiss();
        }
    }

    static class TimerTask_CookieCheck extends TimerTask {//NOTE: MADE STATIC CLASS TO AVOID MEMORY LEAKS


        @Override
        public void run() {

        }
    }

    private class HelloWebViewClient extends WebViewClient {


        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                if (request != null) {

                    // Log.d("requestHeaders", "requestHeaders" + request.getMethod());


                    if (request.getUrl().toString().equals(Variables.URL_HOME + "wishlist/index/add/")) {
                        Variables.loginMethod = 1; //1= addtowishlist login METHOD
                    }


                    // Log.d("requestHeaders22", "requestHeaders22" + request.getUrl().toString());


                    /*28-08-2018 commented
                    if (request.getRequestHeaders() != null) {
                        Log.d("requestHeaders442", "requestHeaders442" + request.getRequestHeaders().values());
                        Log.d("requestHeaders443", "requestHeaders443" + request.getRequestHeaders().entrySet());

                        if (request.getRequestHeaders().get("messages") != null) {
                            Log.d("requestHeaders44", "requestHeaders44" + request.getRequestHeaders().get("messages"));
                        }
                    }*/
                }
            }


            return null;
        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            //Log.d("onPageStartedurl", "onPageStartedurl" + url);//26-9-2018

            if (url.equals("file:///android_asset/errorPage.html")) {
                return;
            }


            if (!(StaticMethods.isNetworkAvailable(((MainActivity) ctx)))) {//1-11-2018 - !( added //11-9-2018 old if (!StaticMethods.isNetworkAvailable(ctx)) {

//when no network connection
                if (Variables.fragmentUrl.contains(Variables.URL_HOME + "customer/account/loginPost?login")) {
//5-9-2018 added note
                    //Note:if no network connection, when come to this place, user may already logged in
                    //in native side, so need to logout from native side as we are unable to
                    //login in inside webview

                    //What happen if user has internet at this time, but could not load page
                    //since exception or since user goes to error page?(other failures will go to gaincity site
                    // so it will not create problems as we check 'myaccount' element inside web page to call
                    // native logout)
                    //when exception occured and user not logged in webview, when user go to a working page
                    //it should call native logout. we do not use any function without webview, so such error
                    // can not create any problem
                    //TODO 5-9-2018: but if user click on logout button at that time
                    //if signout link redirected to a different link like login or
                    //if signout page also has an excpetion
                    //what will happen to signout progress bar?
                    //after signout, user must always see signout success page, can not go to any other page
                    //but here, since user not logged in and doing signout it can redirect to other page, so although
                    //user may logout using native logout call, progress bar will appear for ever
                    //so we need to find that other possible urls and should not change them in future
                    //if signout page also has an excpetion, it will however load the page
                    //it can load a different url with the exception since it happened since a different
                    // file? if it is so progress bar will appear for ever so need to call android side
                    // method when exception occured,and there I should dismiss all progress bars-but
                    //in this place need to consider about what variables should change their status

                    //When signout I redirect to home page from signout success page because
                    //before finish all onLoadResource calls for signout success page
                    // user can load other web pages by clicking some button ,
                    //to avoid that i need to wait until finish all onLoadResource calls
                    //but there is no way to check that,even after onpagefinished called, still
                    //webview can call onLoadResource. so the best way was to hide progress bar
                    //only when it reached home page


                    //TODO 5-9-2018: what happen when a progress bar is showing and suddenly disconnected network? when no network
                    //it goes to no network page, at that time need to dismiss all progress bars

                    //TODO 5-9-2018 CHECK: what happen when a progress bar is showing and it goes to exception page?
                    //FOR a normal load, exception page just shows exception, it always call correct webview methods like
                    //onpagefinished , so it is not a problem
                    //BUT for things like logout, if exception in signout page will not redirect user to
                    //home page. so at this time progress bar appear for ever.
                    //we should hide all progress bars when we receive exception or
                    //if it is not possible, user need close app and open again
                    //at this time if user saved password, it will login again automatically
                    //otherwise it will show complete new session

                    //in all the times, when exception occur progress bar may not dismiss due to not
                    // finishing things. so must call android side method or
                    //user need to close app and reopen app. so for all below condition
                    //checkings I omit exception behaviour, need to check it after I receive android
                    //side method(can not check exception behaviour now, since
                    //TODO:ASK: don't know whether it loads different files-can load different urls in between?
                    //can. for "logout/" url call it can stop in the same place with the exception, not going to successpage
                    //it can happen in any controller like logout controller,
                    //so need to do something for variable status after exception happen if we are not
                    //going to close app when exception occur
                    // if it is same url(file), can check whether variables are setting in correct way )

                    ////for checkout login when it is going through adding to wish list
                    //after onpagefinished for wish list page I call checkout page
                    //if exception occur in wishlist page, it can still go to checkout-but not a big error
                    if (((MainActivity) ctx).session.isLoggedIn()) { //11-9-2018  old if (((MainActivity) ctx).session.isLoggedIn()) {


                        ((MainActivity) ctx).session.logoutUser();  //11-9-2018  old  ((MainActivity) ctx).session.logoutUser();//may require cookies here for loginpost, so i dont clear cookies
                        // I do not use logout();,
                        // since I dont want to reset variables here, they are already
                        //reseted when we call this loginPost url


                    }
                }


                if (((((MainActivity) ctx).prgdialogLogout) != null) && ((((MainActivity) ctx).prgdialogLogout).isShowing())) {
                    (((MainActivity) ctx).prgdialogLogout).dismiss();
                    //27-08-2018 s
                    ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
                }


                //Log.d("onPageStartederr1", "onPageStartederr1"+url);//26-9-2018

                StaticMethods.noConnection((MainActivity) ctx);
            } else {


                //19-9-2018 s
                if ((!(Variables.notificationURL.equals(""))) && (url.equals(Variables.notificationURL))) {

                    Variables.notificationURL = "";
                }

                /*//21-9-2018 old
                if ((Variables.clickedGroup==false)&&(!(((MainActivity) ctx).prgdialogCommon.isShowing()))) {
                    ((MainActivity) ctx).prgdialogCommon.show();
                }*/
                //19-9-2018 e

                //17-9-2018 s
                /*if (url.contains(Variables.URL_HOME + "checkout/onepage/success/")) {
                    Variables.stoppedUsingJs = false;
                    if (((MainActivity) ctx).prgDialogLoadingPurchase.isShowing()) {
                        ((MainActivity) ctx).prgDialogLoadingPurchase.dismiss();
                    }
                }*/
                //17-9-2018 e

//12-9-2018 s
                //TODO: NOTE: We should not use this kind of unnecessary URL checking,
                //TODO: NOTE: it slow down webview's page loading
                if ((url.equals("https://secureacceptance.cybersource.com/canceled")) || (url.equals("https://www.gaincity.com/cybersource/index/cancel/"))) {
                    if (!(((MainActivity) ctx).prgCancelled.isShowing())) {
                       /*13-9-2018 old need ((MainActivity) ctx).prgCancelled.setMessage(getResources().getString(R.string.loadingDialog));
                       ((MainActivity) ctx).prgCancelled.setCancelable(false);*/
                        ((MainActivity) ctx).prgCancelled.show();
                    }
                }

//13-9-2018 s
                if (((MainActivity) ctx).prgdialogHome.isShowing()) {

                    ((MainActivity) ctx).prgdialogHome.dismiss();
                }


                //13-9-2018 e
//12-9-2018 e

                //7-9-2018 s

                if (!(url.contains(Variables.URL_HOME))) {//for now it's going to other sites when
                    //inside checkout only


                    Variables.insideCheckout = true;
                    fab.setVisibility(View.GONE);
                    return;
                }
                /*10-9-2018 old else if((url.contains(Variables.URL_HOME+"checkout/"))||
                        (url.contains(Variables.URL_HOME+"checkout/cart"))){
                    //when payment cancelled it comes back to "checkout/" page
                    //but at that tims js method is not being called by server side
                    //from payment it come back to cart if not succesful
                    Variables.insideCheckout=false;
                }*/
                else {


                    //can not add else to false, because
                    //https://www.gaincity.net/cybersource/index/receipt/
                    //contains Variables.URL_HOME
                    //but user is still inside checkout, so it may cause sudden interrupt
                    //to checkout process by signing out if myaccont element not available there

                    //10-9-2018 BUT TO FIX 2C2P ISSUE NEED TO SET IT TO true here

                    Variables.insideCheckout = false;
                  // fab.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.GONE);
                }


                //7-9-2018 e


                urlonPageStarted = url;


                if (((url.contains(Variables.URL_HOME + "checkout/cart")) || (url.contains(Variables.URL_HOME + "product/")) || (Variables.qrcodeRedirect)) && (!(((MainActivity) ctx).drawer.isDrawerOpen(GravityCompat.START)))) {
                    //5-9-2018 added note
                    // when drawer not opened and comes to this place since
                    //a QR code redirect or
                    //coming to "product/" page or
                    //coming to "checkout/cart" page
                    Variables.qrcodeRedirect = false;
                    if ((!(((MainActivity) ctx).prgdialogAdding.isShowing()) && (url.contains(Variables.URL_HOME + "product/")))) {//12-6 Variables.URL_HOME+
                       /*13-9-2018 old need  ((MainActivity) ctx).prgdialogAdding.setMessage(getResources().getString(R.string.loadingDialog));
                        ((MainActivity) ctx).prgdialogAdding.setCancelable(false);*/
                        ((MainActivity) ctx).prgdialogAdding.show();
                    } else if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
                        /*13-9-2018 old need ((MainActivity) ctx).prgDialogLoading.setMessage(getResources().getString(R.string.loadingDialog));
                        ((MainActivity) ctx).prgDialogLoading.setCancelable(false);*/
                        ((MainActivity) ctx).prgDialogLoading.show();

                    }
                    Variables.clickedMenuItem = false;
                }

            }
        }

        @Override

        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)//NOTE: FOR api 24 and above, android 7.0 and above
        {
            String url = "";
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                url = request.getUrl().toString();
            }

            //7-9-2018 s

            // Log.d("canNotGoBack", "canNotGoBack" + CookieManager.getInstance().getCookie(url));
//7-9-2018 s
           /* 26-10-2018 old need added below
           if( (CookieManager.getInstance().getCookie(url) == null)&&(!(url.equals("https://maintenance.gaincity.com/")))) {//for call us button
                return true;// if return true page will not load in the UI but it
                //really loads and onpagefinished is also get called,
            }*/
//7-9-2018 e

            try {
                connected = StaticMethods.isNetworkAvailable((MainActivity) ctx);
            } catch (Exception e) {
                e.printStackTrace();
                StaticMethods.sendCrashReport(((MainActivity) ctx), e);
            }


            if (!connected) {


                if (Variables.fragmentUrl.contains(Variables.URL_HOME + "customer/account/loginPost?login")) {
                    if (((MainActivity) ctx).session.isLoggedIn()) {

                        ((MainActivity) ctx).session.logoutUser();//may require cookies here for loginpost
                        // I do not use logout(),
                        // since I dont want to reset variables here, they are already
                        //reseted when we call this loginPost url


                    }
                }
                //24-9-2018 old Log.d("onPageStartederr2", "onPageStartederr2");

                if ((((MainActivity) ctx).prgDialogLoading != null) && ((((MainActivity) ctx).prgDialogLoading).isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.dismiss();
                }

                if ((((MainActivity) ctx).prgdialogAdding != null) && ((((MainActivity) ctx).prgdialogAdding).isShowing())) {
                    ((MainActivity) ctx).prgdialogAdding.dismiss();
                }


                if ((((MainActivity) ctx).prgdialog_noti) != null) {
                    if (((MainActivity) ctx).prgdialog_noti.isShowing()) {
                        ((MainActivity) ctx).prgdialog_noti.dismiss();
                    }
                }

                if (((((MainActivity) ctx).prgdialog) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                    (((MainActivity) ctx).prgdialog).dismiss();
                }

                if (((((MainActivity) ctx).prgdialogsearch) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                    (((MainActivity) ctx).prgdialogsearch).dismiss();
                }

                if (((((MainActivity) ctx).prgdialogLogout) != null) && ((((MainActivity) ctx).prgdialogLogout).isShowing())) {
                    (((MainActivity) ctx).prgdialogLogout).dismiss();
                    //27-08-2018 s
                    ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
                }


                view.stopLoading();
                StaticMethods.noConnection((MainActivity) ctx);
                return false;
            }

//17-9-2018 s
            if (url.contains(Variables.URL_HOME + "checkout/onepage/success/")) {
                Variables.stoppedUsingJs = false;
                if (((MainActivity) ctx).prgDialogLoadingPurchase.isShowing()) {
                    ((MainActivity) ctx).prgDialogLoadingPurchase.dismiss();
                }
            }
            //17-9-2018 e

            if ((!(Variables.notificationURL.equals(""))) && (url.equals(Variables.notificationURL))) {

                Variables.notificationURL = "";
            }


            /*27-08-2018 OLD NEED if (((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer")))  && (Variables.loginMethod == 1)) {
                Log.d("needShowLoginWishl22", "needShowLoginWishl22");


                if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
                    ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
                    ((MainActivity) ctx).prgDialogLoading.show();

                }
                ((MainActivity) ctx).showLogin();

                return false;


            }*/


            if (((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer"))) && needShowLoginForCheckout) {
                //24-9-2018 old Log.d("needShowLoginchk22", "needShowLoginchk22" + needShowLoginForCheckout);


                needShowLoginForCheckout = false;
                if (!((((MainActivity) ctx).prgDialogLoading).isShowing())) {
                   /*13-9-2018 old need  ((MainActivity) ctx).prgDialogLoading.setMessage(getResources().getString(R.string.loadingDialog));
                    ((MainActivity) ctx).prgDialogLoading.setCancelable(false);*/
                    ((MainActivity) ctx).prgDialogLoading.show();

                }
                showcheckoutLogin2();
                webView.loadUrl(Variables.URL_HOME + "checkout/cart/");//After showing native login it redirects
                //webview to cart page
                return true;


            }
//29-08-2018 s
            else if ((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer"))) {


                // Log.d("calledShowcheck33", "calledShowcheck33"+calledShowcheckoutLogin);

                //24-9-2018 old Log.d("WhenRedirected22", "WhenRedirected22");
                if (Variables.loginMethod == 1) {//27-08-2018 S For wishlist
                    //24-9-2018 old Log.d("needShowLoginWishlJS1", "needShowLoginWishlJS1");

   /* if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
        ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
        ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
        ((MainActivity) ctx).prgDialogLoading.show();

    }*/
                    //This doesn't cause logout,just delete history
                    webView.clearHistory();// need to clear history otherwise going back to webview
                    // from loginfragment on backpressed, gives loginform back sometimes

                    //view.loadUrl("file:///android_asset/blankPage.html");
                    // webView.stopLoading();
                    //   webView.goBack();
                    //  String lastURLExceptLogin = ((MainActivity) ctx).session.getlastURLExceptLogin();
                    // webView.loadUrl(lastURLExceptLogin);
                    // webView.loadUrl(Variables.URL_HOME);//do not allow loading login page instead go to home
                    // to use when back pressed from login form

                    ((MainActivity) ctx).showLogin();
                    //Note:when we get LoginFragment inside showLogin(), there I reset Variables

                    return true;

                } else { //  else if (!calledShowcheckoutLogin) { // For other cases except wishlist and checkout
//This block doesn't get called for checkout login, only for other cases except wishlist and checkout


                           /*30-08-2018 old Variables.useSmartLock = false;
                            Variables.cusToken = "";
                            Variables.numcartItems = 0;*/
                    // Variables.calledUpgrade = false;


                    Variables.fragmentUrl = "";//5-9-2018 old need TODO:ADD EXCEPT FOR FORGOT PW???

                    //30-08-2018 old  Variables.taskLoadingCount = 0;

                           /*30-08-2018 old  Variables.loginPost = false;

                            Variables.isInsideSessionLogin = false;
                            Variables.isActiveinEmail = false;
                            Variables.isActiveinPassword = false;
                            Variables.isTokenSuccess = false;*/


                    //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = false;
                    if (((MainActivity) ctx).session.isLoggedIn()) {
                                /*30-08-2018 old
                                ((MainActivity) ctx).session.logoutUser();//may require cookies here for checkout, so i do not clear


                                ((MainActivity) ctx).setupBadge();
                                ((MainActivity) ctx).credential = null;

                                ((MainActivity) ctx).updateDrawerLayout();*/
                        ((MainActivity) ctx).logout();//may require cookies here for checkout, so i do not clear 30-08-2018


                    }

//TODO 5-9-2018 :here I am loading login page behind native UI
                    //TODO 5-9-2018 :BUT AT SAME TIME CLEAR HISTORY?WILL IT HAVE AFFECT TO ANYTHING?
                    //
////This doesn't cause logout,just delete history -//TODO 5-9-2018 CHECK- HISTORY OF URLS ONLY???
                    webView.clearHistory(); //5-9-2018 old need TODO:ADD EXCEPT FOR FORGOT PW???  //29-08-2018 old need? webView.goBack();

                    //24-9-2018 old Log.d("FORGOTPWshowLogin", "FORGOTPWshowLogin");
                    ((MainActivity) ctx).showLogin(); //5-9-2018 old need
                    ((MainActivity) ctx).prgDialogLoading.dismiss();
                    return true;//5-9-2018 added EXCEPT FOR FORGOT PW
                    // if return true page will not load in the UI but it
                    //really loads and onpagefinished is also get called,
                    // but js methods will not be called since we do not load UI
                    // Here i do not need to load UI for login web page
                    //so return true is correct as we show native login
                    //only when render login page UI the js method is getting called
                    //although method is inside https://www.gaincity.net/customer/account/forgotpasswordpost/
                    //js method is not going to be called until render the page
                    //because js is for client side execution and "forgotpasswordpost/" link, it self
                    //do not load any UI, it does login page and when login page loads, js methods
                    //inside "forgotpasswordpost/" link will be called
                    //that's why without loading UI of login page, we can't call that js method

                    //1.with js I will check clicking forgot pw button and get data inside text field and
                    //save inside variable,then dispaly in native login and reset variable when
                    //user go to other fragment, to any new web page except login
                    //2.OR if you can call js method at the time user click button before load
                    //any page, then it should also work fine

                    //can not use cookies as cant clear cookie data
                    //

                } /*29-08-2018 No need , but keep commented code
                for checking  possible future errors

                else {//for checkout we gives loginform separately, here we do not show
                    calledShowcheckoutLogin = false;

                }*/


//5-9-2018 EXCEPT FOR FORGOT PW - return statement loads 'https://www.gaincity.net/customer/account/login/referer'
                //shows onpagefinished for that url is also called, but page is not loading
                //5-9-2018 old need EXCEPT FOR FORGOT PW     return true;


            }
            //29-08-2018 e

            if (((((MainActivity) ctx).session.isLoggedIn()) && (Variables.loginPost || Variables.isTokenSuccess))) {


                String lastURL = ((MainActivity) ctx).session.getlastURL();

                //24-9-2018 old Log.d("checklastURL", "checklastURL" + lastURL);

                if (lastURL.trim().length() == 0) {
                    //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = true;
                    view.loadUrl(Variables.appendToHomeUrl(""));
                    return true;
                }


                //24-9-2018 old Log.d("logoutSuccess33", "logoutSuccess33" + lastURL);

                if ((!(lastURL.equals(""))) && (!(lastURL.contains(Variables.URL_HOME + "customer/account")))) {

                    //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = true;
                    view.loadUrl(lastURL);
                    return true;

                } else if ((lastURL.contains(Variables.URL_HOME + "customer/account/logoutSuccess")) || (lastURL.contains(Variables.URL_HOME + "customer/account/login"))) {//27-08-2018 old ok need? || (lastURL.contains(Variables.URL_HOME + "customer/account/login"))) {

                    //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = true;
                    //24-9-2018 old Log.d("logoutSuccess22", "logoutSuccess22" + lastURL);
                    view.loadUrl(Variables.appendToHomeUrl(""));
                    return true;
                }

            }

            ////26-10-2018 s
            if (url.startsWith("https://www.youtube.com")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);

                return true; // we handled the url loading
            }
            //code from top added 26-10-2018 s
            if ((CookieManager.getInstance().getCookie(url) == null) && (!(url.equals("https://maintenance.gaincity.com/")))) {//for call us button
                return true;// if return true page will not load in the UI but it
                //really loads and onpagefinished is also get called,
            }
            //code from top added 26-10-2018 e
//26-10-2018 e


            return false;

            //7-9-2018 e

//29-08-2018 s
/*7-9-2018 old commented s
            try {
                connected = StaticMethods.isNetworkAvailable((MainActivity) ctx);
            } catch (Exception e) {
                e.printStackTrace();
                StaticMethods.sendCrashReport(((MainActivity) ctx), e);
            }


            if (!connected) {




                if (Variables.fragmentUrl.contains(Variables.URL_HOME + "customer/account/loginPost?login")) {
                    if (((MainActivity) ctx).session.isLoggedIn()) {
                        ((MainActivity) ctx).session.logoutUser();//may require cookies here for loginpost
                        // I do not use logout(),
                        // since I dont want to reset variables here, they are already
                        //reseted when we call this loginPost url
                    }
                }
                Log.d("onPageStartederr2", "onPageStartederr2");

                if ((((MainActivity) ctx).prgDialogLoading != null) && ((((MainActivity) ctx).prgDialogLoading).isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.dismiss();
                }

                if ((((MainActivity) ctx).prgdialogAdding != null) && ((((MainActivity) ctx).prgdialogAdding).isShowing())) {
                    ((MainActivity) ctx).prgdialogAdding.dismiss();
                }


                if ((((MainActivity) ctx).prgdialog_noti) != null) {
                    if (((MainActivity) ctx).prgdialog_noti.isShowing()) {
                        ((MainActivity) ctx).prgdialog_noti.dismiss();
                    }
                }

                if (((((MainActivity) ctx).prgdialog) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                    (((MainActivity) ctx).prgdialog).dismiss();
                }

                if (((((MainActivity) ctx).prgdialogsearch) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                    (((MainActivity) ctx).prgdialogsearch).dismiss();
                }

                if (((((MainActivity) ctx).prgdialogLogout) != null) && ((((MainActivity) ctx).prgdialogLogout).isShowing())) {
                    (((MainActivity) ctx).prgdialogLogout).dismiss();
                    //27-08-2018 s
                    ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
                }


                view.stopLoading();
                StaticMethods.noConnection(ctx);
                return false;
            }



            if((!(Variables.notificationURL.equals("")))&&(url.equals(Variables.notificationURL))){

                Variables.notificationURL="";
            }


            *//*27-08-2018 OLD NEED if (((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer")))  && (Variables.loginMethod == 1)) {
                Log.d("needShowLoginWishl22", "needShowLoginWishl22");


                if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
                    ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
                    ((MainActivity) ctx).prgDialogLoading.show();

                }
                ((MainActivity) ctx).showLogin();

                return false;


            }*//*


            if (((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer"))) && needShowLoginForCheckout) {
                Log.d("needShowLoginchk22", "needShowLoginchk22" + needShowLoginForCheckout);


                needShowLoginForCheckout = false;
                if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.setMessage(getResources().getString(R.string.loadingDialog));
                    ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
                    ((MainActivity) ctx).prgDialogLoading.show();

                }
                showcheckoutLogin2();
                webView.loadUrl("https://www.gaincity.net/checkout/cart/");//After showing native login it redirects
                //webview to cart page
                return true;


            }
//29-08-2018 s
            else if ((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer"))) {


                // Log.d("calledShowcheck33", "calledShowcheck33"+calledShowcheckoutLogin);

                Log.d("WhenRedirected11", "WhenRedirected11");
                if(Variables.loginMethod == 1){//27-08-2018 S For wishlist
                    Log.d("needShowLoginWishlJS1", "needShowLoginWishlJS1");

   *//* if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
        ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
        ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
        ((MainActivity) ctx).prgDialogLoading.show();

    }*//*
                    //This doesn't cause logout,just delete history
                    webView.clearHistory();// need to clear history otherwise going back to webview
                    // from loginfragment on backpressed, gives loginform back sometimes

                    //view.loadUrl("file:///android_asset/blankPage.html");
                    // webView.stopLoading();
                    //   webView.goBack();
                    //  String lastURLExceptLogin = ((MainActivity) ctx).session.getlastURLExceptLogin();
                    // webView.loadUrl(lastURLExceptLogin);
                    // webView.loadUrl(Variables.URL_HOME);//do not allow loading login page instead go to home
                    // to use when back pressed from login form

                    ((MainActivity) ctx).showLogin();
                    //Note:when we get LoginFragment inside showLogin(), there I reset Variables
                    return true;

                }



                else{ // No need , but keep commented code for checking  possible future errors  else if (!calledShowcheckoutLogin) { // For other cases except wishlist and checkout
//This block doesn't get called for checkout login, only for other cases except wishlist and checkout


                    *//*30-08-2018 old
                    Variables.useSmartLock = false;
                    Variables.cusToken = "";
                    Variables.numcartItems = 0;*//*
                  //  Variables.calledUpgrade = false;

                    Variables.fragmentUrl = "";

                    //30-08-2018 old  Variables.taskLoadingCount = 0;

                    *//*30-08-2018 old
                    Variables.loginPost = false;

                    Variables.isInsideSessionLogin = false;
                    Variables.isActiveinEmail = false;
                    Variables.isActiveinPassword = false;
                    Variables.isTokenSuccess = false;*//*


                    //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = false;
                    if (((MainActivity) ctx).session.isLoggedIn()) {
                        *//*30-08-2018 old ((MainActivity) ctx).session.logoutUser();//may require cookies here for checkout, so i do not clear


                        ((MainActivity) ctx).setupBadge();
                        ((MainActivity) ctx).credential = null;

                        ((MainActivity) ctx).updateDrawerLayout();*//*
                        ((MainActivity) ctx).logout();//may require cookies here for checkout, so i do not clear 30-08-2018

                    }

Log.d("FORGOTPWshowLogin22","FORGOTPWshowLogin22");
////This doesn't cause logout,just delete history
                   webView.clearHistory(); //29-08-2018 old need? webView.goBack();


                    ((MainActivity) ctx).showLogin();
                    ((MainActivity) ctx).prgDialogLoading.dismiss();


                } *//*29-08-2018 No need , but keep commented code
                for checking  possible future errors

                else {//for checkout we gives loginform separately, here we do not show
                    calledShowcheckoutLogin = false;

                }*//*






                return true;


            }
            //29-08-2018 e

            if (((((MainActivity) ctx).session.isLoggedIn()) && (Variables.loginPost || Variables.isTokenSuccess))) {


                String lastURL = ((MainActivity) ctx).session.getlastURL();

                Log.d("checklastURL", "checklastURL" + lastURL);

                if (lastURL.trim().length() == 0) {
                    //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = true;
                    view.loadUrl(Variables.appendToHomeUrl(""));
                    return true;
                }


                Log.d("logoutSuccess33", "logoutSuccess33" + lastURL);

                if ((!(lastURL.equals(""))) && (!(lastURL.contains(Variables.URL_HOME + "customer/account")))) {

                    //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = true;
                    view.loadUrl(lastURL);
                    return true;

                } else if ((lastURL.contains(Variables.URL_HOME + "customer/account/logoutSuccess")) || (lastURL.contains(Variables.URL_HOME + "customer/account/login"))) {//27-08-2018 old ok need? || (lastURL.contains(Variables.URL_HOME + "customer/account/login"))) {

                    //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = true;
                    Log.d("logoutSuccess22", "logoutSuccess22" + lastURL);
                    view.loadUrl(Variables.appendToHomeUrl(""));
                    return true;
                }

            }


            return false;
            //29-09-2018 e
//7-9-2018 old commented e*/
        }


        //5-9-2018 TODO:ADD shouldoverrideurlloading text from file
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {


            //Log.d("canNotGoBack", "canNotGoBack" + url);

//7-9-2018 s
            /*26-10-2018 old need added below
            if ((CookieManager.getInstance().getCookie(url) == null)&&(!(url.equals("https://maintenance.gaincity.com/")))) {
                return true;// if return true page will not load in the UI but it
                //really loads and onpagefinished is also get called,
                //required for phone call?- 26-9-2018
            }*/
//7-9-2018 e

            try {
                connected = StaticMethods.isNetworkAvailable((MainActivity) ctx);
            } catch (Exception e) {
                e.printStackTrace();
                StaticMethods.sendCrashReport(((MainActivity) ctx), e);
            }


            if (!connected) {


                if (Variables.fragmentUrl.contains(Variables.URL_HOME + "customer/account/loginPost?login")) {
                    if (((MainActivity) ctx).session.isLoggedIn()) {

                        ((MainActivity) ctx).session.logoutUser();//may require cookies here for loginpost
                        // I do not use logout(),
                        // since I dont want to reset variables here, they are already
                        //reseted when we call this loginPost url


                    }
                }
                //24-9-2018 old Log.d("onPageStartederr2", "onPageStartederr2");

                if ((((MainActivity) ctx).prgDialogLoading != null) && ((((MainActivity) ctx).prgDialogLoading).isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.dismiss();
                }

                if ((((MainActivity) ctx).prgdialogAdding != null) && ((((MainActivity) ctx).prgdialogAdding).isShowing())) {
                    ((MainActivity) ctx).prgdialogAdding.dismiss();
                }


                if ((((MainActivity) ctx).prgdialog_noti) != null) {
                    if (((MainActivity) ctx).prgdialog_noti.isShowing()) {
                        ((MainActivity) ctx).prgdialog_noti.dismiss();
                    }
                }

                if (((((MainActivity) ctx).prgdialog) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                    (((MainActivity) ctx).prgdialog).dismiss();
                }

                if (((((MainActivity) ctx).prgdialogsearch) != null) && ((((MainActivity) ctx).prgdialogsearch).isShowing())) {
                    (((MainActivity) ctx).prgdialogsearch).dismiss();
                }

                if (((((MainActivity) ctx).prgdialogLogout) != null) && ((((MainActivity) ctx).prgdialogLogout).isShowing())) {
                    (((MainActivity) ctx).prgdialogLogout).dismiss();
                    //27-08-2018 s
                    ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
                }


                view.stopLoading();
                StaticMethods.noConnection((MainActivity) ctx);
                return false;
            }


            if ((!(Variables.notificationURL.equals(""))) && (url.equals(Variables.notificationURL))) {

                Variables.notificationURL = "";
            }

//17-9-2018 s
            if (url.contains(Variables.URL_HOME + "checkout/onepage/success/")) {
                Variables.stoppedUsingJs = false;
                /*if (((MainActivity) ctx).prgDialogLoadingPurchase.isShowing()) {
                    ((MainActivity) ctx).prgDialogLoadingPurchase.dismiss();
                }*/
            }
            //17-9-2018 e

            /*27-08-2018 OLD NEED if (((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer")))  && (Variables.loginMethod == 1)) {
                Log.d("needShowLoginWishl22", "needShowLoginWishl22");


                if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
                    ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
                    ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
                    ((MainActivity) ctx).prgDialogLoading.show();

                }
                ((MainActivity) ctx).showLogin();

                return false;


            }*/


            if (((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer"))) && needShowLoginForCheckout) {
                //24-9-2018 old Log.d("needShowLoginchk22", "needShowLoginchk22" + needShowLoginForCheckout);


                needShowLoginForCheckout = false;
                if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
                    /*13-9-2018 old need ((MainActivity) ctx).prgDialogLoading.setMessage(getResources().getString(R.string.loadingDialog));
                    ((MainActivity) ctx).prgDialogLoading.setCancelable(false);*/
                    ((MainActivity) ctx).prgDialogLoading.show();

                }
                showcheckoutLogin2();
                webView.loadUrl(Variables.URL_HOME + "checkout/cart/");//After showing native login it redirects
                //webview to cart page
                return true;


            }
//29-08-2018 s
            else if ((url.equals(Variables.appendToHomeUrl("customer/account/login/"))) || (url.contains(Variables.URL_HOME + "customer/account/login/referer"))) {


                // Log.d("calledShowcheck33", "calledShowcheck33"+calledShowcheckoutLogin);

                //24-9-2018 old Log.d("WhenRedirected22", "WhenRedirected22");
                if (Variables.loginMethod == 1) {//27-08-2018 S For wishlist
                    //24-9-2018 old Log.d("needShowLoginWishlJS1", "needShowLoginWishlJS1");

   /* if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
        ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
        ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
        ((MainActivity) ctx).prgDialogLoading.show();

    }*/
                    //This doesn't cause logout,just delete history
                    webView.clearHistory();// need to clear history otherwise going back to webview
                    // from loginfragment on backpressed, gives loginform back sometimes

                    //view.loadUrl("file:///android_asset/blankPage.html");
                    // webView.stopLoading();
                    //   webView.goBack();
                    //  String lastURLExceptLogin = ((MainActivity) ctx).session.getlastURLExceptLogin();
                    // webView.loadUrl(lastURLExceptLogin);
                    // webView.loadUrl(Variables.URL_HOME);//do not allow loading login page instead go to home
                    // to use when back pressed from login form

                    ((MainActivity) ctx).showLogin();
                    //Note:when we get LoginFragment inside showLogin(), there I reset Variables

                    return true;

                } else { //  else if (!calledShowcheckoutLogin) { // For other cases except wishlist and checkout
//This block doesn't get called for checkout login, only for other cases except wishlist and checkout


                           /*30-08-2018 old Variables.useSmartLock = false;
                            Variables.cusToken = "";
                            Variables.numcartItems = 0;*/
                    // Variables.calledUpgrade = false;


                    Variables.fragmentUrl = "";//5-9-2018 old need TODO:ADD EXCEPT FOR FORGOT PW???

                    //30-08-2018 old  Variables.taskLoadingCount = 0;

                           /*30-08-2018 old  Variables.loginPost = false;

                            Variables.isInsideSessionLogin = false;
                            Variables.isActiveinEmail = false;
                            Variables.isActiveinPassword = false;
                            Variables.isTokenSuccess = false;*/


                    //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = false;
                    if (((MainActivity) ctx).session.isLoggedIn()) {
                                /*30-08-2018 old
                                ((MainActivity) ctx).session.logoutUser();//may require cookies here for checkout, so i do not clear


                                ((MainActivity) ctx).setupBadge();
                                ((MainActivity) ctx).credential = null;

                                ((MainActivity) ctx).updateDrawerLayout();*/
                        ((MainActivity) ctx).logout();//may require cookies here for checkout, so i do not clear 30-08-2018


                    }

//TODO 5-9-2018 :here I am loading login page behind native UI
                    //TODO 5-9-2018 :BUT AT SAME TIME CLEAR HISTORY?WILL IT HAVE AFFECT TO ANYTHING?
                    //
////This doesn't cause logout,just delete history -//TODO 5-9-2018 CHECK- HISTORY OF URLS ONLY???
                    webView.clearHistory(); //5-9-2018 old need TODO:ADD EXCEPT FOR FORGOT PW???  //29-08-2018 old need? webView.goBack();

                    //24-9-2018 old Log.d("FORGOTPWshowLogin", "FORGOTPWshowLogin");
                    ((MainActivity) ctx).showLogin(); //5-9-2018 old need
                    ((MainActivity) ctx).prgDialogLoading.dismiss();
                    return true;//5-9-2018 added EXCEPT FOR FORGOT PW
                    // if return true page will not load in the UI but it
                    //really loads and onpagefinished is also get called,
                    // but js methods will not be called since we do not load UI
                    // Here i do not need to load UI for login web page
                    //so return true is correct as we show native login
                    //only when render login page UI the js method is getting called
                    //although method is inside https://www.gaincity.net/customer/account/forgotpasswordpost/
                    //js method is not going to be called until render the page
                    //because js is for client side execution and "forgotpasswordpost/" link, it self
                    //do not load any UI, it does login page and when login page loads, js methods
                    //inside "forgotpasswordpost/" link will be called
                    //that's why without loading UI of login page, we can't call that js method

                    //1.with js I will check clicking forgot pw button and get data inside text field and
                    //save inside variable,then dispaly in native login and reset variable when
                    //user go to other fragment, to any new web page except login
                    //2.OR if you can call js method at the time user click button before load
                    //any page, then it should also work fine

                    //can not use cookies as cant clear cookie data
                    //

                } /*29-08-2018 No need , but keep commented code
                for checking  possible future errors

                else {//for checkout we gives loginform separately, here we do not show
                    calledShowcheckoutLogin = false;

                }*/


//5-9-2018 EXCEPT FOR FORGOT PW - return statement loads 'https://www.gaincity.net/customer/account/login/referer'
                //shows onpagefinished for that url is also called, but page is not loading
                //5-9-2018 old need EXCEPT FOR FORGOT PW     return true;


            }
            //29-08-2018 e

            if (((((MainActivity) ctx).session.isLoggedIn()) && (Variables.loginPost || Variables.isTokenSuccess))) {


                String lastURL = ((MainActivity) ctx).session.getlastURL();

                //24-9-2018 old Log.d("checklastURL", "checklastURL" + lastURL);

                if (lastURL.trim().length() == 0) {
                    //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = true;
                    view.loadUrl(Variables.appendToHomeUrl(""));
                    return true;
                }


                //24-9-2018 old Log.d("logoutSuccess33", "logoutSuccess33" + lastURL);

                if ((!(lastURL.equals(""))) && (!(lastURL.contains(Variables.URL_HOME + "customer/account")))) {

                    //30-08-2018 OLD assigned but never used    Variables.lastURLcalled = true;
                    view.loadUrl(lastURL);
                    return true;

                } else if ((lastURL.contains(Variables.URL_HOME + "customer/account/logoutSuccess")) || (lastURL.contains(Variables.URL_HOME + "customer/account/login"))) {//27-08-2018 old ok need? || (lastURL.contains(Variables.URL_HOME + "customer/account/login"))) {

                    //30-08-2018 OLD assigned but never used   Variables.lastURLcalled = true;
                    //24-9-2018 old Log.d("logoutSuccess22", "logoutSuccess22" + lastURL);
                    view.loadUrl(Variables.appendToHomeUrl(""));
                    return true;
                }

            }


////26-10-2018 s
            if (url.startsWith("https://www.youtube.com")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);

                return true; // we handled the url loading
            }
            //code from top added 26-10-2018 s
            if ((CookieManager.getInstance().getCookie(url) == null) && (!(url.equals("https://maintenance.gaincity.com/")))) {
                return true;// if return true page will not load in the UI but it
                //really loads and onpagefinished is also get called,
                //required for phone call?- 26-9-2018
            }
            //code from top added 26-10-2018 e
//26-10-2018 e


            //31-10-2018 s
            //Log.d("catalogsearch","catalogsearch"+url);
            if ((url.contains(Variables.URL_HOME + "catalogsearch/result/?q=")) && (!((((MainActivity) ctx).prgdialogsearch).isShowing()))) {
                (((MainActivity) ctx).prgdialogsearch).show();
            }
            //31-10-2018 e

            return false;


        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            try {
                if (((MainActivity) ctx).prgdialogAdding.isShowing()) {
                    ((MainActivity) ctx).prgdialogAdding.dismiss();
                } else if (((MainActivity) ctx).prgDialogLoading.isShowing()) {
                    ((MainActivity) ctx).prgDialogLoading.dismiss();
                }

                //12-9-2018 S
                if ((((MainActivity) ctx).prgCancelled) != null) {
                    if (((MainActivity) ctx).prgCancelled.isShowing()) {
                        ((MainActivity) ctx).prgCancelled.dismiss();
                    }
                }
                //12-9-2018 E
                //13-9-2018 s
                if (((MainActivity) ctx).prgdialogHome.isShowing()) {

                    ((MainActivity) ctx).prgdialogHome.dismiss();
                }//13-9-2018 e
                if (((MainActivity) ctx).prgdilogBtn.isShowing()) {

                    ((MainActivity) ctx).prgdilogBtn.dismiss();
                }
                if ((((MainActivity) ctx).prgdialog) != null) {
                    if (((MainActivity) ctx).prgdialog.isShowing()) {
                        ((MainActivity) ctx).prgdialog.dismiss();
                    }
                }
                if ((((MainActivity) ctx).prgdialogLogout) != null) {
                    if (((MainActivity) ctx).prgdialogLogout.isShowing()) {
                        ((MainActivity) ctx).prgdialogLogout.dismiss();
                        //27-08-2018 s
                        ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
                        return;
                    }
                }
                if ((((MainActivity) ctx).prgdialog_noti) != null) {
                    if (((MainActivity) ctx).prgdialog_noti.isShowing()) {
                        ((MainActivity) ctx).prgdialog_noti.dismiss();
                    }
                }

                if ((((MainActivity) ctx).prgdialogsearch) != null) {
                    if (((MainActivity) ctx).prgdialogsearch.isShowing()) {
                        ((MainActivity) ctx).prgdialogsearch.dismiss();
                    }
                }

                if ((((MainActivity) ctx).prgdialogChkout) != null) {
                    if ((((MainActivity) ctx).prgdialogChkout).isShowing()) {
                        (((MainActivity) ctx).prgdialogChkout).dismiss();
                    }
                }


                //check error for not showing messages.html
                // Log.d("onReceivedError", "onReceivedError" + description);//26-9-2018
                view.loadUrl("file:///android_asset/errorPage.html");//This is required to avoid showing error on mainfragment
                //so when user comes back from ErrorFragment it doesnt show web page error just only show blank page when redirecting

                ErrorFragment fragment = new ErrorFragment();

                if (description.equals("net::ERR_INTERNET_DISCONNECTED")) {

                    Bundle bundle = new Bundle();
                    bundle.putString("error", getResources().getString(R.string.no_internet));
                    fragment.setArguments(bundle);

                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("error", getResources().getString(R.string.error_occured));
                    fragment.setArguments(bundle);
                }


                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.category_cont, fragment, ErrorFragment.TAG);

                ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                ft.commit();
            }
//12-2-2019 s
            catch (IllegalStateException e) {
                ((MainActivity) ctx).finish();
                System.exit(0);
            }
//12-2-2019 e
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {

            //24-9-2018 old Log.d("LOLLIPOP16CCCC", "LOLLIPOP16CCCC" + url);
            //24-9-2018 old Log.d("LOLLIPOP16CCCC", "LOLLIPOP16CCCC" + CookieManager.getInstance().getCookie(url));
//13-9-2018 s
            //not working for android 5.0
            /*if(((MainActivity) ctx).prgdialogHome.isShowing()){

                ((MainActivity) ctx).prgdialogHome.dismiss();
            }*///13-9-2018 e

        }


        @Override
        public void onLoadResource(WebView view, String url) {

            //Log.d("onLoadResourceFORM", "onLoadResourceFORM" + url);
            // Log.d("needShowLoginWishl21", "needShowLoginWishl21"+ url);


            if (url.contains(Variables.URL_HOME + "cybersource/index/receipt/")) {
                Variables.stoppedUsingJs = true;

                /*13-9-2018 old need ((MainActivity) ctx).prgDialogLoadingPurchase.setMessage(getResources().getString(R.string.loadingDialog));
                ((MainActivity) ctx).prgDialogLoadingPurchase.setCancelable(false);*/

                if (!(((MainActivity) ctx).prgDialogLoadingPurchase.isShowing())) {//17-9-2018
                    ((MainActivity) ctx).prgDialogLoadingPurchase.show();
                }

            }
            if (!Variables.stoppedUsingJs) {
                try {

                    //7-9-2018 s
                    if (Variables.insideCheckout == false) {
                        if (((MainActivity) ctx).session.isLoggedIn()) {//3-9-2018
                            //24-9-2018 old Log.d("cyberurl", "cyberurl" + url);
                            view.loadUrl(
                                    "javascript:(function() {window.onload = function() {" +


                                            " if (typeof jQuery === 'undefined') {" +

                                            "} else {" +

//28-08-2018 s
                                            "var myaccount = document.getElementById('myaccount');" +

                                            "if(myaccount==null){" +
                                            // "alert(myaccount);"+
                                            "window.MyJavaScriptInterface.nativelogout('" + url + "');" +
                                            "}" +

                                            //28-08-2018 e

                                            /*7-12-2018 old
                                            "jQuery('#proceed_to_checkout').on('click', function() {" +

                                            "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                            " });" +*/
                                            "jQuery('#gotoCheckout').on('click', function() {" +

                                            "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                            " });" +

                                            "jQuery('#product-addtocart-button').on('click', function() {" +

                                            "window.MyJavaScriptInterface.addingToCart();" +
                                            " });" +


                                            "}" +

                                            "}})()"


                            );
                            //28-08-2018 s
                        } else if (!(((MainActivity) ctx).session.isLoggedIn())) {
                            view.loadUrl(
                                    "javascript:(function() {window.onload = function() {" +


                                            " if (typeof jQuery === 'undefined') {" +

                                            "} else {" +


                                            "var myaccount = document.getElementById('myaccount');" +
                                            "if(myaccount!=null){" +
                                            //"alert('webviewlogout');"+
                                            "window.MyJavaScriptInterface.webviewlogout();" +
                                            "}" +


                                            /*7-12-2018 old
                                            "jQuery('#proceed_to_checkout').on('click', function() {" +

                                            "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                            " });" +*/
                                            "jQuery('#gotoCheckout').on('click', function() {" +

                                            "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                            " });" +

                                            "jQuery('#product-addtocart-button').on('click', function() {" +

                                            "window.MyJavaScriptInterface.addingToCart();" +
                                            " });" +


                                            "}" +

                                            "}})()"


                            );
                        }
                    }
//7-9-2018 e
                    //3-9-2018 old ok  if(((MainActivity) ctx).session.isLoggedIn()) {//28-08-2018
                   /* if((((MainActivity) ctx).session.isLoggedIn())&&
                            (!(url.contains("https://testsecureacceptance.cybersource.com")))&&
                           // (!(url.contains("https://testsecureacceptance.cybersource.com/checkout_update")))&&
                            (!(url.contains("https://0eafstag.cardinalcommerce.com")))&&
                            (!(url.contains("https://staging.cdn-net.com")))&&
                    (!(url.contains("https://six.cdn-net.com")))&&
                            (!(url.contains("https://tm.cybersource.com")))&&
                            (!(url.contains("https://testcustomer34.cardinalcommerce.com")))&&
                            (!(url.contains("https://ajax.googleapis.com")))&&
                            //for 2c2p
                            (!(url.contains("https://sandbox.wallet.masterpass.com")))&&
                            (!(url.contains("https://sandbox.masterpass.com")))&&
                            (!(url.contains("https://demo2.2c2p.com")))&&
                            (!(url.contains("https://d2ah0ha08b90zg.cloudfront.net")))
                            ) {//3-9-2018
Log.d("cyberurl","cyberurl"+url);
                        view.loadUrl(
                                "javascript:(function() {window.onload = function() {" +


                                        " if (typeof jQuery === 'undefined') {" +

                                        "} else {" +

//28-08-2018 s
                                        "var myaccount = document.getElementById('myaccount');" +
                                        "if(myaccount==null){" +
                                       // "alert(myaccount);"+
                                        "window.MyJavaScriptInterface.nativelogout('"+url+"');" +
                                        "}" +

                                        //28-08-2018 e

                                        "jQuery('#proceed_to_checkout').on('click', function() {" +

                                        "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                        " });" +
                                        "jQuery('#gotoCheckout').on('click', function() {" +

                                        "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                        " });" +

                                        "jQuery('#product-addtocart-button').on('click', function() {" +

                                        "window.MyJavaScriptInterface.addingToCart();" +
                                        " });" +


                                        "}" +

                                        "}})()"


                        );
                       //28-08-2018 s
                    }

                    else  if(!(((MainActivity) ctx).session.isLoggedIn())){
                        view.loadUrl(
                                "javascript:(function() {window.onload = function() {" +


                                        " if (typeof jQuery === 'undefined') {" +

                                        "} else {" +


                                        "var myaccount = document.getElementById('myaccount');" +
                                        "if(myaccount!=null){" +
                                        //"alert('webviewlogout');"+
                                        "window.MyJavaScriptInterface.webviewlogout();" +
                                        "}" +



                                        "jQuery('#proceed_to_checkout').on('click', function() {" +

                                        "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                        " });" +
                                        "jQuery('#gotoCheckout').on('click', function() {" +

                                        "window.MyJavaScriptInterface.showcheckoutLogin();" +
                                        " });" +

                                        "jQuery('#product-addtocart-button').on('click', function() {" +

                                        "window.MyJavaScriptInterface.addingToCart();" +
                                        " });" +


                                        "}" +

                                        "}})()"


                        );
                    }*/

                    //28-08-2018 e


                } catch (Exception e) {
                    e.printStackTrace();
                    StaticMethods.sendCrashReport(((MainActivity) ctx), e);
                }

            }


        }


        @Override
        public void onPageFinished(WebView view, String url) {
            //  manageCookies(url);//2-11-2018 -TODO:REMOVE
            //24-9-2018 old Log.d("needShowLoginWishl23", "needShowLoginWishl23" + url);
            //24-9-2018 old Log.d("LOLLIPOP16KKKK", "LOLLIPOP16KKKK" + url);
            //Log.d("LOLLIPOP16KKKK", "LOLLIPOP16KKKK" + CookieManager.getInstance().getCookie(url));
            waitTime = 1;//reset to 1 after waiting //TODO:CHECK 21-5-2018
            if (!connected) return;
//23-10-2018 s
            if (((MainActivity) ctx).prgdilogBtn.isShowing()) {

                ((MainActivity) ctx).prgdilogBtn.dismiss();
            }
            //23-10-2018 e

            if (((MainActivity) ctx).prgdialogsearch.isShowing()) {
                ((MainActivity) ctx).prgdialogsearch.dismiss();
            }


            //19-9-2018 s
            /*//21-9-2018 old
            if (((MainActivity) ctx).prgdialogCommon.isShowing()) {
                ((MainActivity) ctx).prgdialogCommon.dismiss();
            }*/
            //19-9-2018 e

//28-08-2018 s LOgout for testing
/*if(url.contains(Variables.URL_HOME+"/catalog/category/view/id/1287")){
    if(((MainActivity) ctx).session.isLoggedIn()){
        ((MainActivity) ctx).session.logoutUser();
    }
}*/
//29-08-2018 s
//12-9-2018 s
            if ((!(url.equals("https://secureacceptance.cybersource.com/canceled"))) && (!(url.equals("https://www.gaincity.com/cybersource/index/cancel/")))) {

                if ((((MainActivity) ctx).prgCancelled.isShowing())) {
                    ((MainActivity) ctx).prgCancelled.dismiss();
                }
            }
            //12-9-2018 e
            //12-9-2018 s
            if (url.contains(Variables.URL_HOME)) {
                //24-9-2018 old Log.d("clearWebView", "clearWebView" + ((MainActivity) ctx).session.getlastURL());
                //get first time returning from checkout and clear history
                if (((((MainActivity) ctx).session.getlastURL().equals(Variables.URL_HOME + "checkout/")) &&
                        (!(url.equals(Variables.URL_HOME + "checkout/#payment")))   //if user in payment last will be checkout, so avoid that condition
                ) ||
                        ((((MainActivity) ctx).session.getlastURL().equals(Variables.URL_HOME + "checkout/#payment")) &&
                                (!(url.equals(Variables.URL_HOME + "checkout/")))//go back to checkout from payment
                        )) {//comes from checkout back to gaincity website for the first time(last url will be
                    //https://www.gaincity.com/checkout/ or https://www.gaincity.com/checkout/#payment)
                    //24-9-2018 old Log.d("clearWebView", "clearWebView");
                    ((MainActivity) ctx).session.setlastURL(Variables.URL_HOME);
                    webView.clearHistory();//not working well if put in onpagestarted or shouldoverride..,(if used in onpagestarted -last page still there)//so can not go back to checkout page(becoz it gives error)
/*It appears that the history clears everything before the current page so if your browser is at page "A",
 you clear history and navigate to page "B" your history will be "A" "B", not just "B", but if you clear history
  when "B" finishes loading you will have only "B"*/
                }
            }
            //12-9-2018 e

            if (url.equals(Variables.URL_HOME)) {
                //24-9-2018 old Log.d("clearHistory", "clearHistory");
                webView.clearHistory();//This doesn't cause logout,just delete history
            }
//29-08-2018 e

            if ((Variables.loadCheckout == true) && (url.contains(Variables.URL_HOME + "wishlist/index/index/"))) {
                Variables.loadCheckout = false;
                view.loadUrl(Variables.URL_HOME + "checkout/");//if user clicked add to wish list but didn't login, when
                //next time login that item will be added to wish list and redirected to wishlist, so in such case
                //it will not go to checkout page from proceed to checkout button's login form
                //so i load checkout page manually here
            }

//28-08-2018 e

            //27-08-2018 s
            if ((!(url.contains(Variables.URL_HOME + "customer/account/login/referer"))) && (!(url.contains(Variables.URL_HOME + "customer/account/loginPost")))) {
                if ((((MainActivity) ctx).prgdialog_wishlist).isShowing()) {
                    (((MainActivity) ctx).prgdialog_wishlist).dismiss();
                }
            }

            //27-08-2018 e

            //19-9-2018  removed   if(url.equals(Variables.URL_HOME + "customer/account/")){ //19-9-2018  old if(url.contains(Variables.URL_HOME + "customer/account/loginPost?login")){

            if (!(Variables.notificationURL.equals(""))) {//19-9-2018 added ( after !

                if (((MainActivity) ctx).prgdialog.isShowing()) {
                    ((MainActivity) ctx).prgdialog.dismiss();
                }
                view.loadUrl(Variables.notificationURL);

                //19-9-2018  old  }


            }

            if (url.contains(Variables.URL_HOME + "checkout/onepage/success/")) {
                //17-9-2018 old need? added to shouldoverrideurlloading  Variables.stoppedUsingJs = false;
                if (((MainActivity) ctx).prgDialogLoadingPurchase.isShowing()) {
                    ((MainActivity) ctx).prgDialogLoadingPurchase.dismiss();
                }
            }


            if ((url.contains(Variables.URL_HOME + "checkout/cart/")) && (((MainActivity) ctx).prgDialogLoadingPurchase.isShowing())) {
                Variables.stoppedUsingJs = false;

                ((MainActivity) ctx).prgDialogLoadingPurchase.dismiss();

            }


            if ((((MainActivity) ctx).prgdialogLogout.isShowing()) && ((url.equals(Variables.appendToHomeUrl(""))) || (url.equals(Variables.appendToHomeUrl("customer/account/login/"))))) {//12-7-2018 customer/account/login/ part added since sometimes it redirects to "customer/account/login/" after logout . But it redirects to "customer/account/login/"
                //even if the user is logged in and then again redirected to https://www.gaincity.net/customer/account/
                // //since user is already logged in
                // So we can not depend on URL, anytime it can redirect to login


                ((MainActivity) ctx).prgdialogLogout.dismiss();
                //27-08-2018 s
                //5-9-2018 old need TODO:ADD EXCEPT FOR FORGOT PW   ((MainActivity) ctx).clearCookies();//After logout finish clear cookies

//27-08-2018 e
                return;

            }

            if (((MainActivity) ctx).prgdialog_noti.isShowing()) {
                ((MainActivity) ctx).prgdialog_noti.dismiss();
            }


            if (((((MainActivity) ctx).prgdialogChkout) != null) && ((url.equals(Variables.appendToHomeUrl("customer/account/create/"))) || (url.equals(Variables.appendToHomeUrl("customer/account/forgotpassword/"))) || (url.equals(Variables.appendToHomeUrl("checkout/"))) || (url.equals(Variables.appendToHomeUrl("customer/account/"))) || (url.equals(Variables.appendToHomeUrl("customer/account/edit/"))))) {//10-7-2018 replaced above line

                if ((((MainActivity) ctx).prgdialogChkout).isShowing()) {
                    (((MainActivity) ctx).prgdialogChkout).dismiss();
                }
            }


            if (((MainActivity) ctx).prgDialogLoading.isShowing()) {
                ((MainActivity) ctx).prgDialogLoading.dismiss();
            }


            //NO MATTER LOGGED IN OR NOT LOGGED IN, SAVE LAST URL- need this to redirect to last page
            //but after close app this session should be home
            //OR WHEN ONCREATE IF NOT LOGGED IN SET THIS TO HOME
            if (url.contains(Variables.URL_HOME)) {//12-9-2018
                ((MainActivity) ctx).session.setlastURL(url);
            }
            //Note:To go back to webview from native login form-need to store previous url except login url
            if (!(url.contains(Variables.URL_HOME + "customer/account/login/"))) {
                ((MainActivity) ctx).session.setlastURLExceptLogin(url);
            }

            //28-08-2018 s

            if (url.equals(Variables.URL_HOME)) {
                if (((MainActivity) ctx).dialogNativeLogout.isShowing()) {
                    ((MainActivity) ctx).dialogNativeLogout.dismiss();
                }
            }
//28-08-2018 e
            //24-9-2018 old Log.d("cookiesGC", "cookiesGC" + CookieManager.getInstance().getCookie(url));

            if (Variables.isInsideSessionLogin == true) {
                if (((MainActivity) ctx).prgdialog.isShowing()) {
                    ((MainActivity) ctx).prgdialog.dismiss();
                }
                Variables.isInsideSessionLogin = false;
            }
            if (Variables.loginPost == true) {
                //called when both using smart lock and not using smart lock
                Variables.loginPost = false;

                if (((MainActivity) ctx).prgdialog.isShowing()) {
                    ((MainActivity) ctx).prgdialog.dismiss();
                }
            }


            if (Variables.useSmartLock == true) {
                Variables.useSmartLock = false;
            }

//25-9-2018 s
            if ((((MainActivity) ctx).prgdialogMenu.isShowing()) &&
                    ((url.equals(Variables.loadedMenuItem)) || (url.equals(Variables.URL_HOME + "customer-service/faq#familycardmembership")))) {//29-10-2018
                ((MainActivity) ctx).prgdialogMenu.dismiss();
            }
            //25-9-2018 e
            //Log.d("customerloggingIn", "customerloggingIn" + Variables.customerloggingIn);


            //24-9-2018 old Log.d("cookies", "cookies" + CookieManager.getInstance().getCookie(url));
        }


    }
    //11-9-2018 e

    public class MyJavaScriptInterface {

        //23-10-2018 s
        @JavascriptInterface
        public void storeLocations() {
            //24-9-2018 old Log.d("phoneNumber", "phoneNumber" + phoneNumber);
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called
                //but now not necessary since avoided leaks

                ((MainActivity) ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) ctx).showStoreL_ns();

                    }
                });

            }
        }
        //23-10-2018 e

        //7-9-2018 s
        @JavascriptInterface
        public void callUs(String phoneNumber) {
            //24-9-2018 old Log.d("phoneNumber", "phoneNumber" + phoneNumber);
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called
                //but now not necessary since avoided leaks

                ((MainActivity) ctx).callGaincity(phoneNumber);
            }
        }

        @JavascriptInterface
        public void checkoutCompleted() {
            Variables.insideCheckout = false;
            //24-9-2018 old Log.d("checkoutCompleted", "checkoutCompleted");
        }

        //7-9-2018 e
//5-9-2018 s
        @JavascriptInterface
        public void getForgotPasswordEmail(String s) {

            Variables.forgotPasswordEmail = s;
    /*((MainActivity) ctx).runOnUiThread(new Runnable() {
        @Override
        public void run() {
            Log.d("getForgotTest","getForgotTest");

        }
    });*/
        }
        //5-9-2018 e

        //28-08-2018 s
        @JavascriptInterface
        public void nativelogout(String s) {
            //24-9-2018 old Log.d("nativelogouturl", "nativelogouturl" + s);
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                if (((MainActivity) ctx).session.isLoggedIn()) {

                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity) ctx).callNativeLogoutFromWebView();

                        }
                    });


                }
            }
        }

        @JavascriptInterface
        public void webviewlogout() {
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                if (!(((MainActivity) ctx).session.isLoggedIn())) {


                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity) ctx).callWebViewLogoutFromWebView();

                        }
                    });


                }
            }
        }

        //28-08-2018 e

       /*28-08-2018 commented @JavascriptInterface
        public void storeWishlistProduct(String product, String uenc, String form_key) {
            //call https://www.gaincity.net/wishlist/index/add/
            //after login
            //with POST
            //with  product, uenc, form_key
            Log.d("storeWishlistProduct", "storeWishlistProduct" + product + "===" + uenc + "===" + form_key);
        }*/


        @JavascriptInterface
        public void showcheckoutLogin() {

            //  calledShowcheckoutLogin=true;//login form for checkout-shows login form twice, so first form with guest btn and 2nd one without guest btn
            //since showNativeLoginWhenRedirected() is also called
            //   Log.d("calledShowcheck11", "calledShowcheck11"+calledShowcheckoutLogin);

            //NOTE: checkout can not use showNativeLoginWhenRedirected since it needs a different flow
            //like navigating to cart before load checkout
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                if (!(((MainActivity) ctx).session.isLoggedIn())) {
                    Variables.numcartItemsBeforeClickCheckout = 0;//14-1-2019 old need = Variables.numcartItems;
                    needShowLoginForCheckout = true;
                }
            }
        }

        //14-1-2019 s
        @JavascriptInterface
        public void showGuestcheckoutLogin() {

            //  calledShowcheckoutLogin=true;//login form for checkout-shows login form twice, so first form with guest btn and 2nd one without guest btn
            //since showNativeLoginWhenRedirected() is also called
            //   Log.d("calledShowcheck11", "calledShowcheck11"+calledShowcheckoutLogin);

            //NOTE: checkout can not use showNativeLoginWhenRedirected since it needs a different flow
            //like navigating to cart before load checkout
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                if (!(((MainActivity) ctx).session.isLoggedIn())) {
                    Variables.numcartItemsBeforeClickCheckout = Variables.numcartItems;
                    needShowLoginForCheckout = true;
                }
            }
        }
        //14-1-2019 e


        @JavascriptInterface
        public void showNativeLoginWhenRedirected() {//This method not required now, but for
            //any case which doesnt call shouldOverrideUrlLoading's login page load funactionality
            //this will be executed(eg.different login url). So I keep this method without deleting
//As I saw showNativeLoginWhenRedirected() is executed only after loading the login page, so
            //as I use shouldOverrideUrlLoading's login page load funactionality
            //this method doesnt get called. But I keep this for checking possible future  error of
            //different login url


            //  Log.d("calledShowcheck22", "calledShowcheck22"+calledShowcheckoutLogin);
            //24-9-2018 old Log.d("WhenRedirected33", "WhenRedirected33");
            if (Variables.loginMethod == 1) {//27-08-2018 S For wishlist
                //24-9-2018 old Log.d("needShowLoginWishlJS1", "needShowLoginWishlJS1");

   /* if (!(((MainActivity) ctx).prgDialogLoading.isShowing())) {
        ((MainActivity) ctx).prgDialogLoading.setMessage("Loading...2222 wishlist");
        ((MainActivity) ctx).prgDialogLoading.setCancelable(false);
        ((MainActivity) ctx).prgDialogLoading.show();

    }*/
                if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            // webView.goBack();
                            ((MainActivity) ctx).showLogin();// Inside LoginFragment , I reset Variables for logout

                        }
                    });
                }
            }
//27-08-2018 E

            //else if (!calledShowcheckoutLogin) { 29-08-2018 old - Note: no need now as new way-  shouldOverrideUrlLoading's login page load funactionality
            else {   // Note: For other cases except wishlist and checkout ( for checking checkout, used calledShowcheckoutLogin , now not using)

                //24-9-2018 old Log.d("forgotpw", "forgotpw");
                if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                       /*30-08-2018 old  Variables.useSmartLock = false;
                        Variables.cusToken = "";
                        Variables.numcartItems = 0;*/
                            // Variables.calledUpgrade = false;

                            Variables.fragmentUrl = "";

                            //30-08-2018 old   Variables.taskLoadingCount = 0;

                        /*30-08-2018 old
                        Variables.loginPost = false;

                        Variables.isInsideSessionLogin = false;
                        Variables.isActiveinEmail = false;
                        Variables.isActiveinPassword = false;
                        Variables.isTokenSuccess = false;*/


                            //30-08-2018 OLD assigned but never used     Variables.lastURLcalled = false;
                            if (((MainActivity) ctx).session.isLoggedIn()) {
                            /*30-08-2018 old
                            ((MainActivity) ctx).session.logoutUser();//may require cookies here for checkout, so i do not clear


                            ((MainActivity) ctx).setupBadge();
                            ((MainActivity) ctx).credential = null;

                            ((MainActivity) ctx).updateDrawerLayout();*/

                                ((MainActivity) ctx).logout();//may require cookies here , so i do not clear 30-08-2018

                            }

//This doesn't cause logout,just delete history
                            webView.clearHistory();//29-08-2018 old  webView.goBack();


                            ((MainActivity) ctx).showLogin();
                            ((MainActivity) ctx).prgDialogLoading.dismiss();
                        }


                    });
                }
            }/*29-08-2018 no need now as new way-  shouldOverrideUrlLoading's login page load funactionality

             else {//for checkout we gives loginform separately, here we do not show
                calledShowcheckoutLogin = false;

            }*/

        }


        @JavascriptInterface
        public void setCartQuantity(int x) {
            //24-9-2018 old Log.d("setCartQuantity", "setCartQuantity" + (MainActivity) ctx);
            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called
                Variables.numcartItems = x;
                ((MainActivity) ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) ctx).setupBadge();

                    }
                });
            }
        }


        @JavascriptInterface
        public void addingToCart() {

            if (((MainActivity) ctx) != null) {//11-9-2018 added to avoid freez when reopen after finish() called

                if (!(StaticMethods.isNetworkAvailable((MainActivity) ctx))) {//1-11-2018 - !( added
                    ((MainActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            StaticMethods.noConnection((MainActivity) ctx);

                        }
                    });


                }
            }
        }


    }

}