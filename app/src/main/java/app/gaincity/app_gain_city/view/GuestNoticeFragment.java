
package app.gaincity.app_gain_city.view;

import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment; //16-10-2018 old android.app.Fragment;
//24-9-2018 old import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Variables;


public class GuestNoticeFragment extends Fragment {
    public static final String TAG = "GuestNoticeFragment";

    View layout_v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout_v = inflater.inflate(R.layout.guest_notice, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018

        Variables.numcartItemsBeforeClickCheckout=Variables.numcartItems;//Note:Every fragment onresume I set this to 0
        //so even go to another fragment without getting any action in this fragment, it will not show guest checkout button
//14-1-2019 s Becoz in Add to wishlist login we should not have guest checkout button.- guest checkout btn only for checkout?14-1-2019 e



        return layout_v;
    }

    @Override
    public void onDestroy() {


        /*FragmentManager fm = getFragmentManager();
        GuestNoticeFragment guestFragment = (GuestNoticeFragment) fm.findFragmentByTag(GuestNoticeFragment.TAG);
        fm.beginTransaction().remove(guestFragment).commit();*/
        super.onDestroy();
        layout_v=null;
    }
}