package app.gaincity.app_gain_city.view;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.opengl.Visibility;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import app.gaincity.app_gain_city.R;

public class PureChatWebViewActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pure_chat_web_view);
        WebView webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setProgressBarVisibility(View.VISIBLE);

        webView.setWebViewClient(new MyWebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // enable for pure chat needs to access storage
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webSettings.setDatabasePath("/data/data/" + webView.getContext().getPackageName() + "/databases/");
        }
        webView.requestFocus(View.FOCUS_DOWN);

        String url =  "https://app.purechat.com/w/btyxbz";

        webView.loadUrl(url);

    }

    private void setProgressBarVisibility(int visibility) {
        // If a user returns back, a NPE may occur if WebView is still loading a page and then tries to hide a ProgressBar.
        if (progressBar != null) {
            progressBar.setVisibility(visibility);
        }
    }

    private class MyWebViewClient extends WebViewClient {


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            // TODO show you progress image
            super.onPageStarted(view, url, favicon);
            setProgressBarVisibility(View.VISIBLE);

        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            // TODO hide your progress image
            super.onPageFinished(view, url);
            setProgressBarVisibility(View.GONE);

        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if ("https://app.purechat.com".equals(Uri.parse(url).getHost())) {
                // This is my website, so do not override; let my WebView load the page
                return false;
            }
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            setProgressBarVisibility(View.GONE);
            handler.proceed();
        }
    }
}
