package app.gaincity.app_gain_city.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.gaincity.app_gain_city.Barcode.BarcodeCaptureFragment;
import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;

public class StoreMapActivity extends AppCompatActivity {

    Context ctx;
    static View v;
    static FragmentManager fm;
    public static final String TAG = "StoreMapActivity";
    private StoreMapActivity.Store_L_AsyncTask storeAsyncTask;
    private StoreMapActivity.CustomProgressDialog prgdilogStore;
    public static final int HANDLE_LOCATION = 5;
    private static final int HANDLE_CALL_PHONE_PERM = 4;//RC_HANDLE_CAMERA_PERM = 2
    private ViewPager mViewPager;
    private TabLayout mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_tabs);
        Context scaledContext = ScaledContextWrapper.wrap(this);
        LayoutInflater scaledInflater = LayoutInflater.from(scaledContext);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        getSupportActionBar().show();//12-11-2018

        Spannable text = new SpannableString("Store Location");
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(text);

        final Drawable backArrow = getResources().getDrawable(R.drawable.ic_arrow_back);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);

//        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.color.textColorPrimary));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //29-10-2018 E
        //29-10-2018 OLD  v = inflater.inflate(R.layout.store_tabs, container, false);
        ctx = this;
        prgdilogStore = new StoreMapActivity.CustomProgressDialog(this, R.drawable.prg_spinner);//18-10-2018
        Variables.storeData = new ArrayList<>();
        Variables.StoreMapData = new ArrayList<>();
        fm = getSupportFragmentManager();

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mTabs = (TabLayout) findViewById(R.id.result_tabs);
        if (Variables.storeData.size() == 0) {
            storeAsyncTask = new StoreMapActivity.Store_L_AsyncTask(ctx, Variables.STATIC_URL + "location.json", this);
            storeAsyncTask.execute();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public  class CustomProgressDialog extends Dialog {

        private ImageView iv;

        public CustomProgressDialog(Context context, int spinnerImg) {
            super(context, R.style.TransparentProgressDialog);
            WindowManager.LayoutParams wmp = getWindow().getAttributes();
            wmp.gravity = Gravity.CENTER_HORIZONTAL;
            getWindow().setAttributes(wmp);
            setCancelable(false);
            setOnCancelListener(null);
            setTitle(null);

            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iv = new ImageView(context);
            iv.setImageResource(spinnerImg);
            layout.addView(iv, params);
            addContentView(layout, params);
        }

        @Override
        public void show() {
            super.show();
            RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
            anim.setInterpolator(new LinearInterpolator());

            anim.setDuration(3000);
            anim.setRepeatCount(Animation.INFINITE);
            iv.setAnimation(anim);
            iv.startAnimation(anim);
        }
    }

    // Add Fragments to Tabs
    private static void setupViewPager(ViewPager viewPager) {
        StoreMapActivity.Adapter adapter = new StoreMapActivity.Adapter(fm);
        adapter.addFragment(new StoreMapFragment(), "MAP VIEW");
        adapter.addFragment(new StoreL_Fragment(), "LIST VIEW");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        v = null;
        ctx = null;//22-10-2018

        if (storeAsyncTask != null) {
            storeAsyncTask.cancel(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//7-9-2018 s
        //19-10-2018 s
        if (requestCode == HANDLE_LOCATION) {//5
            requestPermissionsResult(requestCode, permissions, grantResults);

        }
        //19-10-2018 e
        if (requestCode == HANDLE_CALL_PHONE_PERM) {//4
            requestPermissionsResult(requestCode, permissions, grantResults);

        } else if (requestCode == 2) {//RC_HANDLE_CAMERA_PERM = 2
            //7-9-2018 e

            //pass results of request camera permission to barcodecapturefragment
            FragmentManager fm = getSupportFragmentManager();//16-10-2018 old getFragmentManager();
            BarcodeCaptureFragment fragment = ((BarcodeCaptureFragment) fm.findFragmentByTag(BarcodeCaptureFragment.TAG));
            if (fragment != null)
                fragment.requestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    public void callGaincity(String phoneNumber) {
        //24-9-2018 old Log.d("phoneNumber22", "phoneNumber22"+phoneNumber);
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            Variables.GaincityPhoneNum = "";
            //24-9-2018 old Log.d("PERMISSION_GRANTED", "PERMISSION_GRANTED");
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(intent);
        } else {
            Variables.GaincityPhoneNum = phoneNumber;
            final String[] permissions = new String[]{Manifest.permission.CALL_PHONE};
            //requestPermissionsResult(4,permissions);
            //if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
            //    Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this, permissions, HANDLE_CALL_PHONE_PERM);
            // return;
           /* }
            else {
               AlertDialog.Builder builder = new AlertDialog.Builder(this);
               builder.setTitle("Gain City App")
                       .setMessage(R.string.no_call_permission)
                       .setPositiveButton(R.string.ok, null)
                       .show();
           }*/
        }


    }

    public void requestPermissionsResult(int requestCode,
                                         @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {


        if ((requestCode == HANDLE_LOCATION) &&
                (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            try {
                Variables.gmap.setMyLocationEnabled(true);
            } catch (SecurityException e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(StoreMapActivity.this, e);
            }
            Variables.gmap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                @Override
                public void onMyLocationChange(Location arg0) {

                    Variables.currentLocation = new LatLng(arg0.getLatitude(), arg0.getLongitude());

                }
            });


            if ((Variables.requestDirection) && ((Variables.currentLocation) != null)) {
                Variables.requestDirection = false;
                String uri = "http://maps.google.com/maps?f=d&hl=es&saddr=" + Variables.currentLocation.latitude + "," + Variables.currentLocation.longitude + "&daddr=" + Variables.selectedMarker.latitude +
                        "," + Variables.selectedMarker.longitude;
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            } else if ((Variables.requestDirection) && ((Variables.currentLocation) == null)) {
                Variables.requestDirection = false;
                Toast.makeText(this, "Please try again.", Toast.LENGTH_SHORT).show();
            }

            return;
        } else if (requestCode == HANDLE_LOCATION) {
            if (Variables.requestDirection) {
                Variables.requestDirection = false;
            }

        }
        if (requestCode != HANDLE_CALL_PHONE_PERM) {
            //24-9-2018 old Log.d("CALL_PHONE", "CALL_PHONE Got unexpected permission result: " + requestCode);
            Variables.GaincityPhoneNum = "";
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //24-9-2018 old Log.d("CALL_PHONE", "CALL_PHONE : permission granted ");

            callGaincity(Variables.GaincityPhoneNum);
            return;
        }

        Variables.GaincityPhoneNum = "";
        //24-9-2018 old Log.e("CALL_PHONE", "CALL_PHONE:Permission not granted: results len = " + grantResults.length +
        //24-9-2018 old       " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Gain City App")
                .setMessage(R.string.no_call_permission)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    public class Store_L_AsyncTask extends AsyncTask<Void, Void, JSONObject> {
        private WeakReference<Context> mContext;
        private boolean errorOccured = false;
        private String jsonURL = "";
        Activity mActivity;

        public Store_L_AsyncTask(Context c, String s, Activity a) {
            mActivity = a;
            mContext = new WeakReference<>(c);
            jsonURL = s;
        }

        @Override
        protected void onPreExecute() {
            Context context = mContext.get();
            if (!(((StoreMapActivity) context).prgdilogStore.isShowing())) {
                ((StoreMapActivity) context).prgdilogStore.show();
            }

        }


        @Override
        protected JSONObject doInBackground(Void... params) {
            Context context = mContext.get();
            if (context == null) return null;

            try {

                URLConnection urlConn = null;
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(jsonURL);
                    urlConn = url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuffer.append(line);
                    }

                    return new JSONObject(stringBuffer.toString());
                } catch (Exception ex) {
                    //24-9-2018 old Log.e("App", "yourDataTask", ex);
                    return null;
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(context, e);

                errorOccured = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            final Context context = mContext.get();
            if (context == null) return;
            if (!errorOccured) {
                if (response != null) {
                    //  JSONObject jsonObj = new JSONObject(jsonStr);
                    try {
                        // Getting JSON Array node
                        JSONArray main = response.getJSONArray("main");
                        Variables.storeData.add(new StoreListitem("About Store", "", "", "", "",
                                "", "", 3, 2));//22-10-2018

                        Variables.storeData.add(new StoreListitem("Main Showrooms", "", "", "", "",
                                "", "", 2, 2));//19-10-2018

                        Variables.storeData.add(new StoreListitem("title1", "", "", "", "",
                                "", "", 0, 2));

                        // looping through All Contacts
                        for (int i = 0; i < main.length(); i++) {
                            JSONObject c = main.getJSONObject(i);
                            String name = c.getString("name");
                            String address = c.getString("address");
                            String opening = c.getString("opening");
                            String closing = c.getString("closing");
                            String telno = c.getString("telno");
                            String latitude = c.getString("latitude");
                            String longitude = c.getString("longitude");

                            // Phone node is JSON Object

                            int cType = i % 2;
                            Variables.storeData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));
                            Variables.StoreMapData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));

                        }
                        Variables.storeData.add(new StoreListitem("Specialty Store", "", "", "", "",
                                "", "", 2, 2));//19-10-2018
                        Variables.storeData.add(new StoreListitem("title1", "", "", "", "",
                                "", "", 0, 2));
                        JSONArray specialty = response.getJSONArray("specialty");

                        // looping through All Contacts
                        for (int i = 0; i < specialty.length(); i++) {
                            JSONObject c = specialty.getJSONObject(i);
                            String name = c.getString("name");
                            String address = c.getString("address");
                            String opening = c.getString("opening");
                            String closing = c.getString("closing");
                            String telno = c.getString("telno");
                            String latitude = c.getString("latitude");
                            String longitude = c.getString("longitude");

                            // Phone node is JSON Object
                            int cType = i % 2;

                            Variables.storeData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));
                            Variables.StoreMapData.add(new StoreListitem(name, address, opening, closing, telno,
                                    latitude, longitude, 1, cType));

                        }

                    } catch (final JSONException e) {
                        /*Toast.makeText(context,
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();*/

                    }
                    Variables.viewPager = mViewPager;
                    //Variables.viewPager=viewPager;
                    setupViewPager(Variables.viewPager);
                    // Set Tabs inside Toolbar
                    mTabs.setupWithViewPager(Variables.viewPager);
                    Variables.adapter.notifyDataSetChanged();
                }
            }
            Variables.adapter.notifyDataSetChanged();
            if (((StoreMapActivity) context).prgdilogStore.isShowing()) {
                ((StoreMapActivity) context).prgdilogStore.dismiss();
            }
        }
    }
}