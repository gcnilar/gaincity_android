package app.gaincity.app_gain_city.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import app.gaincity.app_gain_city.StaticMethods;



public class ScaledContextWrapper extends ContextWrapper {

    public ScaledContextWrapper(Context base) {
        super(base);
    }


    @SuppressWarnings("deprecation")
    public static ScaledContextWrapper wrap(Context context) {

        try {
            float fontScale = context.getResources().getConfiguration().fontScale;
            //Log.d("fontScale","fontScale"+fontScale+(fontScale!=1.0));
            if(fontScale!=1.0) {

                Resources resources = context.getResources();
                Configuration configuration = resources.getConfiguration();

                DisplayMetrics metrics = context.getResources().getDisplayMetrics();
                WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
                wm.getDefaultDisplay().getMetrics(metrics);
                metrics.scaledDensity = (float) 1.0 * metrics.density;//1.0 =fontScale
                configuration.densityDpi = (int) (metrics.densityDpi * 1.0);//1.0=fontScale
                configuration.fontScale = (float) 1.0;

                context = context.getApplicationContext().createConfigurationContext(configuration);
                   context.createConfigurationContext(configuration);

            }
        }catch (Exception e){
            e.printStackTrace();

            StaticMethods.sendCrashReport(context.getApplicationContext(), e);
        }

        return new ScaledContextWrapper(context);
    }

}
