package app.gaincity.app_gain_city.view;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.Marker;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Variables;

public class StoreL_Fragment extends Fragment {
    View v;
    Context ctx;
    public static final String TAG = "StoreL_Fragment";
    private static RecyclerView mList;
    private static LinearLayoutManager linearLayoutManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout = 0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        v = null;
        ctx = null;
        mList = null;
        linearLayoutManager = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.store_locations, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018
        ctx = (StoreMapActivity) getActivity();

        mList = v.findViewById(R.id.main_list);
        mList.addOnItemTouchListener(
                new RecyclerItemClickListener(ctx, mList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        for (Marker marker : Variables.markerList) {
                            if(Variables.storeData.get(position).gettype()==1){//23-10-2018
                                Variables.viewPager.setCurrentItem(0);
                                if (((marker.getPosition().latitude) == (Double.parseDouble(Variables.storeData.get(position).getlatitude())))
                                    && ((marker.getPosition().longitude) == (Double.parseDouble(Variables.storeData.get(position).getlongitude())))) {

                                    marker.showInfoWindow();
                                    float zoomLevel = 15.0f; //This goes up to 21
                                    Variables.gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), zoomLevel));
                                }
                            }
                        }

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                })
        );

        Variables.adapter = new MyAdapter(ctx, Variables.storeData);
        linearLayoutManager = new LinearLayoutManager(ctx);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(linearLayoutManager);
        mList.setAdapter(Variables.adapter);

        return v;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}