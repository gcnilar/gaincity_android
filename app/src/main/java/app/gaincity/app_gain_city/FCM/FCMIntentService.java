package app.gaincity.app_gain_city.FCM;

import android.annotation.TargetApi;
//24-9-2018 old import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
//24-9-2018 old import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;


import android.os.Build;
//24-9-2018 old import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
//24-9-2018 old import android.support.v4.content.ContextCompat;
//24-9-2018 old import android.util.Log;
//import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


//24-9-2018 old import java.util.List;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.StaticMethods;
//24-9-2018 old import app.gaincity.app_gain_city.Variables;
import app.gaincity.app_gain_city.view.MainActivity;

public class FCMIntentService extends FirebaseMessagingService {
    private Notification.Builder mBuilder;
    private NotificationManager mNotificationManager;
    private RemoteViews mRemoteViews;
    private static final int NOTIF_ID = 1234;
    private static final String TAG = "FCMIntentService";

    public FCMIntentService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
       //TODO:ADD wakeUpScreen();//24-9-2018// when app closes this method will not be called,so i remove wakelock from here too

        //24-9-2018 old Log.d("Notifications", "Notifications");
        String strmessage = (String) message.getData().get("text");
        String imageurl = (String) message.getData().get("image");
        // Log.d("loadNotificationURL22", "loadNotificationURL22" + imageurl);
        setUpNotification(strmessage, imageurl);
    }

    //18-9-2018 S
    private void setUpNotification(String message,String imageurl) {

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//19-9-2018 s  TEST2:PUSH_NOTI NEED Android 8.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createChannel(mNotificationManager);
        //19-9-2018 e  TEST2:PUSH_NOTI NEED Android 8.0


        Intent intentNotify = new Intent(this, MainActivity.class);
        //18-6-2018 old intentNotify.putExtra("Load", "notifications");//7-9
        intentNotify.putExtra("loadNotificationURL", imageurl); //18-6-2018
        intentNotify.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);//7-9 Added |Intent.FLAG_ACTIVITY_NEW_TASK
        PendingIntent pendIntent = PendingIntent.getActivity(this, 0, intentNotify, PendingIntent.FLAG_UPDATE_CURRENT);//7-9 old ok PendingIntent.FLAG_UPDATE_CURRENT

        // notification's layout
        // mRemoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification_small);
        // notification's icon
        //19-7 ok mRemoteViews.setImageViewResource(R.id.notif_icon, R.drawable.audio);
        //mRemoteViews.setImageViewResource(R.id.notif_icon2, R.drawable.car);
        try {
            //18-6-2018   mRemoteViews.setImageViewBitmap(R.id.notif_icon2, Picasso.with(getApplicationContext()).load(imageurl).get());// ok
//           ImageView imageView = (ImageView) findViewById(R.id.imageView);
//           GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
//           Glide.with(this).load(R.raw.sample_gif).into(imageViewTarget);
        } catch (Exception e) {
            e.printStackTrace();//7-6-2018
            //StaticMethods.sendCrashReport(getApplicationContext(),e.getStackTrace().toString());//7-6-2018
            StaticMethods.sendCrashReport(getApplicationContext(), e);//7-6-2018

        }

        //18-9-2018 S

        mRemoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification_small);

        // notification's title
        mRemoteViews.setTextViewText(R.id.notif_title, getResources().getString(R.string.app_name));
        // notification's content
        mRemoteViews.setTextViewText(R.id.notif_content, message);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {//19-9-2018 s  TEST2:PUSH_NOTI NEED Android 8.0
           // NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "Gaincity");
            NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                    .setSmallIcon(R.drawable.noti_icon_plain)//required for api 26 msg
                    /*.setColor
                    (ContextCompat.getColor(this, R.color.colorHeader))*/
                    .setContentTitle("").setContentText("");//required for api 26 msg


            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder//setSmallIcon(R.drawable.noti_icon_plain)
                   // .setSound(uri)
                   .setOngoing(false)
                   .setContentIntent(pendIntent)//required for api 26 msg
                   // .setContent(mRemoteViews)

                    .setAutoCancel(true);

            // starting service with notification in foreground mode
            Notification notification = mBuilder.build();//v12 TODO:add
            // Cancel the notification after its selected
            notification.flags |= Notification.FLAG_AUTO_CANCEL;//19-9-2018
        /*v13
        Notification notification  =   new Notification.Builder(getApplicationContext())
                .setContentTitle("Title")
                .setContentText("Some text....")
                .setSmallIcon(R.drawable.noti_icon_plain)
                .setContentIntent(pendIntent)
                .setAutoCancel(true).build();*///v12  4-9-2018

            notification.bigContentView = mRemoteViews;//v12 TODO:add

            ////18-9-2018 E

//            }else{
//
//            }
            // Waking up mobile if it is sleeping
            //24-9-2018 old  WakeLocker.acquire(getApplicationContext()); //27-7

            //19-9-2018 ok  startForeground(NOTIF_ID, notification);//19-9-2018
               mNotificationManager.notify(NOTIF_ID /* ID of notification */, notification);
            // Releasing wake lock
            //24-9-2018 old  WakeLocker.release();//27-7


        }//19-9-2018 e  TEST2:PUSH_NOTI NEED Android 8.0
        else {
            mBuilder = new Notification.Builder(getApplicationContext());

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSmallIcon(R.drawable.noti_icon_plain)
                    .setSound(uri)
                    .setOngoing(false)
                    .setContentIntent(pendIntent)
                    .setContent(mRemoteViews)
                    .setAutoCancel(true);

            // starting service with notification in foreground mode
            Notification notification = mBuilder.build();//v12 TODO:add
        /*v13
        Notification notification  =   new Notification.Builder(getApplicationContext())
                .setContentTitle("Title")
                .setContentText("Some text....")
                .setSmallIcon(R.drawable.noti_icon_plain)
                .setContentIntent(pendIntent)
                .setAutoCancel(true).build();*///v12  4-9-2018

            notification.bigContentView = mRemoteViews;//v12 TODO:add

            ////18-9-2018 E

//            }else{
//
//            }
            // Waking up mobile if it is sleeping
          //24-9-2018 old  WakeLocker.acquire(getApplicationContext()); //27-7

            mNotificationManager.notify(NOTIF_ID /* ID of notification */, notification);
            // Releasing wake lock
          //24-9-2018 old  WakeLocker.release();//27-7
//        }
        }
    }
    //18-9-2018 E

    //19-9-2018 s TEST2:PUSH_NOTI NEED
    @TargetApi(26)
    private void createChannel(NotificationManager notificationManager) {

        String name = getString(R.string.default_notification_channel_id);
        String description = "Gaincitydesc";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationChannel mChannel = new NotificationChannel(name, name, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        notificationManager.createNotificationChannel(mChannel);
    }
    //19-9-2018 e

    //24-9-2018 s
   /* private  void wakeUpScreen() {
        // Wake up screen
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn;
        if (Build.VERSION.SDK_INT >= 20) {
            isScreenOn = powerManager.isInteractive();
        } else {
            isScreenOn = powerManager.isScreenOn();
        }
        if (!isScreenOn){
            PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MH24_SCREENLOCK");
            wl.acquire(2000);//The timeout after which to release the wake lock, in milliseconds
            PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MH24_SCREENLOCK");
            wl_cpu.acquire(2000);//The timeout after which to release the wake lock, in milliseconds
        }
    }*/
    //24-9-2018 e
}
