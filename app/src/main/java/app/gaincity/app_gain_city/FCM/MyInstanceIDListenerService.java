package app.gaincity.app_gain_city.FCM;

//24-9-2018 old import android.util.Log;

//import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.view.MainActivity;


public class MyInstanceIDListenerService extends FirebaseInstanceIdService {
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = null;
        String authorizedEntity ="922739263903";//testing app sender id- "722702849713"; // sender id of Gaincity Project
        String scope = "FCM";
        try {
            refreshedToken = FirebaseInstanceId.getInstance().getToken(authorizedEntity, scope);
             //Log.d("Refreshed token:", "Refreshed token:" + refreshedToken);
        } catch (IOException e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(getApplicationContext(), e);

            //24-9-2018 old Log.d("FCM", "Error getting refreshed token");
        } catch (Exception e) {
            e.printStackTrace();

            StaticMethods.sendCrashReport(getApplicationContext(), e);//7-6-2018
            //refreshedToken = FirebaseInstanceId.getInstance().getToken();///check what this means
            //24-9-2018 old Log.d("FCM", "Error getting refreshed token");
        }

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        if (token == null) return;
        //code To send FCM registration token to our server, so we can send msgs by tokens by selectling devices token
        MainActivity.RegisterAsyncTask task = new MainActivity.RegisterAsyncTask(this, token);
        task.execute();
    }
}
