package app.gaincity.app_gain_city.Barcode.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
//24-9-2018 old import android.hardware.camera2.CameraManager;
import android.util.AttributeSet;
import android.view.View;

import app.gaincity.app_gain_city.R;


public final class ViewfinderView extends View {

    private final Paint paint;

    // This constructor is used when the class is built from an XML resource.
    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // Initialize these once for performance rather than calling them every time in onDraw().
        paint = new Paint();
    }


    @SuppressLint("DrawAllocation")
    @Override
    public void onDraw(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        int mBoxWidth = width / 2, mBoxHeight = mBoxWidth;
        int right = (width / 2) + (mBoxHeight / 2);
        int left = (width / 2) - (mBoxHeight / 2);
        int bottom = (height / 2) + (mBoxWidth / 2);
        int top = (height / 2) - (mBoxWidth / 2);
        paint.setColor(Color.BLACK);
        paint.setAlpha(90);

        canvas.drawRect(0, 0, width, top, paint);
        canvas.drawRect(0, top, left, bottom, paint);
        canvas.drawRect(0, bottom, width, height, paint);
        canvas.drawRect(right, top, width, bottom, paint);


        Paint paint2 = new Paint();
        paint2.setColor(Color.WHITE);
        Resources res = getResources();
        Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.scan_top_left);
        canvas.drawBitmap(bitmap, left, top, paint2);

        Bitmap bitmap2 = BitmapFactory.decodeResource(res, R.drawable.scantop_top_right);
        canvas.drawBitmap(bitmap2, right - bitmap2.getWidth(), top, paint2);

        Bitmap bitmap3 = BitmapFactory.decodeResource(res, R.drawable.scan_bottom_left);
        canvas.drawBitmap(bitmap3, left, bottom - bitmap2.getWidth(), paint2);

        Bitmap bitmap4 = BitmapFactory.decodeResource(res, R.drawable.scan_bottom_right);
        canvas.drawBitmap(bitmap4, right - bitmap2.getWidth(), bottom - bitmap2.getWidth(), paint2);

    }

}
