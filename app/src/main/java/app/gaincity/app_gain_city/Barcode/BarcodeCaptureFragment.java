package app.gaincity.app_gain_city.Barcode;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;//16-10-2018 old android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
//24-9-2018 old import android.util.Log;
//24-9-2018 old import android.view.GestureDetector;
import android.view.LayoutInflater;
//24-9-2018 old import android.view.MotionEvent;
//24-9-2018 old import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
//24-9-2018 old import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.MultiProcessor;
//24-9-2018 old import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import app.gaincity.app_gain_city.R;
import app.gaincity.app_gain_city.Barcode.camera.BoxDetector;
import app.gaincity.app_gain_city.Barcode.camera.CameraSource;
import app.gaincity.app_gain_city.Barcode.camera.CameraSourcePreview;
import app.gaincity.app_gain_city.Barcode.camera.GraphicOverlay;
import app.gaincity.app_gain_city.StaticMethods;
import app.gaincity.app_gain_city.Variables;
import app.gaincity.app_gain_city.view.MainActivity;


public class BarcodeCaptureFragment extends Fragment {
    MainActivity ctx;
    View layout_v;
    public static final String TAG = "Barcode-reader";
    CameraSource.Builder builder;
    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;//HANDLE_CALL_PHONE_PERM = 4
    public static final String BarcodeObject = "Barcode";
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Variables.numcartItemsBeforeClickCheckout=0;//24-08-2018 Note: user navigate to this
        //fragment without getting action to set to 0
        Variables.loginMethod = -1;//28-08-2018 Note: user navigate to this
        //fragment without getting action to set to -1
        startCameraSource();
    }

    ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        layout_v = inflater.inflate(R.layout.fragment_barcode_capture, container, false);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//12-11-2018
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();//12-11-2018

        ctx= (MainActivity) getActivity();//11-9-2018
        mPreview = (CameraSourcePreview) layout_v.findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) layout_v.findViewById(R.id.graphicOverlay);

        // read parameters from the intent used to launch the activity.
        boolean autoFocus = true;
        boolean useFlash = false;

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission((MainActivity)ctx, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            //24-9-2018 old  Log.d("PERMISSION_GRANTED", "PERMISSION_GRANTED");
            createCameraSource(autoFocus, useFlash);
        } else {
            requestCameraPermission();
        }

        final View bar = new View((MainActivity)ctx);
        bar.setBackground(ContextCompat.getDrawable((MainActivity)ctx, R.drawable.bg_scan));
        final Animation animation = AnimationUtils.loadAnimation((MainActivity)ctx, R.anim.scan);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bar.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((MainActivity)ctx).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        int mBoxWidth = width / 2, mBoxHeight = mBoxWidth;
        int bottom = (height / 2) + (mBoxWidth / 2);
        int top = (height / 2) - (mBoxWidth / 2);

        RelativeLayout.LayoutParams mParam = new RelativeLayout.LayoutParams(mBoxWidth, mBoxWidth / 10);
        mParam.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        bar.setLayoutParams(mParam);

        RelativeLayout mLinearLayout = (RelativeLayout) layout_v.findViewById(R.id.topLayout);
        mLinearLayout.addView(bar);
        bar.setVisibility(View.VISIBLE);

        ObjectAnimator animator = ObjectAnimator.ofFloat(bar, View.Y, top - getToolBarHeight() + (mBoxWidth / 10), bottom - getToolBarHeight() - (mBoxWidth / 10));
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setDuration(TimeUnit.SECONDS.toMillis(2));
        animator.start();

        if (!(StaticMethods.isNetworkAvailable((MainActivity)ctx))) {//1-11-2018 - !( added
            StaticMethods.noConnection((MainActivity)ctx);
        }

        return layout_v;
    }


    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = ((MainActivity)ctx).getApplicationContext();//29-12 old getApplicationcontext

        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each barcode.
        BarcodeDetector mmbarcodeDetector = new BarcodeDetector.Builder(context)
                .build();
        BoxDetector barcodeDetector = new BoxDetector(mmbarcodeDetector, mPreview.getHeight() / 3, mPreview.getWidth());

        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay,(MainActivity) ctx);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            //24-9-2018 old   Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = ((MainActivity)ctx).registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText((MainActivity)ctx, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                //24-9-2018 old Log.w(TAG, getString(R.string.low_storage_error));

                AlertDialog alertDialog = new AlertDialog.Builder((MainActivity)ctx).create();
                alertDialog.setTitle("Error Reading Barcode");
                alertDialog.setMessage(getString(R.string.low_storage_error));
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        builder = new CameraSource.Builder((MainActivity)ctx, barcodeDetector)//29-12 old getApplicationContext()
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

    private void requestCameraPermission() {
        //24-9-2018 old Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale((MainActivity)ctx,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions((MainActivity)ctx, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = (MainActivity)ctx;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        layout_v.findViewById(R.id.topLayout).setOnClickListener(listener);
        Snackbar.make(((MainActivity)ctx).findViewById(R.id.category_cont), R.string.permission_camera_rationale, Snackbar.LENGTH_LONG)
                .setAction(R.string.ok, listener)
                .show();
    }

    public int getToolBarHeight() {
        int[] attrs = new int[]{R.attr.actionBarSize};
        TypedArray ta = ((MainActivity)ctx).obtainStyledAttributes(attrs);
        int toolBarHeight = ta.getDimensionPixelSize(0, -1);
        ta.recycle();
        return toolBarHeight;
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                ((MainActivity)ctx).getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog((MainActivity)ctx, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                e.printStackTrace();

                StaticMethods.sendCrashReport(((MainActivity) ctx), e);
                //24-9-2018 old  Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    /**
     * Stops the camera.
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
        //11-9-2018 s
        ctx=null;
        layout_v=null;

        builder=null;

       mCameraSource=null;
        mPreview=null;
        mGraphicOverlay=null;
        //11-9-2018 e

    }

    public void requestPermissionsResult(int requestCode,
                                         @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            //24-9-2018 old Log.d(TAG, "Got unexpected permission result: " + requestCode);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //24-9-2018 old  Log.d(TAG, "Camera permission granted - initialize the camera source");
            createCameraSource(true, false);
            return;
        }

        //24-9-2018 old Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
        //24-9-2018 old  " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        AlertDialog.Builder builder = new AlertDialog.Builder((MainActivity)ctx);
        builder.setTitle("Gain City App")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, null)
                .show();
    }
}