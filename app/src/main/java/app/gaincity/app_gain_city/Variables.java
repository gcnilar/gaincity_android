package app.gaincity.app_gain_city;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

import app.gaincity.app_gain_city.service.model.CategoryResponse;
import app.gaincity.app_gain_city.view.StoreListitem;

public class Variables {

    // require for DB in future public static boolean calledUpgrade = false;
    public static String fragmentUrl = "";//Don't reset in oncreate
    final public static String URL_HOME_2 = "https://www.gaincity.com";//12-9-2018 old "https://www.gaincity.net";
    final public static String URL_HOME = URL_HOME_2 + "/";//21-08-2018 old "https://www.gaincity.net/";

    final public static String STATIC_URL = "https://static.gaincity.com/";//16-10-2018

    public static String appendToHomeUrl(String s) {
        return URL_HOME + s
                ;
    }

    public static String getBaseUrl() {//NOTE:TO use when URL_HOME_2 is going to change with an appended part
        //First URL to call when app load (oncreate of MainActivity when load webview)
        return URL_HOME_2
                ; 
    }


    public static int numcartItemsBeforeClickCheckout = 0;//reset when oncreate only//no reset when logout
    public static int numcartItems = 0;//reset when oncreate only//no reset when logout
    //30-08-2018 old public static int taskLoadingCount = 0;
    //30-08-2018 old  public static boolean customerloggingIn = false;
    //30-08-2018 old  public static boolean add = true;
    public static String cusToken = "";//reset when logout  or oncreate
    public static boolean loginPost = false;//reset when logout  or oncreate
    public static boolean useSmartLock = false;//reset when logout  or oncreate
    public static boolean isInsideSessionLogin = false;//need reset when oncreate only//in code reset when onPageFinished (no reset when logout, because can cause continuous progressDialog)
    public static boolean isActiveinEmail = false;//To track text field editting place(need reset when logout  or oncreate)
    public static boolean isActiveinPassword = false;//To track text field editting place(need reset when logout  or oncreate)
    public static boolean isTokenSuccess = false;//reset when logout  or oncreate- can delete this now?-TODO: need in future?
    //30-08-2018 OLD assigned but never used public static boolean lastURLcalled = false;
    public static ArrayList<CategoryResponse> categories = null;//reset when oncreate only //assigened when prepareListData() called, no need reset when logout
    public static boolean showGroup1 = true;//reset when oncreate only
    public static boolean clickedMenuItem = false;//reset when oncreate only
    public static boolean qrcodeRedirect = false;//reset when oncreate only
    //30-08-2018 old never assigned to true public static boolean needNewFragment = false;
    //30-08-2018 old  public static boolean isInsideEditAccount = false;
    public static boolean savePassword = false;//reset when logout  or oncreate
    public static boolean stoppedUsingJs = false;//reset when oncreate only
   // public static boolean loadUrlInPoppedFragment = false;
    public static int loginMethod = -1;//reset when oncreate only//0=checkout login, 1= addtowishlist login , 2= other login //16-8-2018
    //30-08-2018 old  public static String PHPSESSID = "";
    //30-08-2018 OLD assigned but never used public static boolean doingloginAction = false;
    public static String notificationURL = "";//Don't reset in oncreate as notification can have impact when app oncreate?
    public static boolean oncreateCalled = false;//Don't reset to false in oncreate, (this is assigned to true in oncreate)

    public static boolean loadCheckout = false;//if logout happened before go load checkout, it will remain as true, so need to reset to false when logout,login, oncreate
    public static String forgotPasswordEmail = "";//6-9-2018
    public static boolean insideCheckout = false;//when user is inside checkout it goes to different URLs than https://www.gaincity.net
    public static String GaincityPhoneNum = "";//6-9-2018
    //21-9-2018 old public static boolean clickedGroup = false;//19-9-2018

    public static ArrayList<StoreListitem> storeData;//18-10-2018
    public static ArrayList<StoreListitem> StoreMapData;//18-10-2018
    public static RecyclerView.Adapter adapter;//18-10-2018
    public static GoogleMap gmap;//19-10-2018
    public static LatLng currentLocation;//19-10-2018
    public static LatLng selectedMarker;//19-10-2018
    public static String selectedMarkerPhone;//19-10-2018
    public static ViewPager viewPager;//22-10-2018
    public static ArrayList<Marker> markerList;//22-10-2018
    public static boolean requestDirection;//22-10-2018
    public static String loadedMenuItem;//29-10-2018

    //public static String PHPSESSID;//2-11-2018 TODO:REMOVE
}
