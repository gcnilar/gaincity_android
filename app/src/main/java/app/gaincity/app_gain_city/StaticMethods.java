package app.gaincity.app_gain_city;


import android.app.AlertDialog;
//24-9-2018 old import android.app.Fragment;
//24-9-2018 old import android.app.FragmentManager;
//24-9-2018 old import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Patterns;
import android.util.TypedValue;

import java.io.PrintWriter;
import java.io.StringWriter;

import app.gaincity.app_gain_city.view.MainActivity;

public class StaticMethods {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }




    public static void noConnection(Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(context.getString(R.string.checkInternetDialog))//28-08-2018//checkInternetDialog ("Please check your network connection and try again.")
                .setNeutralButton("OK", null)
                .create();
        alertDialog.show();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static void sendCrashReport(Context context, Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String sStackTrace = sw.toString();
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"webadmin@gaincity.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Crash Report:"+BuildConfig.VERSION_CODE);//14-1-2019 old Crash Report
        email.putExtra(Intent.EXTRA_TEXT, sStackTrace);
        email.setType("message/rfc822");

        Intent i = Intent.createChooser(email, "An error occured. Choose an Email client to send crash report:");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public static void sendCrashReport(Context context, Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String sStackTrace = sw.toString();
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"webadmin@gaincity.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Crash Report:"+BuildConfig.VERSION_CODE);
        email.putExtra(Intent.EXTRA_TEXT, sStackTrace);
        email.setType("message/rfc822");

        Intent i = Intent.createChooser(email, "An error occured. Choose an Email client to send crash report:");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }

    public static void showMsgCloseApp(final Context ct) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ct);
        builder.setMessage(R.string.msgAppClose);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();

                //11-9-2018 s
                /*FragmentManager manager = getFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove((Fragment) object);
                trans.commit();*/

                //1-9-2018 e

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((MainActivity) ct).finishAndRemoveTask();
                }
                else {
                    ((MainActivity) ct).finish();
                }
                //System.exit(0);//11-9-2018

            }
        });
        builder.setNegativeButton("NO", null);
        builder.show();
    }

    //14-11-2018 s
    public static int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }
    //14-11-2018 e
}
